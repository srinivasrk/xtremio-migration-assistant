﻿namespace xtremeIO
{
    partial class LandingPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LandingPage));
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.xms_ip_lbl = new System.Windows.Forms.Label();
            this.xms_os_lbl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xMSInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateexceltoolstrip = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fAQToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutUsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportABugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.percent_completed = new System.Windows.Forms.ProgressBar();
            this.view_san_btn = new System.Windows.Forms.Button();
            this.upload_san_btn = new System.Windows.Forms.Button();
            this.scripts_btn = new System.Windows.Forms.Button();
            this.back_btn = new System.Windows.Forms.Button();
            this.bw_reports_btn = new System.Windows.Forms.Button();
            this.cluster_reports_btn = new System.Windows.Forms.Button();
            this.volume_reports_btn = new System.Windows.Forms.Button();
            this.show_reports_btn = new System.Windows.Forms.Button();
            this.cluster_btn = new System.Windows.Forms.Button();
            this.consistency_btn = new System.Windows.Forms.Button();
            this.alert_btn = new System.Windows.Forms.Button();
            this.hardware_btn = new System.Windows.Forms.Button();
            this.initiator_btn = new System.Windows.Forms.Button();
            this.volume_btn = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Font = new System.Drawing.Font("Perpetua Titling MT", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(473, 198);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Version 1.0.2";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Font = new System.Drawing.Font("Perpetua Titling MT", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(326, 165);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(369, 24);
            this.label2.TabIndex = 10;
            this.label2.Text = "XtremIO Migration Assistant";
            // 
            // xms_ip_lbl
            // 
            this.xms_ip_lbl.AutoSize = true;
            this.xms_ip_lbl.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xms_ip_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xms_ip_lbl.ForeColor = System.Drawing.Color.Green;
            this.xms_ip_lbl.Location = new System.Drawing.Point(11, 208);
            this.xms_ip_lbl.Name = "xms_ip_lbl";
            this.xms_ip_lbl.Size = new System.Drawing.Size(144, 18);
            this.xms_ip_lbl.TabIndex = 6;
            this.xms_ip_lbl.Text = "XMS IP address : ";
            this.xms_ip_lbl.Visible = false;
            // 
            // xms_os_lbl
            // 
            this.xms_os_lbl.AutoSize = true;
            this.xms_os_lbl.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xms_os_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xms_os_lbl.ForeColor = System.Drawing.Color.Green;
            this.xms_os_lbl.Location = new System.Drawing.Point(10, 171);
            this.xms_os_lbl.Name = "xms_os_lbl";
            this.xms_os_lbl.Size = new System.Drawing.Size(148, 18);
            this.xms_os_lbl.TabIndex = 9;
            this.xms_os_lbl.Text = "XMS OS version : ";
            this.xms_os_lbl.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.label1.Location = new System.Drawing.Point(0, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Migration Assistant";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1018, 133);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(884, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 20);
            this.label4.TabIndex = 31;
            this.label4.Text = "Beta Version";
            // 
            // menuStrip1
            // 
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1018, 23);
            this.menuStrip1.TabIndex = 12;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xMSInformationToolStripMenuItem,
            this.generateexceltoolstrip,
            this.quitToolStripMenuItem1});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 19);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // xMSInformationToolStripMenuItem
            // 
            this.xMSInformationToolStripMenuItem.Name = "xMSInformationToolStripMenuItem";
            this.xMSInformationToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.xMSInformationToolStripMenuItem.Text = "XMS Information";
            this.xMSInformationToolStripMenuItem.Click += new System.EventHandler(this.xMSInformationToolStripMenuItem_Click);
            // 
            // generateexceltoolstrip
            // 
            this.generateexceltoolstrip.Name = "generateexceltoolstrip";
            this.generateexceltoolstrip.Size = new System.Drawing.Size(191, 22);
            this.generateexceltoolstrip.Text = "Generate Excel Output";
            this.generateexceltoolstrip.Click += new System.EventHandler(this.generateexceltoolstrip_Click);
            // 
            // quitToolStripMenuItem1
            // 
            this.quitToolStripMenuItem1.Name = "quitToolStripMenuItem1";
            this.quitToolStripMenuItem1.Size = new System.Drawing.Size(191, 22);
            this.quitToolStripMenuItem1.Text = "Quit";
            this.quitToolStripMenuItem1.Click += new System.EventHandler(this.quitToolStripMenuItem1_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fAQToolStripMenuItem,
            this.aboutUsToolStripMenuItem1,
            this.reportABugToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 19);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // fAQToolStripMenuItem
            // 
            this.fAQToolStripMenuItem.Name = "fAQToolStripMenuItem";
            this.fAQToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.fAQToolStripMenuItem.Text = "FAQ";
            this.fAQToolStripMenuItem.Click += new System.EventHandler(this.fAQToolStripMenuItem_Click);
            // 
            // aboutUsToolStripMenuItem1
            // 
            this.aboutUsToolStripMenuItem1.Name = "aboutUsToolStripMenuItem1";
            this.aboutUsToolStripMenuItem1.Size = new System.Drawing.Size(142, 22);
            this.aboutUsToolStripMenuItem1.Text = "About Us";
            this.aboutUsToolStripMenuItem1.Click += new System.EventHandler(this.aboutUsToolStripMenuItem1_Click);
            // 
            // reportABugToolStripMenuItem
            // 
            this.reportABugToolStripMenuItem.Name = "reportABugToolStripMenuItem";
            this.reportABugToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.reportABugToolStripMenuItem.Text = "Report a Bug";
            this.reportABugToolStripMenuItem.Click += new System.EventHandler(this.reportABugToolStripMenuItem_Click);
            // 
            // percent_completed
            // 
            this.percent_completed.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.percent_completed.Location = new System.Drawing.Point(0, 717);
            this.percent_completed.Name = "percent_completed";
            this.percent_completed.Size = new System.Drawing.Size(1018, 23);
            this.percent_completed.TabIndex = 13;
            // 
            // view_san_btn
            // 
            this.view_san_btn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.view_san_btn.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.view_san_btn.BackgroundImage = global::xtremeIO.Properties.Resources.search_san1;
            this.view_san_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.view_san_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.view_san_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.view_san_btn.Location = new System.Drawing.Point(555, 398);
            this.view_san_btn.Name = "view_san_btn";
            this.view_san_btn.Size = new System.Drawing.Size(320, 98);
            this.view_san_btn.TabIndex = 33;
            this.view_san_btn.Text = "View Existing San Summary";
            this.view_san_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.view_san_btn.UseVisualStyleBackColor = false;
            this.view_san_btn.Visible = false;
            this.view_san_btn.Click += new System.EventHandler(this.view_san_btn_Click);
            // 
            // upload_san_btn
            // 
            this.upload_san_btn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.upload_san_btn.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.upload_san_btn.BackgroundImage = global::xtremeIO.Properties.Resources.san_upload2;
            this.upload_san_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.upload_san_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upload_san_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.upload_san_btn.Location = new System.Drawing.Point(203, 398);
            this.upload_san_btn.Name = "upload_san_btn";
            this.upload_san_btn.Size = new System.Drawing.Size(278, 98);
            this.upload_san_btn.TabIndex = 32;
            this.upload_san_btn.Text = "Upload San Summary  ";
            this.upload_san_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.upload_san_btn.UseVisualStyleBackColor = false;
            this.upload_san_btn.Visible = false;
            this.upload_san_btn.Click += new System.EventHandler(this.upload_san_btn_Click);
            // 
            // scripts_btn
            // 
            this.scripts_btn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.scripts_btn.BackColor = System.Drawing.SystemColors.Control;
            this.scripts_btn.BackgroundImage = global::xtremeIO.Properties.Resources.scripts_logo11;
            this.scripts_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scripts_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.scripts_btn.Location = new System.Drawing.Point(570, 585);
            this.scripts_btn.Name = "scripts_btn";
            this.scripts_btn.Size = new System.Drawing.Size(223, 91);
            this.scripts_btn.TabIndex = 19;
            this.scripts_btn.Text = "Scripts Generator";
            this.scripts_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.scripts_btn.UseVisualStyleBackColor = false;
            this.scripts_btn.Click += new System.EventHandler(this.scripts_btn_Click);
            // 
            // back_btn
            // 
            this.back_btn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.back_btn.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.back_btn.BackgroundImage = global::xtremeIO.Properties.Resources.back_button2;
            this.back_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.back_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.back_btn.Location = new System.Drawing.Point(885, 650);
            this.back_btn.Name = "back_btn";
            this.back_btn.Size = new System.Drawing.Size(121, 61);
            this.back_btn.TabIndex = 18;
            this.back_btn.Text = "Back";
            this.back_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.back_btn.UseVisualStyleBackColor = false;
            this.back_btn.Visible = false;
            this.back_btn.Click += new System.EventHandler(this.back_btn_Click);
            // 
            // bw_reports_btn
            // 
            this.bw_reports_btn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bw_reports_btn.BackColor = System.Drawing.SystemColors.Control;
            this.bw_reports_btn.BackgroundImage = global::xtremeIO.Properties.Resources.initiator1;
            this.bw_reports_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bw_reports_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.bw_reports_btn.Location = new System.Drawing.Point(699, 380);
            this.bw_reports_btn.Name = "bw_reports_btn";
            this.bw_reports_btn.Size = new System.Drawing.Size(248, 102);
            this.bw_reports_btn.TabIndex = 17;
            this.bw_reports_btn.Text = "Bandwidth / IOPS \r\n       Reports     ";
            this.bw_reports_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bw_reports_btn.UseVisualStyleBackColor = false;
            this.bw_reports_btn.Visible = false;
            this.bw_reports_btn.Click += new System.EventHandler(this.bw_reports_btn_Click);
            // 
            // cluster_reports_btn
            // 
            this.cluster_reports_btn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cluster_reports_btn.BackColor = System.Drawing.SystemColors.Control;
            this.cluster_reports_btn.BackgroundImage = global::xtremeIO.Properties.Resources.cluster3;
            this.cluster_reports_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cluster_reports_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.cluster_reports_btn.Location = new System.Drawing.Point(396, 380);
            this.cluster_reports_btn.Name = "cluster_reports_btn";
            this.cluster_reports_btn.Size = new System.Drawing.Size(259, 102);
            this.cluster_reports_btn.TabIndex = 16;
            this.cluster_reports_btn.Text = "Cluster Reports";
            this.cluster_reports_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cluster_reports_btn.UseVisualStyleBackColor = false;
            this.cluster_reports_btn.Visible = false;
            this.cluster_reports_btn.Click += new System.EventHandler(this.cluster_reports_btn_Click);
            // 
            // volume_reports_btn
            // 
            this.volume_reports_btn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.volume_reports_btn.BackColor = System.Drawing.SystemColors.Control;
            this.volume_reports_btn.BackgroundImage = global::xtremeIO.Properties.Resources.Volume_new;
            this.volume_reports_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.volume_reports_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.volume_reports_btn.Location = new System.Drawing.Point(99, 380);
            this.volume_reports_btn.Name = "volume_reports_btn";
            this.volume_reports_btn.Size = new System.Drawing.Size(231, 102);
            this.volume_reports_btn.TabIndex = 15;
            this.volume_reports_btn.Text = "Volume Reports";
            this.volume_reports_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.volume_reports_btn.UseVisualStyleBackColor = false;
            this.volume_reports_btn.Visible = false;
            this.volume_reports_btn.Click += new System.EventHandler(this.volume_reports_btn_Click);
            // 
            // show_reports_btn
            // 
            this.show_reports_btn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.show_reports_btn.BackColor = System.Drawing.SystemColors.Control;
            this.show_reports_btn.BackgroundImage = global::xtremeIO.Properties.Resources.reports_icon;
            this.show_reports_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.show_reports_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.show_reports_btn.Location = new System.Drawing.Point(230, 585);
            this.show_reports_btn.Name = "show_reports_btn";
            this.show_reports_btn.Size = new System.Drawing.Size(223, 91);
            this.show_reports_btn.TabIndex = 14;
            this.show_reports_btn.Text = "XtremIO Reports";
            this.show_reports_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.show_reports_btn.UseVisualStyleBackColor = false;
            this.show_reports_btn.Click += new System.EventHandler(this.show_reports_Click);
            // 
            // cluster_btn
            // 
            this.cluster_btn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cluster_btn.BackColor = System.Drawing.SystemColors.Window;
            this.cluster_btn.BackgroundImage = global::xtremeIO.Properties.Resources.cluster3;
            this.cluster_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.cluster_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.cluster_btn.Location = new System.Drawing.Point(405, 269);
            this.cluster_btn.Name = "cluster_btn";
            this.cluster_btn.Size = new System.Drawing.Size(238, 105);
            this.cluster_btn.TabIndex = 8;
            this.cluster_btn.Text = "Cluster Info";
            this.cluster_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cluster_btn.UseVisualStyleBackColor = false;
            this.cluster_btn.Click += new System.EventHandler(this.cluster_btn_Click);
            // 
            // consistency_btn
            // 
            this.consistency_btn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.consistency_btn.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.consistency_btn.BackgroundImage = global::xtremeIO.Properties.Resources.Volume_new;
            this.consistency_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consistency_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.consistency_btn.Location = new System.Drawing.Point(377, 434);
            this.consistency_btn.Name = "consistency_btn";
            this.consistency_btn.Size = new System.Drawing.Size(296, 105);
            this.consistency_btn.TabIndex = 7;
            this.consistency_btn.Text = "Consistency Group Details";
            this.consistency_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.consistency_btn.UseVisualStyleBackColor = false;
            this.consistency_btn.Click += new System.EventHandler(this.consistency_btn_Click);
            // 
            // alert_btn
            // 
            this.alert_btn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.alert_btn.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.alert_btn.BackgroundImage = global::xtremeIO.Properties.Resources.alert_warning1;
            this.alert_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alert_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.alert_btn.Location = new System.Drawing.Point(90, 434);
            this.alert_btn.Name = "alert_btn";
            this.alert_btn.Size = new System.Drawing.Size(240, 105);
            this.alert_btn.TabIndex = 6;
            this.alert_btn.Text = "Alerts and Warnings";
            this.alert_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.alert_btn.UseVisualStyleBackColor = false;
            this.alert_btn.Click += new System.EventHandler(this.alert_btn_Click);
            // 
            // hardware_btn
            // 
            this.hardware_btn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.hardware_btn.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.hardware_btn.BackgroundImage = global::xtremeIO.Properties.Resources.hardware_logo1;
            this.hardware_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hardware_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.hardware_btn.Location = new System.Drawing.Point(707, 434);
            this.hardware_btn.Name = "hardware_btn";
            this.hardware_btn.Size = new System.Drawing.Size(240, 105);
            this.hardware_btn.TabIndex = 5;
            this.hardware_btn.Text = "Target Details  ";
            this.hardware_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.hardware_btn.UseVisualStyleBackColor = false;
            this.hardware_btn.Click += new System.EventHandler(this.hardware_btn_Click);
            // 
            // initiator_btn
            // 
            this.initiator_btn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.initiator_btn.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.initiator_btn.BackgroundImage = global::xtremeIO.Properties.Resources.initiator1;
            this.initiator_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.initiator_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.initiator_btn.Location = new System.Drawing.Point(707, 269);
            this.initiator_btn.Name = "initiator_btn";
            this.initiator_btn.Size = new System.Drawing.Size(240, 105);
            this.initiator_btn.TabIndex = 4;
            this.initiator_btn.Text = "Host Details   ";
            this.initiator_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.initiator_btn.UseVisualStyleBackColor = false;
            this.initiator_btn.Click += new System.EventHandler(this.initiator_btn_Click);
            // 
            // volume_btn
            // 
            this.volume_btn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.volume_btn.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.volume_btn.BackgroundImage = global::xtremeIO.Properties.Resources.Volume_new;
            this.volume_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.volume_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.volume_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.volume_btn.Location = new System.Drawing.Point(90, 269);
            this.volume_btn.Name = "volume_btn";
            this.volume_btn.Size = new System.Drawing.Size(240, 105);
            this.volume_btn.TabIndex = 3;
            this.volume_btn.Text = "Volume Details";
            this.volume_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.volume_btn.UseVisualStyleBackColor = false;
            this.volume_btn.Click += new System.EventHandler(this.volume_btn_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Image = global::xtremeIO.Properties.Resources.dell_EMC_logo;
            this.pictureBox2.Location = new System.Drawing.Point(873, 9);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Padding = new System.Windows.Forms.Padding(5);
            this.pictureBox2.Size = new System.Drawing.Size(139, 83);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 30;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.ErrorImage = global::xtremeIO.Properties.Resources.logo;
            this.pictureBox1.Image = global::xtremeIO.Properties.Resources.XtremIO_logo;
            this.pictureBox1.Location = new System.Drawing.Point(13, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Padding = new System.Windows.Forms.Padding(5);
            this.pictureBox1.Size = new System.Drawing.Size(136, 99);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // LandingPage
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1018, 740);
            this.Controls.Add(this.view_san_btn);
            this.Controls.Add(this.upload_san_btn);
            this.Controls.Add(this.scripts_btn);
            this.Controls.Add(this.back_btn);
            this.Controls.Add(this.bw_reports_btn);
            this.Controls.Add(this.cluster_reports_btn);
            this.Controls.Add(this.volume_reports_btn);
            this.Controls.Add(this.show_reports_btn);
            this.Controls.Add(this.percent_completed);
            this.Controls.Add(this.xms_ip_lbl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.xms_os_lbl);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cluster_btn);
            this.Controls.Add(this.consistency_btn);
            this.Controls.Add(this.alert_btn);
            this.Controls.Add(this.hardware_btn);
            this.Controls.Add(this.initiator_btn);
            this.Controls.Add(this.volume_btn);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "LandingPage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main Menu";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button volume_btn;
        private System.Windows.Forms.Button initiator_btn;
        private System.Windows.Forms.Button hardware_btn;
        private System.Windows.Forms.Button alert_btn;
        private System.Windows.Forms.Button consistency_btn;
        private System.Windows.Forms.Button cluster_btn;
        private System.Windows.Forms.Label xms_ip_lbl;
        private System.Windows.Forms.Label xms_os_lbl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xMSInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generateexceltoolstrip;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fAQToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutUsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem reportABugToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ProgressBar percent_completed;
        private System.Windows.Forms.Button show_reports_btn;
        private System.Windows.Forms.Button volume_reports_btn;
        private System.Windows.Forms.Button cluster_reports_btn;
        private System.Windows.Forms.Button bw_reports_btn;
        private System.Windows.Forms.Button back_btn;
        private System.Windows.Forms.Button scripts_btn;
        private System.Windows.Forms.Button view_san_btn;
        private System.Windows.Forms.Button upload_san_btn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox2;


    }
}

