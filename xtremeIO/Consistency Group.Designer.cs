﻿namespace xtremeIO
{
    partial class Consistency_Group
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.xms_ip_lbl = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.xms_os_lbl = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.vol_index_lbl = new System.Windows.Forms.Label();
            this.vol_percent_used_lbl = new System.Windows.Forms.Label();
            this.vol_pic = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.num_of_vol_lbl = new System.Windows.Forms.Label();
            this.cg_list = new System.Windows.Forms.ListBox();
            this.cluster_name_lbl = new System.Windows.Forms.Label();
            this.cg_index_lbl = new System.Windows.Forms.Label();
            this.cg_name_lbl = new System.Windows.Forms.Label();
            this.cg_pic = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cg_vol_mapping = new System.Windows.Forms.DataGridView();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vol_pic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cg_pic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cg_vol_mapping)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.pictureBox2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.xms_ip_lbl);
            this.groupBox3.Controls.Add(this.pictureBox1);
            this.groupBox3.Controls.Add(this.xms_os_lbl);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1008, 105);
            this.groupBox3.TabIndex = 26;
            this.groupBox3.TabStop = false;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(883, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 20);
            this.label2.TabIndex = 31;
            this.label2.Text = "Beta Version";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Image = global::xtremeIO.Properties.Resources.dell_EMC_logo;
            this.pictureBox2.Location = new System.Drawing.Point(866, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Padding = new System.Windows.Forms.Padding(5);
            this.pictureBox2.Size = new System.Drawing.Size(139, 83);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 30;
            this.pictureBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.label1.Location = new System.Drawing.Point(-1, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 20);
            this.label1.TabIndex = 23;
            this.label1.Text = "Migration Assistant";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            this.label5.Font = new System.Drawing.Font("Perpetua Titling MT", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(376, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(369, 24);
            this.label5.TabIndex = 21;
            this.label5.Text = "XtremIO Migration Assistant";
            // 
            // xms_ip_lbl
            // 
            this.xms_ip_lbl.AutoSize = true;
            this.xms_ip_lbl.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xms_ip_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xms_ip_lbl.ForeColor = System.Drawing.Color.Green;
            this.xms_ip_lbl.Location = new System.Drawing.Point(155, 40);
            this.xms_ip_lbl.Name = "xms_ip_lbl";
            this.xms_ip_lbl.Size = new System.Drawing.Size(144, 18);
            this.xms_ip_lbl.TabIndex = 10;
            this.xms_ip_lbl.Text = "XMS IP address : ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::xtremeIO.Properties.Resources.XtremIO_logo;
            this.pictureBox1.Location = new System.Drawing.Point(12, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Padding = new System.Windows.Forms.Padding(5);
            this.pictureBox1.Size = new System.Drawing.Size(127, 83);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // xms_os_lbl
            // 
            this.xms_os_lbl.AutoSize = true;
            this.xms_os_lbl.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xms_os_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xms_os_lbl.ForeColor = System.Drawing.Color.Green;
            this.xms_os_lbl.Location = new System.Drawing.Point(155, 9);
            this.xms_os_lbl.Name = "xms_os_lbl";
            this.xms_os_lbl.Size = new System.Drawing.Size(148, 18);
            this.xms_os_lbl.TabIndex = 11;
            this.xms_os_lbl.Text = "XMS OS version : ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.splitContainer1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 105);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1008, 625);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 16);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.splitContainer1.Panel1.Controls.Add(this.vol_index_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.vol_percent_used_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.vol_pic);
            this.splitContainer1.Panel1.Controls.Add(this.label7);
            this.splitContainer1.Panel1.Controls.Add(this.num_of_vol_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.cg_list);
            this.splitContainer1.Panel1.Controls.Add(this.cluster_name_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.cg_index_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.cg_name_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.cg_pic);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.cg_vol_mapping);
            this.splitContainer1.Size = new System.Drawing.Size(1002, 606);
            this.splitContainer1.SplitterDistance = 386;
            this.splitContainer1.TabIndex = 24;
            // 
            // vol_index_lbl
            // 
            this.vol_index_lbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.vol_index_lbl.AutoSize = true;
            this.vol_index_lbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vol_index_lbl.Location = new System.Drawing.Point(852, 250);
            this.vol_index_lbl.MaximumSize = new System.Drawing.Size(200, 0);
            this.vol_index_lbl.Name = "vol_index_lbl";
            this.vol_index_lbl.Size = new System.Drawing.Size(86, 17);
            this.vol_index_lbl.TabIndex = 52;
            this.vol_index_lbl.Text = "<vol index>";
            // 
            // vol_percent_used_lbl
            // 
            this.vol_percent_used_lbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.vol_percent_used_lbl.AutoSize = true;
            this.vol_percent_used_lbl.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vol_percent_used_lbl.Location = new System.Drawing.Point(821, 340);
            this.vol_percent_used_lbl.Name = "vol_percent_used_lbl";
            this.vol_percent_used_lbl.Size = new System.Drawing.Size(126, 15);
            this.vol_percent_used_lbl.TabIndex = 51;
            this.vol_percent_used_lbl.Text = "Logical Percent Used : ";
            // 
            // vol_pic
            // 
            this.vol_pic.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.vol_pic.Image = global::xtremeIO.Properties.Resources.database_512;
            this.vol_pic.Location = new System.Drawing.Point(884, 270);
            this.vol_pic.Name = "vol_pic";
            this.vol_pic.Size = new System.Drawing.Size(89, 67);
            this.vol_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.vol_pic.TabIndex = 50;
            this.vol_pic.TabStop = false;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Olive;
            this.label7.Location = new System.Drawing.Point(534, 369);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(181, 18);
            this.label7.TabIndex = 49;
            this.label7.Text = "Volumes aligned to CG";
            // 
            // num_of_vol_lbl
            // 
            this.num_of_vol_lbl.AutoSize = true;
            this.num_of_vol_lbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num_of_vol_lbl.Location = new System.Drawing.Point(429, 293);
            this.num_of_vol_lbl.Name = "num_of_vol_lbl";
            this.num_of_vol_lbl.Size = new System.Drawing.Size(169, 17);
            this.num_of_vol_lbl.TabIndex = 48;
            this.num_of_vol_lbl.Text = "<Number of Vols in CG>";
            // 
            // cg_list
            // 
            this.cg_list.BackColor = System.Drawing.SystemColors.Window;
            this.cg_list.Dock = System.Windows.Forms.DockStyle.Left;
            this.cg_list.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cg_list.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cg_list.FormattingEnabled = true;
            this.cg_list.HorizontalScrollbar = true;
            this.cg_list.ItemHeight = 21;
            this.cg_list.Location = new System.Drawing.Point(0, 15);
            this.cg_list.Name = "cg_list";
            this.cg_list.ScrollAlwaysVisible = true;
            this.cg_list.Size = new System.Drawing.Size(315, 371);
            this.cg_list.TabIndex = 47;
            this.cg_list.SelectedIndexChanged += new System.EventHandler(this.cg_list_SelectedIndexChanged);
            // 
            // cluster_name_lbl
            // 
            this.cluster_name_lbl.AutoSize = true;
            this.cluster_name_lbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cluster_name_lbl.Location = new System.Drawing.Point(452, 258);
            this.cluster_name_lbl.Name = "cluster_name_lbl";
            this.cluster_name_lbl.Size = new System.Drawing.Size(117, 17);
            this.cluster_name_lbl.TabIndex = 46;
            this.cluster_name_lbl.Text = "<Cluster Name>";
            // 
            // cg_index_lbl
            // 
            this.cg_index_lbl.AutoSize = true;
            this.cg_index_lbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cg_index_lbl.Location = new System.Drawing.Point(464, 224);
            this.cg_index_lbl.Name = "cg_index_lbl";
            this.cg_index_lbl.Size = new System.Drawing.Size(90, 17);
            this.cg_index_lbl.TabIndex = 45;
            this.cg_index_lbl.Text = "<CG Index>";
            // 
            // cg_name_lbl
            // 
            this.cg_name_lbl.AutoSize = true;
            this.cg_name_lbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cg_name_lbl.Location = new System.Drawing.Point(343, 88);
            this.cg_name_lbl.Name = "cg_name_lbl";
            this.cg_name_lbl.Size = new System.Drawing.Size(90, 17);
            this.cg_name_lbl.TabIndex = 44;
            this.cg_name_lbl.Text = "<CG Name>";
            // 
            // cg_pic
            // 
            this.cg_pic.Image = global::xtremeIO.Properties.Resources.XtremIO_storage_2;
            this.cg_pic.Location = new System.Drawing.Point(401, 123);
            this.cg_pic.Name = "cg_pic";
            this.cg_pic.Size = new System.Drawing.Size(222, 98);
            this.cg_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cg_pic.TabIndex = 24;
            this.cg_pic.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 15);
            this.label4.TabIndex = 23;
            this.label4.Text = "Consistency group";
            // 
            // cg_vol_mapping
            // 
            this.cg_vol_mapping.AllowUserToAddRows = false;
            this.cg_vol_mapping.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.cg_vol_mapping.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.cg_vol_mapping.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.cg_vol_mapping.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.cg_vol_mapping.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.cg_vol_mapping.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.cg_vol_mapping.DefaultCellStyle = dataGridViewCellStyle3;
            this.cg_vol_mapping.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cg_vol_mapping.Location = new System.Drawing.Point(0, 0);
            this.cg_vol_mapping.Name = "cg_vol_mapping";
            this.cg_vol_mapping.Size = new System.Drawing.Size(1002, 216);
            this.cg_vol_mapping.TabIndex = 0;
            this.cg_vol_mapping.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.cg_vol_mapping_CellContentClick);
            this.cg_vol_mapping.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.cg_vol_mapping_CellContentClick);
            this.cg_vol_mapping.SelectionChanged += new System.EventHandler(this.cg_vol_mapping_SelectionChanged);
            // 
            // Consistency_Group
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Name = "Consistency_Group";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consistency Group";
            this.Load += new System.EventHandler(this.Consistency_Group_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vol_pic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cg_pic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cg_vol_mapping)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label xms_ip_lbl;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label xms_os_lbl;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView cg_vol_mapping;
        private System.Windows.Forms.PictureBox cg_pic;
        private System.Windows.Forms.ListBox cg_list;
        private System.Windows.Forms.Label cluster_name_lbl;
        private System.Windows.Forms.Label cg_index_lbl;
        private System.Windows.Forms.Label cg_name_lbl;
        private System.Windows.Forms.Label num_of_vol_lbl;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label vol_index_lbl;
        private System.Windows.Forms.Label vol_percent_used_lbl;
        private System.Windows.Forms.PictureBox vol_pic;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}