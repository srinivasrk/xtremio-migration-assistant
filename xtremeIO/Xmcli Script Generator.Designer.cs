﻿namespace xtremeIO
{
    partial class Xmcli_Script_Generator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ok_btn = new System.Windows.Forms.Button();
            this.cancel_btn = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.auto_tagging_no = new System.Windows.Forms.CheckBox();
            this.auto_tagging_yes = new System.Windows.Forms.CheckBox();
            this.own_cluster_checkbox = new System.Windows.Forms.CheckBox();
            this.cluster_name_txt = new System.Windows.Forms.TextBox();
            this.cluster_name_lbl = new System.Windows.Forms.Label();
            this.clusterList = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkCyan;
            this.label1.Location = new System.Drawing.Point(6, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 19);
            this.label1.TabIndex = 54;
            this.label1.Text = "Select the Cluster :";
            // 
            // ok_btn
            // 
            this.ok_btn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ok_btn.BackColor = System.Drawing.SystemColors.Window;
            this.ok_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ok_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ok_btn.ForeColor = System.Drawing.Color.DarkGreen;
            this.ok_btn.Location = new System.Drawing.Point(160, 269);
            this.ok_btn.Name = "ok_btn";
            this.ok_btn.Size = new System.Drawing.Size(71, 32);
            this.ok_btn.TabIndex = 59;
            this.ok_btn.Text = "OK";
            this.ok_btn.UseVisualStyleBackColor = false;
            this.ok_btn.Click += new System.EventHandler(this.ok_btn_Click);
            // 
            // cancel_btn
            // 
            this.cancel_btn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.cancel_btn.BackColor = System.Drawing.SystemColors.Window;
            this.cancel_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cancel_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancel_btn.ForeColor = System.Drawing.Color.Red;
            this.cancel_btn.Location = new System.Drawing.Point(160, 324);
            this.cancel_btn.Name = "cancel_btn";
            this.cancel_btn.Size = new System.Drawing.Size(71, 34);
            this.cancel_btn.TabIndex = 60;
            this.cancel_btn.Text = "Cancel";
            this.cancel_btn.UseVisualStyleBackColor = false;
            this.cancel_btn.Click += new System.EventHandler(this.cancel_btn_Click);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.DarkCyan;
            this.label6.Location = new System.Drawing.Point(6, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(176, 19);
            this.label6.TabIndex = 67;
            this.label6.Text = "Generate Auto Tagging ?";
            // 
            // auto_tagging_no
            // 
            this.auto_tagging_no.AutoSize = true;
            this.auto_tagging_no.Location = new System.Drawing.Point(272, 66);
            this.auto_tagging_no.Name = "auto_tagging_no";
            this.auto_tagging_no.Size = new System.Drawing.Size(40, 17);
            this.auto_tagging_no.TabIndex = 69;
            this.auto_tagging_no.Text = "No";
            this.auto_tagging_no.UseVisualStyleBackColor = true;
            this.auto_tagging_no.CheckedChanged += new System.EventHandler(this.auto_tagging_no_CheckedChanged);
            // 
            // auto_tagging_yes
            // 
            this.auto_tagging_yes.AutoSize = true;
            this.auto_tagging_yes.Location = new System.Drawing.Point(204, 66);
            this.auto_tagging_yes.Name = "auto_tagging_yes";
            this.auto_tagging_yes.Size = new System.Drawing.Size(44, 17);
            this.auto_tagging_yes.TabIndex = 68;
            this.auto_tagging_yes.Text = "Yes";
            this.auto_tagging_yes.UseVisualStyleBackColor = true;
            this.auto_tagging_yes.CheckedChanged += new System.EventHandler(this.auto_tagging_yes_CheckedChanged);
            // 
            // own_cluster_checkbox
            // 
            this.own_cluster_checkbox.AutoSize = true;
            this.own_cluster_checkbox.Location = new System.Drawing.Point(11, 177);
            this.own_cluster_checkbox.Name = "own_cluster_checkbox";
            this.own_cluster_checkbox.Size = new System.Drawing.Size(123, 17);
            this.own_cluster_checkbox.TabIndex = 70;
            this.own_cluster_checkbox.Text = "Enter Own Cluster Id";
            this.own_cluster_checkbox.UseVisualStyleBackColor = true;
            this.own_cluster_checkbox.CheckedChanged += new System.EventHandler(this.own_cluster_checkbox_CheckedChanged);
            // 
            // cluster_name_txt
            // 
            this.cluster_name_txt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cluster_name_txt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cluster_name_txt.Enabled = false;
            this.cluster_name_txt.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cluster_name_txt.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.cluster_name_txt.Location = new System.Drawing.Point(160, 200);
            this.cluster_name_txt.Name = "cluster_name_txt";
            this.cluster_name_txt.Size = new System.Drawing.Size(206, 25);
            this.cluster_name_txt.TabIndex = 71;
            // 
            // cluster_name_lbl
            // 
            this.cluster_name_lbl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cluster_name_lbl.AutoSize = true;
            this.cluster_name_lbl.Enabled = false;
            this.cluster_name_lbl.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cluster_name_lbl.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.cluster_name_lbl.Location = new System.Drawing.Point(43, 206);
            this.cluster_name_lbl.Name = "cluster_name_lbl";
            this.cluster_name_lbl.Size = new System.Drawing.Size(111, 19);
            this.cluster_name_lbl.TabIndex = 72;
            this.cluster_name_lbl.Text = "Cluster Name :";
            // 
            // clusterList
            // 
            this.clusterList.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clusterList.ForeColor = System.Drawing.Color.Blue;
            this.clusterList.FormattingEnabled = true;
            this.clusterList.Location = new System.Drawing.Point(149, 119);
            this.clusterList.Name = "clusterList";
            this.clusterList.Size = new System.Drawing.Size(206, 27);
            this.clusterList.TabIndex = 73;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(3, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(223, 19);
            this.label4.TabIndex = 74;
            this.label4.Text = "Click on Ok to Generate Scripts";
            // 
            // Xmcli_Script_Generator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(398, 408);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.clusterList);
            this.Controls.Add(this.cluster_name_lbl);
            this.Controls.Add(this.cluster_name_txt);
            this.Controls.Add(this.own_cluster_checkbox);
            this.Controls.Add(this.auto_tagging_no);
            this.Controls.Add(this.auto_tagging_yes);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cancel_btn);
            this.Controls.Add(this.ok_btn);
            this.Controls.Add(this.label1);
            this.Name = "Xmcli_Script_Generator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Xmcli Script Generator";
            this.Load += new System.EventHandler(this.Xmcli_Script_Generator_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ok_btn;
        private System.Windows.Forms.Button cancel_btn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox auto_tagging_no;
        private System.Windows.Forms.CheckBox auto_tagging_yes;
        private System.Windows.Forms.CheckBox own_cluster_checkbox;
        private System.Windows.Forms.TextBox cluster_name_txt;
        private System.Windows.Forms.Label cluster_name_lbl;
        private System.Windows.Forms.ComboBox clusterList;
        private System.Windows.Forms.Label label4;

    }
}