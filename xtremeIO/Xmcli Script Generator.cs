﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;
using System.IO;
using System.Text.RegularExpressions;
namespace xtremeIO
{
    public partial class Xmcli_Script_Generator : Form
    {
        private SQLiteConnection m_dbConnection;
      
        private DataTable host_details;
        private DataTable lun_details;
        Script_Generator_landingpage parentForm;
        DataGridView sourceDataGridView;
        Dictionary<String, String> volume_map_details = new Dictionary<string, string>();
        private string clusterid;
        public Xmcli_Script_Generator(SQLiteConnection params1 , Script_Generator_landingpage params2, DataGridView params3)
        {
            InitializeComponent();
            parentForm = params2;
            m_dbConnection = params1;
            sourceDataGridView = params3;
        }

        private void Xmcli_Script_Generator_Load(object sender, EventArgs e)
        {
            auto_tagging_yes.Checked = true;
            auto_tagging_no.Checked = false;
           

            string sql = "select clustername from clusterdetails order by clustername";
            SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                clusterList.Items.Add(dr[0].ToString());
            }
            clusterList.SelectedIndex = 0;
        }

        private void auto_tagging_yes_CheckedChanged(object sender, EventArgs e)
        {
            auto_tagging_no.Checked = !auto_tagging_yes.Checked;
        }

        private void auto_tagging_no_CheckedChanged(object sender, EventArgs e)
        {
            auto_tagging_yes.Checked = !auto_tagging_no.Checked;
        }


        private void cancel_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ok_btn_Click(object sender, EventArgs e)
        {
            ok_btn.Enabled = false;
            if (cluster_name_txt.Enabled == true)
            {
                Regex regex = new Regex(@"^[a-zA-Z0-9_@.-]*$");
                Match match = regex.Match(cluster_name_txt.Text);
                if (!match.Success)
                {
                    MessageBox.Show("Cluster name cannot contain '_' '/' or '=' ");
                }
                else
                {
                    if (auto_tagging_yes.Checked)
                    {
                        generateTagScript();
                    }
                    generateVolumeScript();
                    generateIGcreationScript();
                    generateLunMappingScript();

                    this.Close();
                }
            }

            else
            {
                if (auto_tagging_yes.Checked)
                {
                    generateTagScript();
                }
                generateVolumeScript();
                generateIGcreationScript();
                generateLunMappingScript();

                this.Close();
            }

            //string tagCreation = "create-tag tag-name=<tagname> entity=Volume";
            //string volCreation = @"add-volume vol-name=<volname> vol-size=<size> cluster-id=<clusterid>
          //                          tag-id=[<tag1>,<tag2>]";
           // string igCreation = "add-initiator-group ig-name=<name> cluster-id=<cluster-id>";
          //  string addInitiator = @"add-initiator ig-id=<igname> initiator-name=<ini-name> operation-system=<os> 
           //                         port-address=<wwpn> cluster-id=<clusterid>";
         //   string mapLuns = @"map-lun ig-id=<igname> vol-id=<vol-name> cluster-id=<cluster-name>";


            
        }

        private void generateLunMappingScript()
        {
            if (own_cluster_checkbox.Checked)
            {
                clusterid = cluster_name_txt.Text;
            }
            else
            {
                clusterid = clusterList.SelectedItem.ToString();
            }
            try
            {
                string volPath = @"c:\\XIOLogAnalyzer\\" + parentForm.san_id + "- Mapping Script.txt";

                if (File.Exists(volPath))
                {
                    DialogResult exists = MessageBox.Show("The following script already exists " + volPath + ". Do you want to override it ?", "File Exists!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    if (exists == DialogResult.Yes)
                    {
                        try
                        {
                            TextWriter tw = new StreamWriter(volPath);
                            foreach (string s in volume_map_details.Keys)
                            {
                                string user_cluster_name = volume_map_details[s];
                                string[] temp = user_cluster_name.Split('|');
                                if (temp.Length > 1)
                                {
                                    foreach (string s1 in temp)
                                    {
                                        tw.WriteLine("map-lun ig-id=\"" + s1 + "\" vol-id=\"" + s + "\" cluster-id=\"" + clusterid + "\"");
                                    }
                                }
                                else
                                {
                                    tw.WriteLine("map-lun ig-id=\"" + temp[0] + "\" vol-id=\"" + s + "\" cluster-id=\"" + clusterid + "\"");
                                }
                            }
                            tw.Close();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message + ex.StackTrace);
                        }
                    }
                }
                else
                {
                    try
                    {
                        TextWriter tw = new StreamWriter(volPath);
                        foreach (string s in volume_map_details.Keys)
                        {
                            string user_cluster_name = volume_map_details[s];
                            string[] temp = user_cluster_name.Split('|');
                            if (temp.Length > 1)
                            {
                                foreach (string s1 in temp)
                                {
                                    tw.WriteLine("map-lun ig-id=\"" + s1 + "\" vol-id=\"" + s + "\" cluster-id=\"" + clusterid + "\"");
                                }
                            }
                            else
                            {
                                tw.WriteLine("map-lun ig-id=\"" + temp[0] + "\" vol-id=\"" + s + "\" cluster-id=\"" + clusterid + "\"");
                            }
                        }
                        tw.Close();
                        MessageBox.Show("Scripts Generated are stored at c:\\XIOLogAnalyzer");
                       
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + ex.StackTrace);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " " + ex.StackTrace);
            }

            
        }

        private void generateIGcreationScript()
        {
            if (own_cluster_checkbox.Checked)
            {
                clusterid = cluster_name_txt.Text;
            }
            else
            {
                clusterid = clusterList.SelectedItem.ToString();
            }
            try
            {

                string volPath = @"c:\\XIOLogAnalyzer\\" + parentForm.san_id + "-IG Creation Script.txt";

                if (File.Exists(volPath))
                {
                    DialogResult exists = MessageBox.Show("The following script already exists " + volPath + ". Do you want to override it ?", "File Exists!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    if (exists == DialogResult.Yes)
                    {
                        try
                        {
                            TextWriter tw = new StreamWriter(volPath);
                            for (int i = 0; i <= parentForm.host_details_table.Rows.Count - 1; i++)
                            {
                                if (parentForm.host_details_table.Rows[i][0].ToString().Trim().Length > 0)
                                {

                                    tw.WriteLine("add-initiator-group ig-name=\"" + parentForm.host_details_table.Rows[i][0].ToString().Trim() + "\" cluster-id=\"" + clusterid + "\"");
                                }
                                if (parentForm.host_details_table.Rows[i][2].ToString().Trim().Length > 0)
                                {
                                    string[] temp = parentForm.host_details_table.Rows[i][2].ToString().Split('\n');
                                    if (temp.Length > 1)
                                    {
                                        foreach (string port_addr in temp)
                                        {
                                            string[] osname = parentForm.host_details_table.Rows[i][1].ToString().Split('|');
                                            tw.WriteLine("add-initiator ig-id=\"" + parentForm.host_details_table.Rows[i][0] + "\" operating-system=\"" + osname[0].Trim() + "\" port-address=\"" + port_addr + "\" cluster-id=\"" + clusterid + "\"");
                                        }
                                    }
                                    else
                                    {
                                        string[] osname = parentForm.host_details_table.Rows[i][1].ToString().Split('|');
                                        tw.WriteLine("add-initiator ig-id=\"" + parentForm.host_details_table.Rows[i][0] + "\" operating-system=\"" + osname[0].Trim() + "\" port-address=\"" + parentForm.host_details_table.Rows[i][2] + "\" cluster-id=\"" + clusterid + "\"");
                                    }
                                }

                            }
                            tw.Close();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message + ex.StackTrace);
                        }
                    }
                }
                else
                {
                    try
                    {
                        TextWriter tw = new StreamWriter(volPath);
                        for (int i = 0; i <= parentForm.host_details_table.Rows.Count - 1; i++)
                        {
                            if (parentForm.host_details_table.Rows[i][0].ToString().Trim().Length > 0)
                            {

                                tw.WriteLine("add-initiator-group ig-name=\"" + parentForm.host_details_table.Rows[i][0].ToString().Trim() + "\" cluster-id=\"" + clusterList.SelectedItem.ToString() + "\"");
                            }
                            if (parentForm.host_details_table.Rows[i][2].ToString().Trim().Length > 0)
                            {
                                string[] temp = parentForm.host_details_table.Rows[i][2].ToString().Split('\n');
                                if (temp.Length > 1)
                                {
                                    foreach (string port_addr in temp)
                                    {
                                        string[] osname = parentForm.host_details_table.Rows[i][1].ToString().Split('|');
                                        tw.WriteLine("add-initiator ig-id=\"" + parentForm.host_details_table.Rows[i][0] + "\" operating-system=\"" + osname[0].Trim() + "\" port-address=\"" + port_addr + "\" cluster-id=\"" + clusterid + "\"");
                                    }
                                }
                                else
                                {
                                    string[] osname = parentForm.host_details_table.Rows[i][1].ToString().Split('|');
                                    tw.WriteLine("add-initiator ig-id=\"" + parentForm.host_details_table.Rows[i][0] + "\" operating-system=\"" + osname[0].Trim() + "\" port-address=\"" + parentForm.host_details_table.Rows[i][2] + "\" cluster-id=\"" + clusterid + "\"");
                                }
                            }

                        }
                        tw.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + ex.StackTrace);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " " + ex.StackTrace);
            }
        }

        private void generateVolumeScript()
        {
            if (own_cluster_checkbox.Checked)
            {
                clusterid = cluster_name_txt.Text;
            }
            else
            {
                clusterid = clusterList.SelectedItem.ToString();
            }
            try
            {
                string volPath = @"c:\\XIOLogAnalyzer\\" + parentForm.san_id + "-VolumeScript.txt";

                if (File.Exists(volPath))
                {
                    DialogResult exists = MessageBox.Show("The following script already exists " + volPath + ". Do you want to override it ?", "File Exists!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    if (exists == DialogResult.Yes)
                    {
                        try
                        {
                            TextWriter tw = new StreamWriter(volPath);
                            if (auto_tagging_yes.Checked)
                            {
                                for (int i = 0; i < sourceDataGridView.Rows.Count - 1; i++)
                                {
                                    if (sourceDataGridView[8, i].Value.ToString().Trim().Length > 0)
                                    {
                                        if (parentForm.vol_name_tagging.ContainsKey(sourceDataGridView[8, i].Value.ToString()))
                                        {

                                            tw.WriteLine("add-volume vol-name=\"" + sourceDataGridView[8, i].Value.ToString() + "\" vol-size=\"" + sourceDataGridView[4, i].Value.ToString() + "k\" cluster-id=\"" + clusterid + "\" tag-list=[/Volume/" + parentForm.vol_name_tagging[sourceDataGridView[8, i].Value.ToString().Trim()] + "]");
                                            volume_map_details.Add(sourceDataGridView[8, i].Value.ToString(), parentForm.vol_name_tagging[sourceDataGridView[8, i].Value.ToString().Trim()]);
                                        }
                                    }

                                }
                            }
                            else
                            {
                                for (int i = 0; i < sourceDataGridView.Rows.Count - 1; i++)
                                {
                                    if (sourceDataGridView[8, i].Value.ToString().Trim().Length > 0)
                                    {
                                        if (parentForm.vol_name_tagging.ContainsKey(sourceDataGridView[8, i].Value.ToString()))
                                        {

                                            tw.WriteLine("add-volume vol-name=\"" + sourceDataGridView[8, i].Value.ToString() + "\" vol-size=\"" + sourceDataGridView[4, i].Value.ToString() + "k\" cluster-id=\"" + clusterid + "\" ");
                                            volume_map_details.Add(sourceDataGridView[8, i].Value.ToString(), parentForm.vol_name_tagging[sourceDataGridView[8, i].Value.ToString().Trim()]);
                                        }
                                    }

                                }
                            }
                            tw.Close();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message + ex.StackTrace);
                        }
                    }
                }
                else
                {
                    try
                    {
                        TextWriter tw = new StreamWriter(volPath);
                        for (int i = 0; i < sourceDataGridView.Rows.Count - 1; i++)
                        {
                            if (sourceDataGridView[8, i].Value.ToString().Trim().Length > 0)
                            {
                                if (parentForm.vol_name_tagging.ContainsKey(sourceDataGridView[8, i].Value.ToString()))
                                {
                                    tw.WriteLine("add-volume vol-name=\"" + sourceDataGridView[8, i].Value.ToString() + "\" vol-size=\"" + sourceDataGridView[4, i].Value.ToString() + "k\" cluster-id=\"" + clusterid + "\" tag-list=[/Volume/" + parentForm.vol_name_tagging[sourceDataGridView[8, i].Value.ToString().Trim()] + "]");
                                    volume_map_details.Add(sourceDataGridView[8, i].Value.ToString(), parentForm.vol_name_tagging[sourceDataGridView[8, i].Value.ToString().Trim()]);
                                }
                            }

                        }
                        tw.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + ex.StackTrace);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " " + ex.StackTrace);
            }
        }

        private void generateTagScript()
        {
            try
            {
                string tagPath = @"c:\\XIOLogAnalyzer\\" + parentForm.san_id + "-TagScript.txt";
                if (File.Exists(tagPath))
                {
                    DialogResult exists = MessageBox.Show("The following script already exists " + tagPath + ". Do you want to override it ?", "File Exists!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    if (exists == DialogResult.Yes)
                    {
                        TextWriter tw = new StreamWriter(tagPath);
                        foreach (String s in parentForm.cluster_vol_index.Keys)
                        {
                            tw.WriteLine("create-tag tag-name=\"" + s + "\" entity=\"Volume\"");

                        }
                        tw.Close();

                    }
                    else
                    {


                    }
                }
                else
                {
                    TextWriter tw = new StreamWriter(tagPath);
                    foreach (String s in parentForm.cluster_vol_index.Keys)
                    {
                        tw.WriteLine("create-tag tag-name=\"" + s + "\" entity=\"Volume\"");

                    }
                    tw.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " " + ex.StackTrace);
            }
        }

        private void own_cluster_checkbox_CheckedChanged(object sender, EventArgs e)
        {
            if (own_cluster_checkbox.Checked)
            {
                cluster_name_lbl.Enabled = true;
                cluster_name_txt.Enabled = true;
                cluster_name_lbl.ForeColor = Color.Aqua;
                cluster_name_txt.ForeColor = Color.Black;
                clusterList.Enabled = false;
            }
            else
            {
                cluster_name_lbl.Enabled = false;
                cluster_name_txt.Enabled = false;
                cluster_name_lbl.ForeColor = Color.Gray;
                cluster_name_txt.ForeColor = Color.Gray;
                clusterList.Enabled = true;
            }
        }

        

       
    }
}
