﻿namespace xtremeIO
{
    partial class Xms_Information
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.phy_vol_consumed_lbl = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.xms_ntp_lbl = new System.Windows.Forms.Label();
            this.xms_ip_lbl = new System.Windows.Forms.Label();
            this.xms_subnet_ip = new System.Windows.Forms.Label();
            this.xms_os_lbl = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.dedup_ratio = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.compression_ratio = new System.Windows.Forms.Label();
            this.logical_vol_lbl = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.xms_platform_lbl = new System.Windows.Forms.Label();
            this.xms_gateway_lbl = new System.Windows.Forms.Label();
            this.logical_vol_pic = new System.Windows.Forms.PictureBox();
            this.logical = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.phy_vol_pic = new System.Windows.Forms.PictureBox();
            this.phy = new System.Windows.Forms.PictureBox();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logical_vol_pic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logical)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phy_vol_pic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phy)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.pictureBox2);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.pictureBox1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1008, 105);
            this.groupBox3.TabIndex = 26;
            this.groupBox3.TabStop = false;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(883, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 20);
            this.label4.TabIndex = 31;
            this.label4.Text = "Beta Version";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Image = global::xtremeIO.Properties.Resources.dell_EMC_logo;
            this.pictureBox2.Location = new System.Drawing.Point(869, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Padding = new System.Windows.Forms.Padding(5);
            this.pictureBox2.Size = new System.Drawing.Size(139, 83);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 30;
            this.pictureBox2.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.label2.Location = new System.Drawing.Point(-4, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(163, 20);
            this.label2.TabIndex = 23;
            this.label2.Text = "Migration Assistant";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.Font = new System.Drawing.Font("Perpetua Titling MT", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(327, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(369, 24);
            this.label1.TabIndex = 22;
            this.label1.Text = "XtremIO Migration Assistant";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::xtremeIO.Properties.Resources.XtremIO_logo;
            this.pictureBox1.Location = new System.Drawing.Point(12, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Padding = new System.Windows.Forms.Padding(5);
            this.pictureBox1.Size = new System.Drawing.Size(127, 83);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.label5.Location = new System.Drawing.Point(129, 432);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(206, 22);
            this.label5.TabIndex = 29;
            this.label5.Text = "Phy. Volume Consumed ";
            // 
            // phy_vol_consumed_lbl
            // 
            this.phy_vol_consumed_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.phy_vol_consumed_lbl.AutoSize = true;
            this.phy_vol_consumed_lbl.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.phy_vol_consumed_lbl.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phy_vol_consumed_lbl.ForeColor = System.Drawing.Color.Black;
            this.phy_vol_consumed_lbl.Location = new System.Drawing.Point(453, 399);
            this.phy_vol_consumed_lbl.Name = "phy_vol_consumed_lbl";
            this.phy_vol_consumed_lbl.Size = new System.Drawing.Size(0, 19);
            this.phy_vol_consumed_lbl.TabIndex = 31;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.label7.Location = new System.Drawing.Point(717, 657);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 22);
            this.label7.TabIndex = 33;
            this.label7.Text = "NTP Servers ";
            // 
            // xms_ntp_lbl
            // 
            this.xms_ntp_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.xms_ntp_lbl.AutoSize = true;
            this.xms_ntp_lbl.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xms_ntp_lbl.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xms_ntp_lbl.ForeColor = System.Drawing.Color.Black;
            this.xms_ntp_lbl.Location = new System.Drawing.Point(890, 605);
            this.xms_ntp_lbl.Name = "xms_ntp_lbl";
            this.xms_ntp_lbl.Size = new System.Drawing.Size(0, 19);
            this.xms_ntp_lbl.TabIndex = 34;
            // 
            // xms_ip_lbl
            // 
            this.xms_ip_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.xms_ip_lbl.AutoSize = true;
            this.xms_ip_lbl.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xms_ip_lbl.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xms_ip_lbl.ForeColor = System.Drawing.Color.Firebrick;
            this.xms_ip_lbl.Location = new System.Drawing.Point(448, 128);
            this.xms_ip_lbl.Name = "xms_ip_lbl";
            this.xms_ip_lbl.Size = new System.Drawing.Size(134, 19);
            this.xms_ip_lbl.TabIndex = 36;
            this.xms_ip_lbl.Text = "XMS IP Address : ";
            // 
            // xms_subnet_ip
            // 
            this.xms_subnet_ip.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.xms_subnet_ip.AutoSize = true;
            this.xms_subnet_ip.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xms_subnet_ip.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xms_subnet_ip.ForeColor = System.Drawing.Color.Firebrick;
            this.xms_subnet_ip.Location = new System.Drawing.Point(448, 166);
            this.xms_subnet_ip.Name = "xms_subnet_ip";
            this.xms_subnet_ip.Size = new System.Drawing.Size(126, 19);
            this.xms_subnet_ip.TabIndex = 37;
            this.xms_subnet_ip.Text = "XMS Subnet IP : ";
            // 
            // xms_os_lbl
            // 
            this.xms_os_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.xms_os_lbl.AutoSize = true;
            this.xms_os_lbl.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xms_os_lbl.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xms_os_lbl.ForeColor = System.Drawing.Color.Firebrick;
            this.xms_os_lbl.Location = new System.Drawing.Point(448, 204);
            this.xms_os_lbl.Name = "xms_os_lbl";
            this.xms_os_lbl.Size = new System.Drawing.Size(138, 19);
            this.xms_os_lbl.TabIndex = 38;
            this.xms_os_lbl.Text = "XMS OS Version : ";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label12.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.label12.Location = new System.Drawing.Point(61, 657);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(173, 22);
            this.label12.TabIndex = 40;
            this.label12.Text = "Deduplication Ratio";
            // 
            // dedup_ratio
            // 
            this.dedup_ratio.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dedup_ratio.AutoSize = true;
            this.dedup_ratio.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dedup_ratio.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dedup_ratio.ForeColor = System.Drawing.Color.Black;
            this.dedup_ratio.Location = new System.Drawing.Point(238, 605);
            this.dedup_ratio.Name = "dedup_ratio";
            this.dedup_ratio.Size = new System.Drawing.Size(58, 19);
            this.dedup_ratio.TabIndex = 41;
            this.dedup_ratio.Text = "<ratio>";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label14.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.label14.Location = new System.Drawing.Point(382, 657);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(167, 22);
            this.label14.TabIndex = 43;
            this.label14.Text = "Compression Ratio";
            // 
            // compression_ratio
            // 
            this.compression_ratio.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.compression_ratio.AutoSize = true;
            this.compression_ratio.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.compression_ratio.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.compression_ratio.ForeColor = System.Drawing.Color.Black;
            this.compression_ratio.Location = new System.Drawing.Point(567, 605);
            this.compression_ratio.Name = "compression_ratio";
            this.compression_ratio.Size = new System.Drawing.Size(58, 19);
            this.compression_ratio.TabIndex = 44;
            this.compression_ratio.Text = "<ratio>";
            // 
            // logical_vol_lbl
            // 
            this.logical_vol_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.logical_vol_lbl.AutoSize = true;
            this.logical_vol_lbl.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.logical_vol_lbl.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logical_vol_lbl.ForeColor = System.Drawing.Color.Black;
            this.logical_vol_lbl.Location = new System.Drawing.Point(828, 399);
            this.logical_vol_lbl.Name = "logical_vol_lbl";
            this.logical_vol_lbl.Size = new System.Drawing.Size(0, 19);
            this.logical_vol_lbl.TabIndex = 48;
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label17.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.label17.Location = new System.Drawing.Point(504, 432);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(232, 22);
            this.label17.TabIndex = 46;
            this.label17.Text = "Logical Volume Consumed ";
            // 
            // xms_platform_lbl
            // 
            this.xms_platform_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.xms_platform_lbl.AutoSize = true;
            this.xms_platform_lbl.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xms_platform_lbl.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xms_platform_lbl.ForeColor = System.Drawing.Color.Firebrick;
            this.xms_platform_lbl.Location = new System.Drawing.Point(448, 244);
            this.xms_platform_lbl.Name = "xms_platform_lbl";
            this.xms_platform_lbl.Size = new System.Drawing.Size(119, 19);
            this.xms_platform_lbl.TabIndex = 49;
            this.xms_platform_lbl.Text = "XMS Platform : ";
            // 
            // xms_gateway_lbl
            // 
            this.xms_gateway_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.xms_gateway_lbl.AutoSize = true;
            this.xms_gateway_lbl.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.xms_gateway_lbl.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xms_gateway_lbl.ForeColor = System.Drawing.Color.Firebrick;
            this.xms_gateway_lbl.Location = new System.Drawing.Point(448, 280);
            this.xms_gateway_lbl.Name = "xms_gateway_lbl";
            this.xms_gateway_lbl.Size = new System.Drawing.Size(139, 19);
            this.xms_gateway_lbl.TabIndex = 50;
            this.xms_gateway_lbl.Text = "XMS Gateway IP : ";
            // 
            // logical_vol_pic
            // 
            this.logical_vol_pic.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.logical_vol_pic.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.logical_vol_pic.Image = global::xtremeIO.Properties.Resources.progressbar_0;
            this.logical_vol_pic.Location = new System.Drawing.Point(626, 392);
            this.logical_vol_pic.Name = "logical_vol_pic";
            this.logical_vol_pic.Size = new System.Drawing.Size(196, 28);
            this.logical_vol_pic.TabIndex = 47;
            this.logical_vol_pic.TabStop = false;
            // 
            // logical
            // 
            this.logical.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.logical.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.logical.Image = global::xtremeIO.Properties.Resources.Volume_new;
            this.logical.Location = new System.Drawing.Point(535, 349);
            this.logical.Name = "logical";
            this.logical.Size = new System.Drawing.Size(85, 80);
            this.logical.TabIndex = 45;
            this.logical.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox8.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox8.Image = global::xtremeIO.Properties.Resources.Speedometer;
            this.pictureBox8.Location = new System.Drawing.Point(386, 565);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(157, 80);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox8.TabIndex = 42;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox7.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox7.Image = global::xtremeIO.Properties.Resources.Speedometer;
            this.pictureBox7.Location = new System.Drawing.Point(65, 565);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(157, 80);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 39;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox6.Image = global::xtremeIO.Properties.Resources.XtremIO_icon1;
            this.pictureBox6.Location = new System.Drawing.Point(276, 128);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(150, 171);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 35;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox5.Image = global::xtremeIO.Properties.Resources.ntp_server;
            this.pictureBox5.Location = new System.Drawing.Point(672, 591);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(212, 42);
            this.pictureBox5.TabIndex = 32;
            this.pictureBox5.TabStop = false;
            // 
            // phy_vol_pic
            // 
            this.phy_vol_pic.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.phy_vol_pic.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.phy_vol_pic.Image = global::xtremeIO.Properties.Resources.progressbar_0;
            this.phy_vol_pic.Location = new System.Drawing.Point(251, 392);
            this.phy_vol_pic.Name = "phy_vol_pic";
            this.phy_vol_pic.Size = new System.Drawing.Size(196, 28);
            this.phy_vol_pic.TabIndex = 30;
            this.phy_vol_pic.TabStop = false;
            // 
            // phy
            // 
            this.phy.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.phy.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.phy.Image = global::xtremeIO.Properties.Resources.Volume_new;
            this.phy.Location = new System.Drawing.Point(160, 349);
            this.phy.Name = "phy";
            this.phy.Size = new System.Drawing.Size(85, 80);
            this.phy.TabIndex = 28;
            this.phy.TabStop = false;
            // 
            // Xms_Information
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.xms_gateway_lbl);
            this.Controls.Add(this.xms_platform_lbl);
            this.Controls.Add(this.logical_vol_lbl);
            this.Controls.Add(this.logical_vol_pic);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.logical);
            this.Controls.Add(this.compression_ratio);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.dedup_ratio);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.xms_os_lbl);
            this.Controls.Add(this.xms_subnet_ip);
            this.Controls.Add(this.xms_ip_lbl);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.xms_ntp_lbl);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.phy_vol_consumed_lbl);
            this.Controls.Add(this.phy_vol_pic);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.phy);
            this.Controls.Add(this.groupBox3);
            this.Name = "Xms_Information";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Xms_Information";
            this.Load += new System.EventHandler(this.Xms_Information_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logical_vol_pic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logical)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phy_vol_pic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phy)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox phy;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox phy_vol_pic;
        private System.Windows.Forms.Label phy_vol_consumed_lbl;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label xms_ntp_lbl;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label xms_ip_lbl;
        private System.Windows.Forms.Label xms_subnet_ip;
        private System.Windows.Forms.Label xms_os_lbl;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label dedup_ratio;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label compression_ratio;
        private System.Windows.Forms.Label logical_vol_lbl;
        private System.Windows.Forms.PictureBox logical_vol_pic;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox logical;
        private System.Windows.Forms.Label xms_platform_lbl;
        private System.Windows.Forms.Label xms_gateway_lbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}