﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;
namespace xtremeIO
{
    public partial class Bw_IO_Reports : Form
    {
        #region Global Variables Declaration
        SQLiteConnection m_dbConnection;
        LandingPage back_instance;
        public Bw_IO_Reports(SQLiteConnection conn, string ip, string os, LandingPage back_data)
        {
            InitializeComponent();
            back_instance = back_data;
            m_dbConnection = conn;
            xms_ip_lbl.Text = ip;
            xms_os_lbl.Text = os;
        }
        #endregion
        #region Form Load
        private void Bw_IO_Reports_Load(object sender, EventArgs e)
        {
            filter_data.SelectedIndex = 0;
            go_btn_Click(null, null);
        }
        #endregion
        #region Go Button Click
        private void go_btn_Click(object sender, EventArgs e)
        {
            foreach (var series in graph_data.Series)
            {
                series.Points.Clear();
            }

            switch (filter_data.SelectedIndex)
            {
                #region Total Read IOPS
                case 0:
                    {
                        string sql = "select * from initiatorIOPS order by totalreadiops desc limit 10";
                        SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                        SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        foreach (DataRow dr in dt.Rows)
                        {
                            graph_data.Series[0].Points.AddXY(dr["initiatorname"].ToString(), dr["totalreadiops"].ToString());
                        }
                        graph_data.Series[0].LegendText = "Read IOPS";
                    }
                    break;
                #endregion
                #region Total Write IOPS
                case 1:
                    {
                        string sql = "select * from initiatorIOPS order by totalwriteiops desc limit 10";
                        SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                        SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        foreach (DataRow dr in dt.Rows)
                        {
                            graph_data.Series[0].Points.AddXY(dr["initiatorname"].ToString(), dr["totalwriteiops"].ToString());
                        }
                        graph_data.Series[0].LegendText = "Write IOPS";
                    }
                    break;
                #endregion
                #region Total Read GB
                case 2:
                    {
                        string sql = "select * from initiatorIOPS order by totalread desc limit 10";
                        SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                        SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        foreach (DataRow dr in dt.Rows)
                        {
                            graph_data.Series[0].Points.AddXY(dr["initiatorname"].ToString(), dr["totalread"].ToString());
                        }
                        graph_data.Series[0].LegendText = "Total Reads in GB";
                    }
                    break;
                #endregion
                #region Total Write GB
                case 3:
                    {
                        string sql = "select * from initiatorIOPS order by totalwrite desc limit 10";
                        SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                        SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        foreach (DataRow dr in dt.Rows)
                        {
                            graph_data.Series[0].Points.AddXY(dr["initiatorname"].ToString(), dr["totalwrite"].ToString());
                        }
                        graph_data.Series[0].LegendText = "Total Writes in GB";
                    }
                    break;
                #endregion
                #region Total bandwidth
                case 4:
                    {
                        string sql = "select * from initiatorIOPS order by totalbw desc limit 10";
                        SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                        SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        foreach (DataRow dr in dt.Rows)
                        {
                            graph_data.Series[0].Points.AddXY(dr["initiatorname"].ToString(), dr["totalbw"].ToString());
                        }
                        graph_data.Series[0].LegendText = "Total Bandwidth in MB/s";
                    }
                    break;
                #endregion
            }
        }

        #endregion

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
    }
}

