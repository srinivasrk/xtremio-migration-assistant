﻿namespace xtremeIO
{
    partial class Script_Generator_landingpage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Script_Generator_landingpage));
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.xms_ip_lbl = new System.Windows.Forms.Label();
            this.xms_os_lbl = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.total_lun_lbl = new System.Windows.Forms.Label();
            this.total_host_lbl = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.server_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.os_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wwpn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ip_addr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.disk_size = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.array_serial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.array_device = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.array_device_wwn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.target_device = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lun_mapping_btn = new System.Windows.Forms.Button();
            this.lun_details_btn = new System.Windows.Forms.Button();
            this.host_details_btn = new System.Windows.Forms.Button();
            this.from_to_btn = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.generate_script_btn = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.pictureBox2);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.xms_ip_lbl);
            this.groupBox3.Controls.Add(this.pictureBox1);
            this.groupBox3.Controls.Add(this.xms_os_lbl);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1008, 105);
            this.groupBox3.TabIndex = 29;
            this.groupBox3.TabStop = false;
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(883, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 20);
            this.label4.TabIndex = 31;
            this.label4.Text = "Beta Version";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.Control;
            this.label3.Font = new System.Drawing.Font("Perpetua Titling MT", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(484, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "Version 1.0.2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.label1.Location = new System.Drawing.Point(-1, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 20);
            this.label1.TabIndex = 23;
            this.label1.Text = "Migration Assistant";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            this.label5.Font = new System.Drawing.Font("Perpetua Titling MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(376, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(297, 20);
            this.label5.TabIndex = 21;
            this.label5.Text = "XtremIO Migration Assistant";
            // 
            // xms_ip_lbl
            // 
            this.xms_ip_lbl.AutoSize = true;
            this.xms_ip_lbl.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xms_ip_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xms_ip_lbl.ForeColor = System.Drawing.Color.Green;
            this.xms_ip_lbl.Location = new System.Drawing.Point(155, 40);
            this.xms_ip_lbl.Name = "xms_ip_lbl";
            this.xms_ip_lbl.Size = new System.Drawing.Size(144, 18);
            this.xms_ip_lbl.TabIndex = 10;
            this.xms_ip_lbl.Text = "XMS IP address : ";
            // 
            // xms_os_lbl
            // 
            this.xms_os_lbl.AutoSize = true;
            this.xms_os_lbl.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xms_os_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xms_os_lbl.ForeColor = System.Drawing.Color.Green;
            this.xms_os_lbl.Location = new System.Drawing.Point(155, 9);
            this.xms_os_lbl.Name = "xms_os_lbl";
            this.xms_os_lbl.Size = new System.Drawing.Size(148, 18);
            this.xms_os_lbl.TabIndex = 11;
            this.xms_os_lbl.Text = "XMS OS version : ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.from_to_btn);
            this.groupBox1.Controls.Add(this.total_lun_lbl);
            this.groupBox1.Controls.Add(this.pictureBox4);
            this.groupBox1.Controls.Add(this.total_host_lbl);
            this.groupBox1.Controls.Add(this.generate_script_btn);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 662);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1008, 68);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            // 
            // total_lun_lbl
            // 
            this.total_lun_lbl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.total_lun_lbl.AutoSize = true;
            this.total_lun_lbl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.total_lun_lbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total_lun_lbl.ForeColor = System.Drawing.Color.Black;
            this.total_lun_lbl.Location = new System.Drawing.Point(410, 44);
            this.total_lun_lbl.Name = "total_lun_lbl";
            this.total_lun_lbl.Size = new System.Drawing.Size(160, 17);
            this.total_lun_lbl.TabIndex = 21;
            this.total_lun_lbl.Text = "Total Number of Luns :";
            // 
            // total_host_lbl
            // 
            this.total_host_lbl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.total_host_lbl.AutoSize = true;
            this.total_host_lbl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.total_host_lbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total_host_lbl.ForeColor = System.Drawing.Color.Black;
            this.total_host_lbl.Location = new System.Drawing.Point(410, 24);
            this.total_host_lbl.Name = "total_host_lbl";
            this.total_host_lbl.Size = new System.Drawing.Size(166, 17);
            this.total_host_lbl.TabIndex = 19;
            this.total_host_lbl.Text = "Total Number of Hosts :";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 105);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lun_mapping_btn);
            this.splitContainer1.Panel1.Controls.Add(this.lun_details_btn);
            this.splitContainer1.Panel1.Controls.Add(this.host_details_btn);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dataGridView1);
            this.splitContainer1.Size = new System.Drawing.Size(1008, 557);
            this.splitContainer1.SplitterDistance = 98;
            this.splitContainer1.TabIndex = 32;
            // 
            // dataGridView1
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.server_name,
            this.os_name,
            this.wwpn,
            this.ip_addr,
            this.disk_size,
            this.array_serial,
            this.array_device,
            this.array_device_wwn,
            this.target_device});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.Size = new System.Drawing.Size(1008, 455);
            this.dataGridView1.TabIndex = 25;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            this.dataGridView1.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGridView1_CellPainting);
            // 
            // server_name
            // 
            this.server_name.DataPropertyName = "server_name";
            this.server_name.HeaderText = "Server Name";
            this.server_name.Name = "server_name";
            // 
            // os_name
            // 
            this.os_name.DataPropertyName = "os_name";
            this.os_name.HeaderText = "OS Version";
            this.os_name.Name = "os_name";
            // 
            // wwpn
            // 
            this.wwpn.DataPropertyName = "wwpn";
            this.wwpn.HeaderText = "WWPN";
            this.wwpn.Name = "wwpn";
            // 
            // ip_addr
            // 
            this.ip_addr.DataPropertyName = "ip_addr";
            this.ip_addr.HeaderText = "IP Address";
            this.ip_addr.Name = "ip_addr";
            // 
            // disk_size
            // 
            this.disk_size.DataPropertyName = "disk_size";
            this.disk_size.HeaderText = "Disk Size (KB)";
            this.disk_size.Name = "disk_size";
            // 
            // array_serial
            // 
            this.array_serial.DataPropertyName = "array_serial";
            this.array_serial.HeaderText = "Array Serial";
            this.array_serial.Name = "array_serial";
            // 
            // array_device
            // 
            this.array_device.DataPropertyName = "array_device";
            this.array_device.HeaderText = "Array Device";
            this.array_device.Name = "array_device";
            // 
            // array_device_wwn
            // 
            this.array_device_wwn.DataPropertyName = "array_device_wwn";
            this.array_device_wwn.HeaderText = "Device WWN";
            this.array_device_wwn.Name = "array_device_wwn";
            // 
            // target_device
            // 
            this.target_device.DataPropertyName = "target_device";
            this.target_device.HeaderText = "XtremIO Target Device";
            this.target_device.Name = "target_device";
            // 
            // lun_mapping_btn
            // 
            this.lun_mapping_btn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lun_mapping_btn.BackColor = System.Drawing.SystemColors.Window;
            this.lun_mapping_btn.BackgroundImage = global::xtremeIO.Properties.Resources.lun_mapping2;
            this.lun_mapping_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lun_mapping_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.lun_mapping_btn.Location = new System.Drawing.Point(644, 9);
            this.lun_mapping_btn.Name = "lun_mapping_btn";
            this.lun_mapping_btn.Size = new System.Drawing.Size(339, 74);
            this.lun_mapping_btn.TabIndex = 6;
            this.lun_mapping_btn.Text = "Show Lun Mapping/\r\nGenerate From to List\r\n";
            this.lun_mapping_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lun_mapping_btn.UseVisualStyleBackColor = false;
            this.lun_mapping_btn.Click += new System.EventHandler(this.lun_mapping_btn_Click);
            // 
            // lun_details_btn
            // 
            this.lun_details_btn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lun_details_btn.BackColor = System.Drawing.SystemColors.Window;
            this.lun_details_btn.BackgroundImage = global::xtremeIO.Properties.Resources.san_lun;
            this.lun_details_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.lun_details_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lun_details_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.lun_details_btn.Location = new System.Drawing.Point(342, 17);
            this.lun_details_btn.Name = "lun_details_btn";
            this.lun_details_btn.Size = new System.Drawing.Size(228, 59);
            this.lun_details_btn.TabIndex = 5;
            this.lun_details_btn.Text = "Show Lun Details  ";
            this.lun_details_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lun_details_btn.UseVisualStyleBackColor = false;
            this.lun_details_btn.Click += new System.EventHandler(this.lun_details_btn_Click);
            // 
            // host_details_btn
            // 
            this.host_details_btn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.host_details_btn.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.host_details_btn.BackgroundImage = global::xtremeIO.Properties.Resources.initiator_icon;
            this.host_details_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.host_details_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.host_details_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.host_details_btn.Location = new System.Drawing.Point(42, 17);
            this.host_details_btn.Name = "host_details_btn";
            this.host_details_btn.Size = new System.Drawing.Size(217, 59);
            this.host_details_btn.TabIndex = 4;
            this.host_details_btn.Text = "Show Host Details  ";
            this.host_details_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.host_details_btn.UseVisualStyleBackColor = false;
            this.host_details_btn.Click += new System.EventHandler(this.host_details_btn_Click);
            // 
            // from_to_btn
            // 
            this.from_to_btn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.from_to_btn.BackColor = System.Drawing.SystemColors.Window;
            this.from_to_btn.BackgroundImage = global::xtremeIO.Properties.Resources.generate_script1;
            this.from_to_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.from_to_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.from_to_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.from_to_btn.Location = new System.Drawing.Point(764, 6);
            this.from_to_btn.Name = "from_to_btn";
            this.from_to_btn.Size = new System.Drawing.Size(241, 56);
            this.from_to_btn.TabIndex = 22;
            this.from_to_btn.Text = "Generate From To List";
            this.from_to_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.from_to_btn.UseVisualStyleBackColor = false;
            this.from_to_btn.Visible = false;
            this.from_to_btn.Click += new System.EventHandler(this.from_to_btn_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Image = global::xtremeIO.Properties.Resources.info2;
            this.pictureBox4.Location = new System.Drawing.Point(367, 30);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Padding = new System.Windows.Forms.Padding(5);
            this.pictureBox4.Size = new System.Drawing.Size(37, 26);
            this.pictureBox4.TabIndex = 20;
            this.pictureBox4.TabStop = false;
            // 
            // generate_script_btn
            // 
            this.generate_script_btn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.generate_script_btn.BackColor = System.Drawing.SystemColors.Window;
            this.generate_script_btn.BackgroundImage = global::xtremeIO.Properties.Resources.generate_script1;
            this.generate_script_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.generate_script_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.generate_script_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.generate_script_btn.Location = new System.Drawing.Point(3, 3);
            this.generate_script_btn.Name = "generate_script_btn";
            this.generate_script_btn.Size = new System.Drawing.Size(256, 58);
            this.generate_script_btn.TabIndex = 8;
            this.generate_script_btn.Text = "Generate XMCLI Script  ";
            this.generate_script_btn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.generate_script_btn.UseVisualStyleBackColor = false;
            this.generate_script_btn.Visible = false;
            this.generate_script_btn.Click += new System.EventHandler(this.generate_script_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Image = global::xtremeIO.Properties.Resources.dell_EMC_logo;
            this.pictureBox2.Location = new System.Drawing.Point(869, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Padding = new System.Windows.Forms.Padding(5);
            this.pictureBox2.Size = new System.Drawing.Size(139, 83);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 30;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::xtremeIO.Properties.Resources.XtremIO_logo;
            this.pictureBox1.Location = new System.Drawing.Point(12, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Padding = new System.Windows.Forms.Padding(5);
            this.pictureBox1.Size = new System.Drawing.Size(127, 83);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Script_Generator_landingpage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Script_Generator_landingpage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Script Generator";
            this.Load += new System.EventHandler(this.Script_Generator_landingpage_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label xms_ip_lbl;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label xms_os_lbl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button generate_script_btn;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button lun_mapping_btn;
        private System.Windows.Forms.Button lun_details_btn;
        private System.Windows.Forms.Button host_details_btn;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label total_lun_lbl;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label total_host_lbl;
        private System.Windows.Forms.Button from_to_btn;
        private System.Windows.Forms.DataGridViewTextBoxColumn server_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn os_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn wwpn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ip_addr;
        private System.Windows.Forms.DataGridViewTextBoxColumn disk_size;
        private System.Windows.Forms.DataGridViewTextBoxColumn array_serial;
        private System.Windows.Forms.DataGridViewTextBoxColumn array_device;
        private System.Windows.Forms.DataGridViewTextBoxColumn array_device_wwn;
        private System.Windows.Forms.DataGridViewTextBoxColumn target_device;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}