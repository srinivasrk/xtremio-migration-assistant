﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;
namespace xtremeIO
{
    public partial class san_search : Form
    {
        private SQLiteConnection m_dbConnection;
        private String xms_ip_addr, xms_os_data;
        private List<String> tablenames = new List<String>();
        public san_search(SQLiteConnection conn , string params1 , string params2)
        {
            InitializeComponent();
            m_dbConnection = conn;
            xms_ip_addr = params1;
            xms_os_data = params2;
        }

        private void san_search_Load(object sender, EventArgs e)
        {
            string sql = "SELECT name FROM sqlite_master WHERE type='table' and name like 'SAN%'";
            SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                String temp = dr[0].ToString();
                int index = -1;
                index = temp.IndexOf("Host");
                
                if (index != -1)
                {
                    temp = dr[0].ToString().Substring(4, index -1 - 4);
                    if (!tablenames.Contains(temp))
                    {
                        tablenames.Add(temp);
                    }
                }
            }
            foreach (String s in tablenames)
            {
                if(!san_id_text.Items.Contains(s))
                {
                    san_id_text.Items.Add(s);
                }
            }
            san_id_text.SelectedItem = san_id_text.Items[0];
        }

        private void ok_btn_Click(object sender, EventArgs e)
        {
            
            Script_Generator_landingpage form1 = new Script_Generator_landingpage((san_id_text.Text), m_dbConnection, xms_ip_addr, xms_os_data);
            form1.ShowDialog();
            this.Close();
        }

        private void cancel_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
