﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Data.SQLite;
using System.Security.Cryptography;
namespace xtremeIO
{
    public partial class Home_Page : Form
    {
        #region Global Variable Declaration
        public int success = -1;
        // Global Variable for this form only
        BackgroundWorker bw;
        private const int Keysize = 256;
        private const int DerivationIterations = 1000;

        List<double> freeclusterphyspaceGB = new List<double>();
        List<double> freeclusterlogicaspaceGB = new List<double>();
        List<double> clustertotalwrites = new List<double>();
        List<double> clustertotalreads = new List<double>();
        List<string> cgMappingName = new List<string>();
        List<string> cgMappingVolume = new List<string>();
        List<string> cgIndex = new List<string>();
        List<string> cgnumofVol = new List<string>();
        List<string> cgClusterName = new List<string>();
        List<string> cgName = new List<string>();
        List<string> xmsName = new List<string>();
        List<string> xmsIP = new List<string>();
        List<string> xmsSubnet = new List<string>();
        List<string> xmsPlatform = new List<string>();
        List<string> xmsGateway = new List<string>();
        List<string> xmsThinProvision = new List<string>();
        List<string> xmsNumOfCluster = new List<string>();
        List<string> xmsnumOfVols = new List<string>();
        List<string> xmsNumOfIG = new List<string>();
        List<string> xmsNTP = new List<string>();
        List<string> xmsLogicalfreepercent = new List<string>();
        List<string> xmsPhyFreepercent = new List<string>();
        List<string> xmsPhyFree = new List<string>();
        List<string> xmsLogicalFree = new List<string>();
        List<string> xmsDedup = new List<string>();
        List<string> xmsclusterTime = new List<string>();
        List<string> xmsDatareduction = new List<string>();
        List<string> xmsCompression = new List<string>();
        List<string> xmsOverallEff = new List<string>();
        List<string> xmsOS = new List<string>();
        List<string> clusterName = new List<string>();
        List<string> initiatorGroupData = new List<string>();
        List<double> volsizeinGB = new List<double>();
        List<string> LBNotation = new List<string>();
        List<string> volClusterName = new List<string>();
        List<string> volumename = new List<string>();
        List<string> volsize = new List<string>();
        List<string> clusterIndex = new List<string>();
        List<string> psntNumber = new List<string>();
        List<string> numOfVol = new List<string>();
        List<string> numOfBricks = new List<string>();
        List<string> sizeCap = new List<string>();
        List<string> dedupRatio = new List<string>();
        List<string> freephySpace = new List<string>();
        List<string> freelogSpace = new List<string>();
        List<string> swVersion = new List<string>();
        List<string> thinProvision = new List<string>();
        List<string> volIndex = new List<string>();
        List<string> lbSize = new List<string>();
        List<double> volSpaceused = new List<double>();
        List<string> createdFrom = new List<string>();
        List<string> smallIO = new List<string>();
        List<string> unalignedIO = new List<string>();
        List<string> vaaiTP = new List<string>();
        List<string> naaID = new List<string>();
        List<string> creationTime = new List<string>();
        List<string> initiatorOS = new List<string>();
        List<string> initiatorGroupName = new List<string>();
        List<string> portAddr = new List<string>();
        List<string> portType = new List<string>();
        List<string> initiatorClusterName = new List<string>();
        List<string> initiatorIndex = new List<string>();
        List<string> initiatorName = new List<string>();
        List<string> allScNames = new List<string>();
        List<string> scClusterName = new List<string>();
        List<string> scmgmtIPaddrA = new List<string>();
        List<string> scmgmtIPaddrB = new List<string>();
        List<string> scmgmtIP = new List<string>();
        List<string> schealthState = new List<string>();
        List<string> scgatewayIP = new List<string>();
        List<string> scsubnetAddr = new List<string>();
        List<string> scIndex = new List<string>();
        List<string> lunMappingData = new List<string>();
        List<string> alertCode = new List<string>();
        List<string> alertIndex = new List<string>();
        List<string> alertState = new List<string>();
        List<string> alertType = new List<string>();
        List<string> alertentityData = new List<string>();
        List<string> alertentityName = new List<string>();
        List<string> alertDesc = new List<string>();
        List<string> objectSev = new List<string>();
        List<string> raiseTime = new List<string>();
        List<string> alertclusterName = new List<string>();
        List<string> targetName = new List<string>();
        List<string> targetIndex = new List<string>();
        List<string> targetjumboFrames = new List<string>();
        List<string> targetmtuData = new List<string>();
        List<string> targetportAddress = new List<string>();
        List<string> targetmacAddress = new List<string>();
        List<string> targetportSpeed = new List<string>();
        List<string> targetportType = new List<string>();
        List<string> targetscClusterName = new List<string>();
        List<string> targetportState = new List<string>();
        List<string> lunMappingVolmue = new List<string>();
        List<string> connectedIni = new List<string>();
        List<string> connectedTarget = new List<string>();
        List<string> initiatorIOPSName = new List<string>();
        List<double> totalReadIO = new List<double>();
        List<double> totalWriteIO = new List<double>();
        List<double> totalread = new List<double>();
        List<double> totalwrites = new List<double>();
        List<double> totalbandwidth = new List<double>();

        // Global Connection , Closed when from is closed
        private SQLiteConnection m_dbConnection;

        // To check if all data has been received ! Work-Around for 2.x and 3.x
        private int filled=0;
        public Home_Page()
        {
            //Encrypt the log file in C:/XIOLogAnalyzer

            InitializeComponent();
            RijndaelManaged rm = new RijndaelManaged();
            encryptor = rm.CreateEncryptor(key, vector);
            decryptor = rm.CreateDecryptor(key, vector);
            encoder = new UTF8Encoding();
        }
        #endregion
        #region Create new Project
        private void button1_Click(object sender, EventArgs e)
        {
            load_button.Enabled = false;
            delete_button.Enabled = false;
           // load_button.ForeColor = Color.Silver;
            load_button.BackColor = Color.FromArgb(100, Color.Lavender);
           // delete_button.ForeColor = Color.Silver;
            delete_button.BackColor = Color.FromArgb(100, Color.Lavender);
            cluster1_lbl.Visible = true;
            cluster1_text.Visible = true;
            upload_pic_1.Visible = true;
            info_lbl.Visible = true;
            upload_logs_1.Visible = true;
            proj_id_lbl.Visible = true;
            project_id_text.Visible = true;
            
            ok_btn.Visible = true;
            cancel_btn.Visible = true;
        }
        #endregion
        #region Check if application is expired

            private static byte[] key = { 123, 217, 19, 11, 24, 26, 85, 45, 114, 184, 27, 162, 37, 112, 222, 209, 241, 24, 175, 144, 173, 53, 196, 29, 24, 26, 17, 218, 131, 236, 53, 209 };
            private static byte[] vector = { 146, 64, 191, 111, 23, 3, 113, 119, 231, 121, 221, 112, 79, 32, 114, 156 };
            private ICryptoTransform encryptor, decryptor;
            private UTF8Encoding encoder;

            public string Encrypt(string unencrypted)
            {
                return Convert.ToBase64String(Encrypt(encoder.GetBytes(unencrypted)));
            }

            public string Decrypt(string encrypted)
            {
                return encoder.GetString(Decrypt(Convert.FromBase64String(encrypted)));
            }

            public byte[] Encrypt(byte[] buffer)
            {
                return Transform(buffer, encryptor);
            }

            public byte[] Decrypt(byte[] buffer)
            {
                return Transform(buffer, decryptor);
            }

            protected byte[] Transform(byte[] buffer, ICryptoTransform transform)
            {
                MemoryStream stream = new MemoryStream();
                using (CryptoStream cs = new CryptoStream(stream, transform, CryptoStreamMode.Write))
                {
                    cs.Write(buffer, 0, buffer.Length);
                }
                return stream.ToArray();
            }

                private void CheckExpiration()
                {
                    // Check if the log file exists

                    string logpath = "c:/XIOLogAnalyzer/XLA.log";
                    if (System.IO.File.Exists(logpath))
                    {
                        // If log File exists check the log date and current system date

                        string[] temp = File.ReadAllLines(logpath);
                        string decryptedString = Decrypt(temp[0].ToString());
                        string[] logdate = decryptedString.Split(' ');
                       /* if(logdate[0].ToString().Equals(System.DateTime.Today.ToString().Split(' ')[0]))
                        {
                            MessageBox.Show("Success");
                        }*/
                        DateTime d1 = DateTime.Parse(logdate[0]);
                       
                        DateTime d2 = DateTime.Today;
                        if (d2.Date >= d1.AddDays(30))
                        {
                            MessageBox.Show("The Application has expired after 30 day use. Please contact administrator for renewal", "XtremIO Log Analyzer - Expired !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            System.Environment.Exit(0);
                        }
                       
                    }
                    else
                    {
                        TextWriter tw = new StreamWriter(logpath, true);
                        tw.WriteLine(Encrypt(System.DateTime.Today.ToString()));
                        tw.Close();
                    }

                }

        #endregion
        #region Create Ok Click
        private void ok_btn_Click(object sender, EventArgs e)
        {
            //Create project

            percent_completed.Visible = true;
            ok_btn.Enabled = false;
          
            if (!(project_id_text.Text.Length >= 5))
            {
                MessageBox.Show("Please enter atleast 5 letters or digits for Project ID", "Failed to create Project !!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ok_btn.Enabled = true;
            }
            else if (File.Exists("C:\\XIOLogAnalyzer\\" + project_id_text.Text + ".sqlite"))
            {
                MessageBox.Show("The project with ID " + project_id_text.Text + " Already exists !", "Failed to create Project", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ok_btn.Enabled = true;
            }

            else if (success == -1)
            {
                MessageBox.Show("Please upload the logs and then proceed", "Error- No logs found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ok_btn.Enabled = true;
            }
            else
            {
                cancel_btn.Enabled = false;
                createDatabase();
            }
        }
        #endregion
        #region Creating Database
        private void createDatabase()
        {
            // VERY BIG METHOD -- TODO Break down to modules !!!!!
        /*
         * You may think you know what the following code does.
         * But you dont. Trust me.
         * Fiddle with it, and you'll spend many a sleepless nights
         */ 
            try
            {
                #region Create Database file
                percent_completed.Visible = true;
                bw = new BackgroundWorker();
                bw.WorkerReportsProgress = true;
                bw.WorkerSupportsCancellation = true;

                //BG TASK Started on new thread , phew!

                bw.DoWork += new DoWorkEventHandler(
                delegate(object o, DoWorkEventArgs args)
                {
                    BackgroundWorker b = o as BackgroundWorker;
                   
                    SQLiteConnection.CreateFile("C:\\XIOLogAnalyzer\\" + project_id_text.Text + ".sqlite");
                    m_dbConnection = new SQLiteConnection("Data Source=C:\\XIOLogAnalyzer\\" + project_id_text.Text + ".sqlite;Version=3;");
                    m_dbConnection.Open();
                #endregion
                #region XMS Info Database

                    // Create XMS info table , Primary table. Blood and bone

                    string sql = @"create table xmsinfo (xmsname varchar(100), xmsip varchar(30),xmssubnet varchar(30),xmsos varchar(20) ,xmsplatform varchar(100),xmsgateway varchar(30) ,
                dedupratio varchar(10) , clustertime varchar(30)
                ,compressionfactor varchar(10) , freelogicalspace varchar(20) , freephyspace varchar(20) , freelogicalpercent varchar(10), freephypercent varchar(10),
                ntpservers varchar(100),numofig varchar(10) , numofcluster varchar(10), numofvol varchar(10) , overallefficiency varchar(20),
                thinprovsaving varchar(10))";

                    SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                    command.ExecuteNonQuery().ToString();

                    if (bw.CancellationPending == true)
                    {
                        args.Cancel = true;
                        MessageBox.Show("Project creation cancelled..","Cancelled!!",MessageBoxButtons.OK,MessageBoxIcon.Information);
                        return; 
                        
                        // abort work, if it's cancelled

                    }
                    b.ReportProgress(10);

                    // Read Show All XMS file.

                    string path = folderBrowserDialog1.SelectedPath + "/xms/xmcli/ShowAllXms.out";
                    if (File.Exists(path))
                    {
                        // This is the only way to parse , Dont go gentle into that good night !!
                        string[] data = System.IO.File.ReadAllLines(path);
                        for (int i = 0; i < data.Length; i++)
                        {
                            if (data[i].StartsWith("Name:") && data[i + 1].StartsWith("Index:"))
                            {
                                //check number of rows , this If block will anyway execute since each row should have a NAME and Index
                                filled++;
                                int len = data[i].Length - 5;
                                xmsName.Add(data[i].Substring(5, len).Trim());

                            }
                            if (data[i].StartsWith("SW-Version:") || data[i].StartsWith("Version:"))
                            {
                                if (data[i].StartsWith("Version:"))
                                {
                                    //2.x
                                    int len = data[i].Length - 8;
                                    xmsOS.Add(data[i].Substring(8, len).Trim());
                                }
                                else
                                {
                                    int len = data[i].Length - 11;
                                    xmsOS.Add(data[i].Substring(11, len).Trim());
                                }
                            }

                            if (data[i].StartsWith("Xms-IP-Addr:"))
                            {
                                //next line of dedup ratio
                                int len = data[i].Length - 12;
                                xmsIP.Add(data[i].Substring(12, len).Trim());
                            }
                            if (data[i].StartsWith("Xms-IP-Addr-Subnet:"))
                            {
                                int len = data[i].Length - 19;
                                xmsSubnet.Add(data[i].Substring(19, len).Trim());

                            }
                            if (data[i].StartsWith("XMS-Platform:"))
                            {
                                int len = data[i].Length - 13;
                                xmsPlatform.Add(data[i].Substring(13, len).Trim());
                            }
                            if (data[i].StartsWith("Xms-GW-Addr:"))
                            {
                                int len = data[i].Length - 12;
                                xmsGateway.Add(data[i].Substring(12, len).Trim());

                            }
                            if (data[i].StartsWith("Thin-Provisioning-Savings:"))
                            {
                                int len = data[i].Length - 26;
                                xmsThinProvision.Add(data[i].Substring(26, len).Trim());

                            }
                            if (data[i].StartsWith("Num-of-Clusters:"))
                            {
                                int len = data[i].Length - 16;
                                xmsNumOfCluster.Add(data[i].Substring(16, len).Trim());
                            }
                            if (data[i].StartsWith("Num-of-Vols:"))
                            {
                                int len = data[i].Length - 12;
                                xmsnumOfVols.Add(data[i].Substring(12, len).Trim());

                            }
                            if (data[i].StartsWith("Num-of-IGs:"))
                            {
                                int len = data[i].Length - 11;
                                xmsNumOfIG.Add(data[i].Substring(11, len).Trim());
                            }
                            if (data[i].StartsWith("NTP-Servers:"))
                            {
                                int len = data[i].Length - 12;
                                if (len > 5)
                                {
                                    xmsNTP.Add(data[i].Substring(12, len).Trim());
                                }
                                else
                                {
                                    xmsNTP.Add("N/A");
                                }

                            }
                            if (data[i].StartsWith("Free-Logical-Space (%):"))
                            {
                                int len = data[i].Length - 23;
                                xmsLogicalfreepercent.Add(data[i].Substring(23, len).Trim());

                            }
                            if (data[i].StartsWith("Free-UD-SSD-Space(%):"))
                            {
                                int len = data[i].Length - 21;
                                xmsPhyFreepercent.Add(data[i].Substring(21, len).Trim());

                            }
                            if (data[i].StartsWith("Free-UD-SSD-Space:"))
                            {
                                int len = data[i].Length - 18;
                                xmsPhyFree.Add(data[i].Substring(18, len).Trim());

                            }
                            if (data[i].StartsWith("Free-Logical-Space:"))
                            {
                                int len = data[i].Length - 19;
                                xmsLogicalFree.Add(data[i].Substring(19, len).Trim());

                            }
                            if (data[i].StartsWith("Dedup-Ratio:"))
                            {
                                int len = data[i].Length - 12;
                                xmsDedup.Add(data[i].Substring(12, len).Trim());

                            }
                            if (data[i].StartsWith("Cluster-Time:"))
                            {
                                int len = data[i].Length - 13;
                                xmsclusterTime.Add(data[i].Substring(13, len).Trim());

                            }
                            if (data[i].StartsWith("Data-Reduction-Ratio:"))
                            {
                                int len = data[i].Length - 21;
                                xmsDatareduction.Add(data[i].Substring(21, len).Trim());

                            }
                            if (data[i].StartsWith("Overall_Efficiency_Ratio:"))
                            {
                                int len = data[i].Length - 25;
                                xmsOverallEff.Add(data[i].Substring(25, len).Trim());

                            }

                            if (data[i].StartsWith("Compression Factor:"))
                            {
                                int len = data[i].Length - 19;
                                xmsCompression.Add(data[i].Substring(19, len).Trim());

                            }
                        }

                        //Get total number of rows
                        int temp = filled;

                        if (bw.CancellationPending == true)
                        {
                            args.Cancel = true;
                            MessageBox.Show("Project creation cancelled..", "Cancelled!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                            // abort work, if it's cancelled
                        }
                        // Fill Empty Fillers

                        while (temp != 0)
                        {
                            /* Sometimes the rows not always contain same data.
                             * the logs miss out few things , now we do not want <UNSET> data in our tables do we ?
                             * So find those missing pieces and drag them out */
                            if (!(xmsPlatform.Count == filled))
                            {
                                xmsPlatform.Add("N/A");

                            }
                            if (!(xmsThinProvision.Count == filled))
                            {
                                xmsThinProvision.Add("N/A");
                            }
                            if (!(xmsSubnet.Count == filled))
                            {
                                xmsSubnet.Add("N/A");
                            }
                            if (!(xmsnumOfVols.Count == filled))
                            {
                                xmsnumOfVols.Add("N/A");
                            }
                            if (!(xmsCompression.Count == filled))
                            {
                                xmsCompression.Add("N/A");
                            }
                            if (!(xmsOverallEff.Count == filled))
                            {
                                xmsOverallEff.Add("N/A");
                            }
                            if (!(xmsDatareduction.Count == filled))
                            {
                                xmsDatareduction.Add("N/A");
                            }
                            if (!(xmsDedup.Count == filled))
                            {
                                xmsDedup.Add("N/A");
                            }
                            if (!(xmsLogicalFree.Count == filled))
                            {
                                xmsLogicalFree.Add("N/A");
                            }
                            if (!(xmsLogicalfreepercent.Count == filled))
                            {
                                xmsLogicalfreepercent.Add("N/A");
                            }
                            if (!(xmsPhyFree.Count == filled))
                            {
                                xmsPhyFree.Add("N/A");
                            }
                            if (!(xmsPhyFreepercent.Count == filled))
                            {
                                xmsPhyFreepercent.Add("N/A");
                            }
                            if (!(xmsOS.Count == filled))
                            {
                                xmsOS.Add("N/A");
                            }
                            if (!(xmsGateway.Count == filled))
                            {
                                xmsGateway.Add("N/A");
                            }
                            if (!(xmsclusterTime.Count == filled))
                            {
                                xmsclusterTime.Add("N/A");
                            }
                            if (!(xmsclusterTime.Count == filled))
                            {
                                xmsclusterTime.Add("N/A");
                            }
                            if (!(xmsNTP.Count == filled))
                            {
                                xmsNTP.Add("N/A");
                            }
                            if (!(xmsNumOfIG.Count == filled))
                            {
                                xmsNumOfIG.Add("N/A");
                            }
                            if (!(xmsNumOfCluster.Count == filled))
                            {
                                xmsNumOfCluster.Add("N/A");
                            }
                            temp--;
                         
                        }
                    }

                    // Insert into XMSInfo Table
                    if (bw.CancellationPending == true)
                    {
                        args.Cancel = true;
                        return; 
                        // abort work, if it's cancelled
                    }
                    for (int i = 0; i < xmsName.Count; i++)
                    {
                        SQLiteCommand insertSQL = new SQLiteCommand(@"insert into xmsinfo (xmsname, 
                    xmsip,xmssubnet,xmsos,xmsplatform,xmsgateway,dedupratio,clustertime
                    ,compressionfactor,freelogicalspace,freephyspace,freelogicalpercent,freephypercent,ntpservers,numofig,numofcluster
                     ,numofvol,
                    overallefficiency,thinprovsaving) values 
                    (@xmsname,@xmsip,@xmssubnet,@xmsos,@xmsplatform,@xmsgateway,@dedupratio,@clustertime
                    ,@compressionfactor,@freelogicalspace,@freephyspace,@freelogicalpercent,
                    @freephypercent,@ntpservers,@numofig,@numofcluster,@numofvol,
                    @overallefficiency,@thinprovsaving)", m_dbConnection);
                        try
                        {
                        insertSQL.Parameters.AddWithValue("xmsname", xmsName[i]);
                        insertSQL.Parameters.AddWithValue("xmsip", xmsIP[i]);
                        insertSQL.Parameters.AddWithValue("xmssubnet", xmsSubnet[i]);
                        insertSQL.Parameters.AddWithValue("xmsos", xmsOS[i]);
                        insertSQL.Parameters.AddWithValue("xmsplatform", xmsPlatform[i]);
                        insertSQL.Parameters.AddWithValue("xmsgateway", xmsGateway[i]);
                        insertSQL.Parameters.AddWithValue("dedupratio", xmsDedup[i]);
                        insertSQL.Parameters.AddWithValue("clustertime", xmsclusterTime[i]);
                        insertSQL.Parameters.AddWithValue("compressionfactor", xmsCompression[i]);
                        insertSQL.Parameters.AddWithValue("freelogicalspace", xmsLogicalFree[i]);
                        insertSQL.Parameters.AddWithValue("freephyspace", xmsPhyFree[i]);
                        insertSQL.Parameters.AddWithValue("freelogicalpercent", xmsLogicalfreepercent[i]);
                        insertSQL.Parameters.AddWithValue("freephypercent", xmsPhyFreepercent[i]);
                        insertSQL.Parameters.AddWithValue("ntpservers", xmsNTP[i]);
                        insertSQL.Parameters.AddWithValue("numofig", xmsNumOfIG[i]);
                        insertSQL.Parameters.AddWithValue("numofcluster", xmsNumOfCluster[i]);
                        insertSQL.Parameters.AddWithValue("numofvol", xmsnumOfVols[i]);
                        insertSQL.Parameters.AddWithValue("overallefficiency", xmsOverallEff[i]);
                        insertSQL.Parameters.AddWithValue("thinprovsaving", xmsThinProvision[i]);
                      
                            insertSQL.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                            
                        }

                    }
                #endregion XMS Info Database
                    if (bw.CancellationPending == true)
                    {
                        args.Cancel = true;
                        MessageBox.Show("Project creation cancelled..", "Cancelled!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return; // abort work, if it's cancelled

                    }
                #region Initiator Connectivity Details

                    //Same story different region !!!!
                    //target connectivity details , Get connectivity data. Small table :)

                    sql = @"create table initiatorconnectivity (initiatorname varchar(100) , targetlist varchar(200))";
                    command = new SQLiteCommand(sql, m_dbConnection);
                    command.ExecuteNonQuery();

                    path = folderBrowserDialog1.SelectedPath + "/xms/xmcli/ShowInitiatorsConnectivity.out";
                    if (File.Exists(path))
                    {
                        string[] data = System.IO.File.ReadAllLines(path);
                        for (int i = 0; i < data.Length; i++)
                        {
                            string[] arr = data[i].Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                            if (arr.Length > 8)
                            {
                                connectedIni.Add(arr[2].ToString());
                                int x = 7; string temp = "";
                                while (x != arr.Length)
                                {
                                    temp += arr[x].ToString();
                                    x++;
                                }
                                connectedTarget.Add(temp);
                            }
                        }

                    }
                    //Not checked for filled ????? , TO DO !!

                    // Insert into database

                    for (int i = 0; i < connectedIni.Count; i++)
                    {
                        SQLiteCommand insertSQL = new SQLiteCommand(@"insert into initiatorconnectivity (initiatorname, 
                    targetlist)values(@initname,@targetlist)", m_dbConnection);

                        insertSQL.Parameters.AddWithValue("initname", connectedIni[i]);
                        insertSQL.Parameters.AddWithValue("targetlist", connectedTarget[i]);

                        try
                        {
                            insertSQL.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }

                    }
                    if (bw.CancellationPending == true)
                    {
                        args.Cancel = true;
                        return; // abort work, if it's cancelled
                    }
                    #endregion
                    if (bw.CancellationPending == true)
                    {
                        args.Cancel = true;
                        MessageBox.Show("Project creation cancelled..", "Cancelled!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return; // abort work, if it's cancelled

                    }
                #region Cluster Details
                    //cluster details , Create clusterdetails table
                   
                    sql = @"create table clusterdetails (clustername varchar(50), clusterindex varchar(10),psnt varchar(20),num_of_vol varchar(20),
                num_of_bricks varchar(20),size_capacity varchar(20),dedup_ratio varchar(10),free_phy_space varchar(20),free_logical_space varchar(20),
                sw_version varchar(10),thin_provisioning_savings varchar(20),totalreads double , totalwrites double , freelogicalspace_gb double ,freephyspace_gb double)";

                    command = new SQLiteCommand(sql, m_dbConnection);
                    command.ExecuteNonQuery().ToString();

                    filled = 0;


                    b.ReportProgress(20);
                    Thread.Sleep(100);
                    path = folderBrowserDialog1.SelectedPath + "/xms/xmcli/ShowAllClusters.out";
                    
                    // Read Data from show all clusters

                    if (File.Exists(path))
                    {
                        string[] data = System.IO.File.ReadAllLines(path);
                        for (int i = 0; i < data.Length; i++)
                        {
                            if ((data[i].StartsWith("Cluster-Name:") && data[i + 1].StartsWith("Index:"))|| (data[i].StartsWith("Name:") && data[i+1].StartsWith("Index:") && (!data[i+2].StartsWith("BW(MB/s):"))))
                            {
                 
                                filled++;
                                if (data[i].StartsWith("Cluster-Name"))
                                {
                                    int len = data[i].Length - 13;
                                    clusterName.Add(data[i].Substring(13, len).Trim());
                                    len = data[i + 1].Length - 6;
                                    clusterIndex.Add(data[i + 1].Substring(6, len).Trim());
                                }
                                else
                                {
                                    int len = data[i].Length - 5;
                                    clusterName.Add(data[i].Substring(5, len).Trim());
                                    len = data[i + 1].Length - 6;
                                    clusterIndex.Add(data[i + 1].Substring(6, len).Trim());
                                }

                            }
                            if (data[i].StartsWith("Dedup-Ratio:") && !data[i + 1].StartsWith("Dedup-Space-In-Use:"))
                            {
                                //next line of dedup ratio
                                int len = data[i + 1].Length - 12;
                                dedupRatio.Add(data[i + 1].Substring(12, len).Trim());
                            }
                            // Should convert from LB to GB
                            if (data[i].StartsWith("Total-Reads:"))
                            {
                                int len = data[i].Length - 12;
                                string s = data[i].Substring(12, len).Trim();
                                if (s[s.Length - 1] == 'T')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    clustertotalreads.Add(num * 1024);

                                }
                                if (s[s.Length - 1] == 'K')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    clustertotalreads.Add(num / (1024 * 1024));


                                }
                                if (s[s.Length - 1] == 'P')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    clustertotalreads.Add(num * 1024 * 1024);

                                }
                                if (s[s.Length - 1] == 'M')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    clustertotalreads.Add(num / 1024);

                                }
                                if (s[s.Length - 1] == 'G')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    clustertotalreads.Add(num);

                                }
                                if (s.Length == 1)
                                {
                                    s = s.Trim();

                                    clustertotalreads.Add(0);

                                }

                            }
                            // Should convert from LB to GB
                            if (data[i].StartsWith("Total-Writes:"))
                            {
                                int len = data[i].Length - 13;
                                string s = data[i].Substring(13, len).Trim();
                                if (s[s.Length - 1] == 'T')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    clustertotalwrites.Add(num * 1024);

                                }
                                if (s[s.Length - 1] == 'K')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    clustertotalwrites.Add(num / (1024 * 1024));


                                }
                                if (s[s.Length - 1] == 'P')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    clustertotalwrites.Add(num * 1024 * 1024);

                                }
                                if (s[s.Length - 1] == 'M')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    clustertotalwrites.Add(num / 1024);

                                }
                                if (s[s.Length - 1] == 'G')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    clustertotalwrites.Add(num);

                                }
                                if (s.Length == 1)
                                {
                                    s = s.Trim();

                                    clustertotalwrites.Add(0);

                                }

                            }
                            // Should convert from LB to GB
                            if (data[i].StartsWith("Free-UD-SSD-Space(%):"))
                            {
                                int len = data[i - 1].Length - 18;
                                string s = data[i - 1].Substring(18, len).Trim();

                                if (s[s.Length - 1] == 'T')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    freeclusterphyspaceGB.Add(num * 1024);

                                }
                                if (s[s.Length - 1] == 'K')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    freeclusterphyspaceGB.Add(num / (1024 * 1024));


                                }
                                if (s[s.Length - 1] == 'P')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    freeclusterphyspaceGB.Add(num * 1024 * 1024);

                                }
                                if (s[s.Length - 1] == 'M')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    freeclusterphyspaceGB.Add(num / 1024);

                                }
                                if (s[s.Length - 1] == 'G')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    freeclusterphyspaceGB.Add(num);

                                }
                                if (s.Length == 1)
                                {
                                    s = s.Trim();

                                    freeclusterphyspaceGB.Add(0);

                                }

                            }
                            // Should convert from LB to GB
                            if (data[i].StartsWith("Free-Logical-Space (%):"))
                            {
                                int len = data[i - 1].Length - 19;
                                string s = data[i - 1].Substring(19, len).Trim();

                                if (s[s.Length - 1] == 'T')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    freeclusterlogicaspaceGB.Add(num * 1024);

                                }
                                if (s[s.Length - 1] == 'K')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    freeclusterlogicaspaceGB.Add(num / (1024 * 1024));


                                }
                                if (s[s.Length - 1] == 'P')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    freeclusterlogicaspaceGB.Add(num * 1024 * 1024);

                                }
                                if (s[s.Length - 1] == 'M')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    freeclusterlogicaspaceGB.Add(num / 1024);

                                }
                                if (s[s.Length - 1] == 'G')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    freeclusterlogicaspaceGB.Add(num);

                                }
                                if (s.Length == 1)
                                {
                                    s = s.Trim();

                                    freeclusterlogicaspaceGB.Add(0);

                                }

                            }


                            if (data[i].StartsWith("Free-Logical-Space (%):"))
                            {
                                int len = data[i].Length - 23;
                                freelogSpace.Add(data[i].Substring(23, len).Trim());

                            }
                            if (data[i].StartsWith("Free-UD-SSD-Space(%):"))
                            {
                                int len = data[i].Length - 21;
                                freephySpace.Add(data[i].Substring(21, len).Trim());
                            }
                            if (data[i].StartsWith("Num-of-Bricks:"))
                            {
                                int len = data[i].Length - 14;
                                numOfBricks.Add(data[i].Substring(14, len).Trim());

                            }
                            if (data[i].StartsWith("Num-of-Vols:"))
                            {
                                int len = data[i].Length - 12;
                                numOfVol.Add(data[i].Substring(12, len).Trim());

                            }
                            if (data[i].StartsWith("PSNT-Serial-Number:"))
                            {
                                int len = data[i].Length - 19;
                                psntNumber.Add(data[i].Substring(19, len).Trim());
                            }
                            if (data[i].StartsWith("Size-and-Capacity:"))
                            {
                                int len = data[i].Length - 18;
                                sizeCap.Add(data[i].Substring(18, len).Trim());

                            }
                            if (data[i].StartsWith("SW-Version:"))
                            {
                                int len = data[i].Length - 11;
                                swVersion.Add(data[i].Substring(11, len).Trim());
                            }
                            if (data[i].StartsWith("Thin-Provisioning-Savings:"))
                            {
                                int len = data[i].Length - 26;
                                thinProvision.Add(data[i].Substring(26, len).Trim());

                            }
                        }

                        int temp = filled;
                        while (temp != 0)
                        {
                            if (!(freelogSpace.Count == filled))
                            {
                                freelogSpace.Add("N/A");

                            }
                            if (!(freephySpace.Count == filled))
                            {
                                freephySpace.Add("N/A");
                            }
                            if (!(clusterIndex.Count == filled))
                            {
                                clusterIndex.Add("N/A");
                            }
                            if (!(dedupRatio.Count == filled))
                            {
                                dedupRatio.Add("N/A");
                            }
                            if (!(numOfBricks.Count == filled))
                            {
                                numOfBricks.Add("N/A");
                            }
                            if (!(numOfVol.Count == filled))
                            {
                                numOfVol.Add("N/A");
                            }
                            if (!(psntNumber.Count == filled))
                            {
                                psntNumber.Add("N/A");
                            }
                            if (!(sizeCap.Count == filled))
                            {
                                sizeCap.Add("N/A");
                            }
                            if (!(xmsDedup.Count == filled))
                            {
                                xmsDedup.Add("N/A");
                            }
                            if (!(swVersion.Count == filled))
                            {
                                swVersion.Add("N/A");
                            }
                            if (!(thinProvision.Count == filled))
                            {
                                thinProvision.Add("N/A");
                            }

                            if (!(freeclusterlogicaspaceGB.Count == filled))
                            {
                                freeclusterlogicaspaceGB.Add(0);
                            }
                            if (!(freeclusterphyspaceGB.Count == filled))
                            {
                                freeclusterphyspaceGB.Add(0);
                            }
                            if (!(clustertotalwrites.Count == filled))
                            {
                                clustertotalwrites.Add(0);
                            }
                            if (!(clustertotalreads.Count == filled))
                            {
                                clustertotalreads.Add(0);
                            }

                            temp--;
                        }

                    }
                    b.ReportProgress(30);
                    Thread.Sleep(100);

                    for (int i = 0; i < clusterIndex.Count; i++)
                    {
                        SQLiteCommand insertSQL = new SQLiteCommand(@"insert into clusterdetails (clustername, 
                    clusterindex,psnt,num_of_vol,num_of_bricks
                    ,size_capacity,dedup_ratio,free_phy_space,free_logical_space,sw_version,thin_provisioning_savings,totalreads,
                    totalwrites,freelogicalspace_gb,freephyspace_gb) values 
                    (@clustername,@clusterindex,@psnt,@numofvol,@numofbricks,@sizecap,@dedupratio,@freephy,@freelog,@swversion
                     ,@thin,@totalreads,@totalwrites,@freelogGB,@freephyGB)", m_dbConnection);

                        insertSQL.Parameters.AddWithValue("clustername", clusterName[i]);
                        insertSQL.Parameters.AddWithValue("clusterindex", clusterIndex[i]);
                        insertSQL.Parameters.AddWithValue("psnt", psntNumber[i]);
                        insertSQL.Parameters.AddWithValue("numofvol", numOfVol[i]);
                        insertSQL.Parameters.AddWithValue("numofbricks", numOfBricks[i]);
                        insertSQL.Parameters.AddWithValue("sizecap", sizeCap[i]);
                        insertSQL.Parameters.AddWithValue("dedupratio", dedupRatio[i]);
                        insertSQL.Parameters.AddWithValue("freephy", freephySpace[i]);
                        insertSQL.Parameters.AddWithValue("freelog", freelogSpace[i]);
                        insertSQL.Parameters.AddWithValue("swversion", swVersion[i]);
                        insertSQL.Parameters.AddWithValue("thin", thinProvision[i]);
                        insertSQL.Parameters.AddWithValue("totalreads", clustertotalreads[i]);
                        insertSQL.Parameters.AddWithValue("totalwrites", clustertotalwrites[i]);
                        insertSQL.Parameters.AddWithValue("freelogGB", freeclusterlogicaspaceGB[i]);
                        insertSQL.Parameters.AddWithValue("freephyGB", freeclusterphyspaceGB[i]);
                        try
                        {
                            insertSQL.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }

                    }
                    Thread.Sleep(100);
                    #endregion
                    if (bw.CancellationPending == true)
                    {
                        args.Cancel = true;
                        MessageBox.Show("Project creation cancelled..", "Cancelled!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return; // abort work, if it's cancelled

                    }
                #region Volume Details
                    sql = @"CREATE TABLE volumedetails (volumename varchar(100), volumeindex varchar(10),clustername varchar(50),
                volsize varchar(20),lbsize varchar(20),physpaceused double ,lbnotation varchar(15),realvolsize double,
                createdfrom varchar(100),smallIO varchar(20),unalignedIO varchar(20),
                VAAITP varchar(20),naaID varchar(20),creationTime varchar(100));";

                    command = new SQLiteCommand(sql, m_dbConnection);
                    command.ExecuteNonQuery();

                    //LUN MAPPING

                    sql = @"CREATE TABLE lunMapping (volumename varchar(100),igname varchar(100));";

                    command = new SQLiteCommand(sql, m_dbConnection);
                    command.ExecuteNonQuery().ToString();

                    filled = 0;
                    path = folderBrowserDialog1.SelectedPath + "/xms/xmcli/ShowAllVolumes.out";
                    if (File.Exists(path))
                    {
                       
                        string[] data = System.IO.File.ReadAllLines(path);
                        for (int i = 0; i < data.Length; i++)
                        {
                           
                            if ((data[i].StartsWith("Volume-Name:") || data[i].StartsWith("Name:")) && data[i + 1].StartsWith("Index:"))
                            {
                                filled++;
                                if (data[i].StartsWith("Volume-Name:"))
                                {
                                    int len = data[i].Length - 13;
                                    volumename.Add(data[i].Substring(13, len).Trim());
                                    len = data[i + 1].Length - 6;
                                    volIndex.Add(data[i + 1].Substring(6, len).Trim());
                                }
                                else
                                {
                                    int len = data[i].Length - 5;
                                    volumename.Add(data[i].Substring(5, len).Trim());
                                    len = data[i + 1].Length - 6;
                                    volIndex.Add(data[i + 1].Substring(6, len).Trim());
                                }
                            }
                            if (data[i].StartsWith("Vol-Size:"))
                            {

                                int len = data[i].Length - 9;
                                volsize.Add(data[i].Substring(9, len).Trim());
                            }
                            if (data[i].StartsWith("LB-Size:"))
                            {
                                int len = data[i].Length - 8;
                                lbSize.Add(data[i].Substring(8, len).Trim());

                            }
                            if (data[i].StartsWith("VSG-Space-In-Use:"))
                            {
                                int len = data[i].Length - 17;
                                string s = data[i].Substring(17, len).Trim();
                                if (s[s.Length - 1] == 'T')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));
                                    LBNotation.Add("TeraBytes");
                                    volsizeinGB.Add(num * 1024);
                                    volSpaceused.Add(num);
                                }
                                if (s[s.Length - 1] == 'K')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));
                                    LBNotation.Add("KiloBytes");
                                    volsizeinGB.Add(num / (1024 * 1024));
                                    volSpaceused.Add(num);

                                }
                                if (s[s.Length - 1] == 'P')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));
                                    LBNotation.Add("PetaBytes");
                                    volsizeinGB.Add(num * 1024 * 1024);
                                    volSpaceused.Add(num);
                                }
                                if (s[s.Length - 1] == 'M')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));
                                    LBNotation.Add("MegaBytes");
                                    volsizeinGB.Add(num / 1024);
                                    volSpaceused.Add(num);
                                }
                                if (s[s.Length - 1] == 'G')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));
                                    LBNotation.Add("GigaBytes");
                                    volsizeinGB.Add(num);
                                    volSpaceused.Add(num);
                                }
                                if (s.Length == 1)
                                {
                                    s = s.Trim();
                                    LBNotation.Add("KiloBytes");
                                    volsizeinGB.Add(0);
                                    volSpaceused.Add(0);
                                }

                            }
                            if (data[i].StartsWith("Created-From-Volume:"))
                            {
                                int len = data[i].Length - 20;
                                if (len > 2)
                                {
                                    createdFrom.Add(data[i].Substring(20, len).Trim());
                                }
                                else
                                {
                                    createdFrom.Add("N/A");
                                }
                            }
                            if (data[i].StartsWith("Ancestor-Vol-Id:"))
                            {
                                int len = data[i].Length - 16;
                                string s = data[i].Substring(16, len).Trim();
                                if (s.Length > 5)
                                {
                                    string[] arr = s.Split('\'');
                                    createdFrom.Add(arr[3].Trim());
                                }
                                else
                                {
                                    createdFrom.Add("N/A");
                                }


                            }
                            if (data[i].StartsWith("Small-IO-Alerts:"))
                            {
                                int len = data[i].Length - 16;
                                smallIO.Add(data[i].Substring(16, len).Trim());

                            }
                            if (data[i].StartsWith("Unaligned-IO-Alerts:"))
                            {
                                int len = data[i].Length - 20;
                                unalignedIO.Add(data[i].Substring(20, len).Trim());
                            }
                            if (data[i].StartsWith("VAAI-TP-Alerts:"))
                            {
                                int len = data[i].Length - 15;
                                vaaiTP.Add(data[i].Substring(15, len).Trim());

                            }
                            if (data[i].StartsWith("NAA-Identifier:"))
                            {
                                int len = data[i].Length - 15;
                                if (len > 4)
                                {
                                    naaID.Add(data[i].Substring(15, len).Trim());
                                }
                                else
                                {
                                    naaID.Add("N/A");
                                }
                            }
                            if (data[i].StartsWith("LUN-Mapping-List:"))
                            {
                                int j = i, flag = 0;
                                while (!data[j].StartsWith("NAA-Identifier:"))
                                {
                                    //populate
                                    if (data[j + 1].Contains("IG Name:"))
                                    {

                                        int len = data[j + 1].Trim().Length - 8;
                                        string s = data[j + 1].Trim();
                                        lunMappingData.Add(s.Substring(8, len).Trim());
                                        lunMappingVolmue.Add(volumename[volumename.Count - 1]);
                                        flag = 1;
                                    }

                                    j++;
                                }
                                if (flag == 0)
                                {
                                    lunMappingData.Add("Not Mapped");
                                    lunMappingVolmue.Add(volumename[volumename.Count - 1]);
                                }

                            }
                            if (data[i].StartsWith("Cluster-Id:"))
                            {
                                int len = data[i].Length - 11;
                                string s = data[i].Substring(11, len).Trim();
                                string[] arr = s.Split('\'');
                                foreach (string s1 in clusterName)
                                {
                                    if (arr.Contains(s1))
                                    {
                                        volClusterName.Add(s1);
                                        break;
                                    }
                                }
                            }
                            if (data[i].StartsWith("Creation-Time:") || data[i].StartsWith("Created:"))
                            {
                                if (data[i].StartsWith("Created:"))
                                {
                                    int len = data[i].Length - 8;
                                    creationTime.Add(data[i].Substring(8, len).Trim());
                                }
                                else
                                {
                                    int len = data[i].Length - 14;
                                    creationTime.Add(data[i].Substring(14, len).Trim());
                                }

                            }
                        }
                        b.ReportProgress(40);
                    }

                    int temp1 = filled;
                    while (temp1 != 0)
                    {
                        if (!(creationTime.Count == filled))
                        {
                            creationTime.Add("N/A");

                        }
                        if (!(volClusterName.Count == filled))
                        {
                            volClusterName.Add("N/A");
                        }
                        if (!(lunMappingData.Count == filled))
                        {
                            lunMappingData.Add("N/A");
                        }
                        if (!(lunMappingVolmue.Count == filled))
                        {
                            lunMappingVolmue.Add("N/A");
                        }
                        if (!(naaID.Count == filled))
                        {
                            naaID.Add("N/A");
                        }
                        if (!(vaaiTP.Count == filled))
                        {
                            vaaiTP.Add("N/A");
                        }
                        if (!(unalignedIO.Count == filled))
                        {
                            unalignedIO.Add("N/A");
                        }
                        if (!(smallIO.Count == filled))
                        {
                            smallIO.Add("N/A");
                        }
                        if (!(createdFrom.Count == filled))
                        {
                            createdFrom.Add("N/A");
                        }
                        if (!(volSpaceused.Count == filled))
                        {
                            volSpaceused.Add(0);
                        }
                        if (!(volsizeinGB.Count == filled))
                        {
                            volsizeinGB.Add(0);
                        }

                        if (!(lbSize.Count == filled))
                        {
                            lbSize.Add("N/A");
                        }
                        if (!(volsize.Count == filled))
                        {
                            volsize.Add("N/A");
                        }
                        if (!(LBNotation.Count == filled))
                        {
                            LBNotation.Add("N/A");
                        }
                        
                        temp1--;
                    }

                    //generating for volume 
                    for (int i = 0; i < volIndex.Count; i++)
                    {
                        SQLiteCommand insertSQL = new SQLiteCommand(@"insert into volumedetails (volumename, 
                    volumeindex,clustername,volsize
                    ,lbsize,physpaceused,lbnotation,realvolsize,createdfrom,smallIO,unalignedIO,VAAITP,naaID,creationTime) values 
                    (@volumename,@volumeindex,@clustername,@volsize,@lbsize,@physpaceused,@lbnotation,@realvolsize,@createdfrom,@smallIO,@unalignedIO
                     ,@VAAITP,@naaID,@creationTime)", m_dbConnection);

                        insertSQL.Parameters.AddWithValue("volumename", volumename[i]);
                        insertSQL.Parameters.AddWithValue("volumeindex", volIndex[i]);
                        insertSQL.Parameters.AddWithValue("clustername", volClusterName[i]);
                        insertSQL.Parameters.AddWithValue("volsize", volsize[i]);
                        insertSQL.Parameters.AddWithValue("lbsize", lbSize[i]);
                        insertSQL.Parameters.AddWithValue("physpaceused", volSpaceused[i]);
                        insertSQL.Parameters.AddWithValue("lbnotation", LBNotation[i]);
                        insertSQL.Parameters.AddWithValue("realvolsize", volsizeinGB[i]);
                        try
                        {
                            insertSQL.Parameters.AddWithValue("createdfrom", createdFrom[i]);
                        }
                        catch
                        {
                            insertSQL.Parameters.AddWithValue("createdfrom", "null");
                        }
                        insertSQL.Parameters.AddWithValue("smallIO", smallIO[i]);
                        insertSQL.Parameters.AddWithValue("unalignedIO", unalignedIO[i]);
                        insertSQL.Parameters.AddWithValue("VAAITP", vaaiTP[i]);
                        insertSQL.Parameters.AddWithValue("naaID", naaID[i]);
                        insertSQL.Parameters.AddWithValue("creationTime", creationTime[i]);
                        try
                        {
                            insertSQL.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }

                    }

                    for (int i = 0; i < lunMappingData.Count; i++)
                    {
                        SQLiteCommand insertSQL = new SQLiteCommand(@"insert into lunMapping(volumename,igname) values 
                    (@volumename,@igname)", m_dbConnection);

                        insertSQL.Parameters.AddWithValue("volumename", lunMappingVolmue[i]);
                        insertSQL.Parameters.AddWithValue("igname", lunMappingData[i]);

                        try
                        {
                            insertSQL.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }

                    }

                    b.ReportProgress(50);
                    Thread.Sleep(100);
                    #endregion
                    if (bw.CancellationPending == true)
                    {
                        args.Cancel = true;
                        MessageBox.Show("Project creation cancelled..", "Cancelled!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return; // abort work, if it's cancelled

                    }
                #region Initiator Group Details
                    getAllInitiatorGroup();
                    // Do for initiator details
                    filled = 0;
                    sql = @"CREATE TABLE initiatordetails (initiatorname varchar(100), initiatorindex varchar(10),clustername varchar(50),
                porttype varchar(10),portaddr varchar(100),igname varchar(100),
                initiatorOS varchar(100));";

                    command = new SQLiteCommand(sql, m_dbConnection);
                    command.ExecuteNonQuery().ToString();

                    path = folderBrowserDialog1.SelectedPath + "/xms/xmcli/ShowAllInitiators.out";
                    if (File.Exists(path))
                    {
                        string[] data = System.IO.File.ReadAllLines(path);
                        for (int i = 0; i < data.Length; i++)
                        {
                            if ((data[i].StartsWith("Initiator-Name:") || data[i].StartsWith("Name:")) && data[i + 1].StartsWith("Index:"))
                            {
                                filled++;
                                if (data[i].StartsWith("Initiator-Name:"))
                                {
                                    
                                    int len = data[i].Length - 15;
                                    initiatorName.Add(data[i].Substring(15, len).Trim());
                                    len = data[i + 1].Length - 6;
                                    initiatorIndex.Add(data[i + 1].Substring(6, len).Trim());
                                }
                                else
                                {
                                    int len = data[i].Length - 5;
                                    initiatorName.Add(data[i].Substring(5, len).Trim());
                                    len = data[i + 1].Length - 6;
                                    initiatorIndex.Add(data[i + 1].Substring(6, len).Trim());
                                }

                            }
                            if (data[i].StartsWith("Cluster-Id:"))
                            {
                                int len = data[i].Length - 11;
                                string s = data[i].Substring(11, len).Trim();
                                string[] arr = s.Split('\'');
                                foreach (string s1 in clusterName)
                                {
                                    if (arr.Contains(s1))
                                    {
                                        initiatorClusterName.Add(s1);
                                        break;
                                    }
                                }
                            }
                            if (data[i].StartsWith("Port-Type:"))
                            {
                                int len = data[i].Length - 10;
                                portType.Add(data[i].Substring(10, len).Trim());
                            }
                            if (data[i].StartsWith("Port-Address:"))
                            {
                                int len = data[i].Length - 13;
                                portAddr.Add(data[i].Substring(13, len).Trim());
                            }
                            if (data[i].StartsWith("IG-Id:"))
                            {
                                int len = data[i].Length - 6;
                                string s = data[i].Substring(6, len).Trim();
                                string[] arr = s.Split('\'');
                                foreach (string s1 in initiatorGroupData)
                                {
                                    if (arr.Contains(s1))
                                    {
                                        initiatorGroupName.Add(s1);
                                        break;
                                    }
                                }
                            }
                            if (data[i].StartsWith("Initiator-OS:"))
                            {
                                int len = data[i].Length - 13;
                                initiatorOS.Add(data[i].Substring(13, len).Trim());
                            }

                        }
                        int temp = filled;
                        while (temp != 0)
                        {
                            if (!(initiatorIndex.Count == filled))
                            {
                                initiatorIndex.Add("N/A");
                            }
                            if (!(initiatorClusterName.Count == filled))
                            {
                                initiatorClusterName.Add("N/A");
                            }
                            if (!(portType.Count == filled))
                            {
                                portType.Add("N/A");
                            }
                            if (!(portAddr.Count == filled))
                            {
                                portAddr.Add("N/A");
                            }
                            if (!(initiatorGroupName.Count == filled))
                            {
                                initiatorGroupName.Add("N/A");
                            }
                            if (!(initiatorOS.Count == filled))
                            {
                                initiatorOS.Add("N/A");
                            }
                            temp--;
                        }
                    }
                    for (int i = 0; i < initiatorIndex.Count; i++)
                    {
                        SQLiteCommand insertSQL = new SQLiteCommand(@"insert into initiatordetails (initiatorname, 
                    initiatorindex,clustername,porttype
                    ,portaddr,igname,initiatorOS) values 
                    (@initiatorname,@initiatorindex,@clustername,@porttype,@portaddr,@igname,@initiatorOS)", m_dbConnection);

                        insertSQL.Parameters.AddWithValue("initiatorname", initiatorName[i]);
                        insertSQL.Parameters.AddWithValue("initiatorindex", initiatorIndex[i]);
                        insertSQL.Parameters.AddWithValue("clustername", initiatorClusterName[i]);
                        insertSQL.Parameters.AddWithValue("porttype", portType[i]);
                        insertSQL.Parameters.AddWithValue("portaddr", portAddr[i]);
                        insertSQL.Parameters.AddWithValue("igname", initiatorGroupName[i]);
                        insertSQL.Parameters.AddWithValue("initiatorOS", initiatorOS[i]);

                        try
                        {
                            insertSQL.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }

                    }

                    #endregion
                    if (bw.CancellationPending == true)
                    {
                        args.Cancel = true;
                        MessageBox.Show("Project creation cancelled..", "Cancelled!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return; // abort work, if it's cancelled

                    }
                #region CG Details database
                    b.ReportProgress(60);
                    Thread.Sleep(100);
                    // TO DO Consistency Group Details
                    //Normalized data - Key CG and Volname
                    sql = @"CREATE TABLE cgdetails (cgname varchar(100), cgindex varchar(10),clustername varchar(100),numofvols varchar(10))";
                    command = new SQLiteCommand(sql, m_dbConnection);
                    command.ExecuteNonQuery();

                    string cgInfo = folderBrowserDialog1.SelectedPath + "/xms/xmcli/ShowAllConsistencyGroups.out";
                    if (File.Exists(cgInfo))
                    {
                        string[] cgData = System.IO.File.ReadAllLines(cgInfo);
                        for (int i = 0; i < cgData.Length; i++)
                        {
                            if (cgData[i].StartsWith("Name:") && cgData[i + 1].StartsWith("Index:"))
                            {
                                int length = cgData[i].Length - 5;
                                cgName.Add(cgData[i].Substring(5, length).Trim());
                                length = cgData[i + 1].Length - 6;
                                cgIndex.Add(cgData[i + 1].Substring(6, length).Trim());
                            }
                            if (cgData[i].StartsWith("Num-of-Vols:"))
                            {
                                int length = cgData[i].Length - 12;
                                cgnumofVol.Add(cgData[i].Substring(12, length).Trim());
                            }

                            if (cgData[i].Contains("Cluster-Id:"))
                            {
                                int len = cgData[i].Length - 11;
                                string s = cgData[i].Substring(11, len).Trim();
                                string[] arr = s.Split('\'');
                                foreach (string s1 in clusterName)
                                {
                                    if (arr.Contains(s1))
                                    {
                                        cgClusterName.Add(s1);
                                        break;
                                    }
                                }
                            }

                            if (cgData[i].StartsWith("Volume-List:"))
                            {
                                int j = i + 1;
                                try
                                {
                                    while (j < cgData.Length && cgData[j + 1].StartsWith(" ") || cgData[j + 1].StartsWith("\t"))
                                    {
                                        //populate
                                        if (cgData[j].Contains("Name:"))
                                        {

                                            int len = cgData[j].Trim().Length - 5;
                                            string s = cgData[j].Trim();
                                            cgMappingVolume.Add(s.Substring(5, len).Trim());
                                            cgMappingName.Add(cgName[cgName.Count - 1]);

                                        }

                                        j++;
                                    }
                                }
                                catch (Exception ex)
                                {

                                }



                            }



                        }
                    }
                    for (int i = 0; i < cgIndex.Count; i++)
                    {
                        SQLiteCommand insertSQL = new SQLiteCommand(@"insert into cgdetails values (@cgname ,@cgindex,@clustername,@numofvols)", m_dbConnection);
                        insertSQL.Parameters.AddWithValue("cgname", cgName[i]);
                        insertSQL.Parameters.AddWithValue("cgindex", cgIndex[i]);
                        insertSQL.Parameters.AddWithValue("clustername", cgClusterName[i]);
                        insertSQL.Parameters.AddWithValue("numofvols", cgnumofVol[i]);


                        try
                        {
                            insertSQL.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                    #endregion
                    if (bw.CancellationPending == true)
                    {
                        args.Cancel = true;
                        MessageBox.Show("Project creation cancelled..", "Cancelled!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return; // abort work, if it's cancelled

                    }
                #region CG Mapping Data
                    sql = @"CREATE TABLE cgmapping (cgname varchar(100), volname varchar(100))";
                    command = new SQLiteCommand(sql, m_dbConnection);
                    command.ExecuteNonQuery();

                    for (int i = 0; i < cgMappingName.Count; i++)
                    {
                        SQLiteCommand insertSQL = new SQLiteCommand(@"insert into cgmapping values (@cgname ,@volnames)", m_dbConnection);
                        insertSQL.Parameters.AddWithValue("cgname", cgMappingName[i]);
                        insertSQL.Parameters.AddWithValue("volnames", cgMappingVolume[i]);
                        try
                        {
                            insertSQL.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                    // bandwidth and IOPS

                    #endregion
                    if (bw.CancellationPending == true)
                    {
                        args.Cancel = true;
                        MessageBox.Show("Cancelled");
                        return; // abort work, if it's cancelled

                    }
                #region Initiator IOPS DB
                    sql = @"CREATE TABLE initiatorIOPS (initiatorname varchar(100), totalreadiops double,
                totalwriteiops double,totalread double,totalwrite varchar(20),totalbw varchar(20))";
                    command = new SQLiteCommand(sql, m_dbConnection);
                    command.ExecuteNonQuery();


                    path = folderBrowserDialog1.SelectedPath + "/xms/xmcli/ShowAllInitiators.out";
                    if (File.Exists(path))
                    {
                        string[] data = System.IO.File.ReadAllLines(path);
                        for (int i = 0; i < data.Length; i++)
                        {
                            if (data[i].StartsWith("Initiator-Name:") && data[i + 1].StartsWith("Index:"))
                            {

                                int len = data[i].Length - 15;
                                initiatorIOPSName.Add(data[i].Substring(15, len).Trim());

                            }

                            if (data[i].StartsWith("Total-Read-IOs:"))
                            {
                                int len = data[i].Length - 15;
                                totalReadIO.Add(double.Parse(data[i].Substring(15, len).Trim()));
                            }
                            if (data[i].StartsWith("Total-Write-IOs:"))
                            {
                                int len = data[i].Length - 16;
                                totalWriteIO.Add(double.Parse(data[i].Substring(16, len).Trim()));
                            }
                            if (data[i].StartsWith("Total-Reads:"))
                            {
                                int len = data[i].Length - 12;
                                string s = data[i].Substring(12, len).Trim();
                                if (s[s.Length - 1] == 'T')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));
                                    totalread.Add(num * 1024);

                                }
                                if (s[s.Length - 1] == 'K')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    totalread.Add(num / (1024 * 1024));


                                }
                                if (s[s.Length - 1] == 'P')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    totalread.Add(num * 1024 * 1024);

                                }
                                if (s[s.Length - 1] == 'M')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));

                                    totalread.Add(num / 1024);

                                }
                                if (s[s.Length - 1] == 'G')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));
                                    LBNotation.Add("GigaBytes");
                                    totalread.Add(num);

                                }
                                if (s.Length == 1)
                                {
                                    s = s.Trim();

                                    totalread.Add(0);

                                }

                            }
                            if (data[i].StartsWith("Total-Writes:"))
                            {
                                int len = data[i].Length - 13;

                                string s = data[i].Substring(13, len).Trim();
                                if (s[s.Length - 1] == 'T')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));
                                    totalwrites.Add(num * 1024);

                                }
                                if (s[s.Length - 1] == 'K')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));
                                    totalwrites.Add(num / (1024 * 1024));


                                }
                                if (s[s.Length - 1] == 'P')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));
                                    totalwrites.Add(num * 1024 * 1024);

                                }
                                if (s[s.Length - 1] == 'M')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));
                                    totalwrites.Add(num / 1024);

                                }
                                if (s[s.Length - 1] == 'G')
                                {
                                    s = s.Trim();
                                    double num = double.Parse(s.Substring(0, s.Length - 1));
                                    totalwrites.Add(num);

                                }
                                if (s.Length == 1)
                                {
                                    s = s.Trim();
                                    totalwrites.Add(0);

                                }
                            }
                            if (data[i].StartsWith("BW(MB/s):"))
                            {
                                int length = data[i].Length - 9;
                                string s = data[i].Substring(9, length).Trim();
                                double num = double.Parse(s.Substring(0, s.Length - 1));
                                totalbandwidth.Add(num);
                            }
                        }

                    }



                    for (int i = 0; i < initiatorIOPSName.Count; i++)
                    {
                        SQLiteCommand insertSQL = new SQLiteCommand(@"insert into initiatorIOPS
                    values (@ininame ,@totalreadiops,@totalwriteiops,@totalread,@totalwrite,@totalbw)", m_dbConnection);
                        insertSQL.Parameters.AddWithValue("ininame", initiatorIOPSName[i]);
                        insertSQL.Parameters.AddWithValue("totalreadiops", totalReadIO[i]);
                        insertSQL.Parameters.AddWithValue("totalwriteiops", totalWriteIO[i]);
                        insertSQL.Parameters.AddWithValue("totalread", totalread[i]);
                        insertSQL.Parameters.AddWithValue("totalwrite", totalwrites[i]);
                        insertSQL.Parameters.AddWithValue("totalbw", totalbandwidth[i]);
                        try
                        {
                            insertSQL.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                    #endregion
                    if (bw.CancellationPending == true)
                    {
                        args.Cancel = true;
                        MessageBox.Show("Project creation cancelled..", "Cancelled!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return; // abort work, if it's cancelled

                    }
                #region Storage Controller Details
                    // TO DO Storage Controller Details
                    filled = 0;
                    sql = @"CREATE TABLE scDetails (scname varchar(100), scindex varchar(10),clustername varchar(50),
                ipA varchar(30),ipB varchar(30),mgmtaddr varchar(30),
                schealth varchar(20),gatewayaddr varchar(30),subnetaddr varchar(30));";

                    command = new SQLiteCommand(sql, m_dbConnection);
                    command.ExecuteNonQuery();

                    string scInfo = folderBrowserDialog1.SelectedPath + "/xms/xmcli/ShowAllStorageControllers.out";
                    if (File.Exists(scInfo))
                    {
                        string[] scData = System.IO.File.ReadAllLines(scInfo);
                        for (int i = 0; i < scData.Length; i++)
                        {
                            if ((scData[i].StartsWith("Storage-Controller-Name:") || scData[i].StartsWith("Name:")) && scData[i + 1].Contains("Index:"))
                            {
                                filled++;
                                if (scData[i].StartsWith("Storage-Controller-Name:"))
                                {
                                    int length = scData[i].Length - 24;
                                    int indexlength = scData[i + 1].Length - 6;
                                    try
                                    {
                                        allScNames.Add(scData[i].Substring(24, length).Trim());
                                        scIndex.Add(scData[i + 1].Substring(6, indexlength).Trim());
                                    }
                                    catch (Exception e)
                                    {
                                        MessageBox.Show(e.Message);
                                    }
                                }
                                else
                                {
                                    int length = scData[i].Length - 5;
                                    int indexlength = scData[i + 1].Length - 6;
                                    try
                                    {
                                        allScNames.Add(scData[i].Substring(5, length).Trim());
                                        scIndex.Add(scData[i + 1].Substring(6, indexlength).Trim());
                                    }
                                    catch (Exception e)
                                    {
                                        MessageBox.Show(e.Message);
                                    }
                                }
                            }

                            if (scData[i].Contains("Cluster-Id:"))
                            {
                                int len = scData[i].Length - 11;
                                string s = scData[i].Substring(11, len).Trim();
                                string[] arr = s.Split('\'');
                                foreach (string s1 in clusterName)
                                {
                                    if (arr.Contains(s1))
                                    {
                                        scClusterName.Add(s1);
                                        break;
                                    }
                                }
                            }

                            if (scData[i].Contains("IB-Addr-1:"))
                            {
                                int length = scData[i].Length - 10;
                                scmgmtIPaddrA.Add(scData[i].Substring(10, length).Trim());

                            }
                            if (scData[i].Contains("IB-Addr-2:"))
                            {
                                int length = scData[i].Length - 10;
                                scmgmtIPaddrB.Add(scData[i].Substring(10, length).Trim());

                            }
                            if (scData[i].Contains("Mgr-Addr:"))
                            {
                                int length = scData[i].Length - 9;
                                scmgmtIP.Add(scData[i].Substring(9, length).Trim());
                            }
                            if (scData[i].Contains("Free-Disk:"))
                            {
                                int length = scData[i + 1].Length - 6;
                                schealthState.Add(scData[i + 1].Substring(6, length).Trim());
                            }
                            if (scData[i].Contains("MGMT-GW-IP:"))
                            {
                                int length = scData[i].Length - 11;
                                scgatewayIP.Add(scData[i].Substring(11, length).Trim());
                            }
                            if (scData[i].Contains("Mgr-Addr-Subnet:"))
                            {
                                int length = scData[i].Length - 16;
                                scsubnetAddr.Add(scData[i].Substring(16, length).Trim());
                            }

                        }
                        int temp = filled;
                        while (temp != 0)
                        {
                            if (!(scClusterName.Count == filled))
                            {
                                scClusterName.Add("N/A");
                            }
                            if (!(schealthState.Count == filled))
                            {
                                schealthState.Add("N/A");
                            }
                            if (!(scsubnetAddr.Count == filled))
                            {
                                scsubnetAddr.Add("N/A");
                            }
                            if (!(scgatewayIP.Count == filled))
                            {
                                scgatewayIP.Add("N/A");
                            }
                            if (!(scmgmtIP.Count == filled))
                            {
                                scmgmtIP.Add("N/A");
                            }
                            if (!(scmgmtIPaddrB.Count == filled))
                            {
                                scmgmtIPaddrB.Add("N/A");
                            }
                            if (!(scmgmtIPaddrA.Count == filled))
                            {
                                scmgmtIPaddrA.Add("N/A");
                            }
                            if (!(scClusterName.Count == filled))
                            {
                                scClusterName.Add("N/A");
                            }

                            temp--;
                        }
                    }

                    for (int i = 0; i < scIndex.Count; i++)
                    {
                        SQLiteCommand insertSQL = new SQLiteCommand(@"insert into scDetails (scname, 
                    scindex,clustername,ipA,ipB
                    ,mgmtaddr,schealth,gatewayaddr,subnetaddr) values 
                    (@scname,@scindex,@clustername,@ipA,@ipB,@mgmtaddr,@schealth,@gatewayaddr,@subnetaddr)", m_dbConnection);

                        insertSQL.Parameters.AddWithValue("scname", allScNames[i]);
                        insertSQL.Parameters.AddWithValue("scindex", scIndex[i]);
                        insertSQL.Parameters.AddWithValue("clustername", scClusterName[i]);
                        insertSQL.Parameters.AddWithValue("ipA", scmgmtIPaddrA[i]);
                        insertSQL.Parameters.AddWithValue("ipB", scmgmtIPaddrB[i]);
                        insertSQL.Parameters.AddWithValue("mgmtaddr", scmgmtIP[i]);
                        insertSQL.Parameters.AddWithValue("schealth", schealthState[i]);
                        insertSQL.Parameters.AddWithValue("gatewayaddr", scgatewayIP[i]);
                        insertSQL.Parameters.AddWithValue("subnetaddr", scsubnetAddr[i]);
                        try
                        {
                            insertSQL.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }

                    }
                    if (bw.CancellationPending == true)
                    {
                        args.Cancel = true;
                        MessageBox.Show("Cancelled");
                        return; // abort work, if it's cancelled

                    }
                    b.ReportProgress(70);
                    Thread.Sleep(100);
                    #endregion
                #region Alert Data DB
                    // Get Alert data
                    filled = 0;

                    string targetPath = folderBrowserDialog1.SelectedPath + "/xms/xmcli/ShowAllAlerts.out";
                    if (File.Exists(targetPath))
                    {

                        string[] targetData = System.IO.File.ReadAllLines(targetPath);
                        for (int i = 0; i < targetData.Length; i++)
                        {
                            if (targetData[i].StartsWith("Name:") && targetData[i + 1].StartsWith("Index:"))
                            {
                                filled++;
                                int length = targetData[i + 1].Length - 6;
                                alertIndex.Add(targetData[i + 1].Substring(6, length).Trim());


                            }
                            if (targetData[i].StartsWith("Alert-Code:"))
                            {
                                int length = targetData[i].Length - 11;
                                alertCode.Add(targetData[i].Substring(11, length).Trim());

                            }
                            if (targetData[i].StartsWith("State:"))
                            {
                                int length = targetData[i].Length - 6;
                                alertState.Add(targetData[i].Substring(6, length).Trim());

                            }
                            if (targetData[i].StartsWith("Alert-Type:"))
                            {
                                int length = targetData[i].Length - 11;
                                alertType.Add(targetData[i].Substring(11, length).Trim());
                            }

                            if (targetData[i].StartsWith("Entity:"))
                            {
                                int length = targetData[i].Length - 7;
                                alertentityData.Add(targetData[i].Substring(7, length).Trim());
                                length = targetData[i - 1].Length - 5;
                                alertentityName.Add(targetData[i - 1].Substring(5, length).Trim());
                                length = targetData[i + 1].Length - 12;
                                alertDesc.Add(targetData[i + 1].Substring(12, length).Trim());
                            }

                            if (targetData[i].StartsWith("Object-Severity:"))
                            {
                                int length = targetData[i].Length - 16;
                                objectSev.Add(targetData[i].Substring(16, length).Trim());
                            }
                            if (targetData[i].StartsWith("Raise-Time:"))
                            {
                                int length = targetData[i].Length - 11;
                                raiseTime.Add(targetData[i].Substring(11, length).Trim());
                            }
                            if (targetData[i].StartsWith("Cluster-Name:"))
                            {
                                int length = targetData[i].Length - 13;
                                alertclusterName.Add(targetData[i].Substring(13, length).Trim().ToUpper());
                            }

                        }
                        int temp = filled;
                        while (temp != 0)
                        {
                            if (!(alertCode.Count == filled))
                            {
                                alertCode.Add("N/A");
                            }
                            if (!(alertState.Count == filled))
                            {
                                alertState.Add("N/A");
                            }
                            if (!(alertType.Count == filled))
                            {
                                alertType.Add("N/A");
                            }
                            if (!(alertentityData.Count == filled))
                            {
                                alertentityData.Add("N/A");
                            }
                            if (!(alertentityName.Count == filled))
                            {
                                alertentityName.Add("N/A");
                            }
                            if (!(alertDesc.Count == filled))
                            {
                                alertDesc.Add("N/A");
                            }
                            if (!(objectSev.Count == filled))
                            {
                                objectSev.Add("N/A");
                            }
                            if (!(raiseTime.Count == filled))
                            {
                                raiseTime.Add("N/A");
                            }
                            if (!(alertclusterName.Count == filled))
                            {
                                alertclusterName.Add("N/A");
                            }

                            temp--;
                        }
                    }
                    sql = @"CREATE TABLE alertdata (alertcode varchar(100), alertindex varchar(10),alertstate varchar(50),
                alerttype varchar(30),alertentitydata varchar(30),alertentityname varchar(50),
                alertdesc varchar(100),objectsev varchar(30),raisetime varchar(50),alertclusterName varchar(50));";

                    command = new SQLiteCommand(sql, m_dbConnection);
                    command.ExecuteNonQuery().ToString();


                    for (int i = 0; i < alertIndex.Count; i++)
                    {

                        SQLiteCommand insertSQL = new SQLiteCommand(@"insert into alertdata (alertcode, 
                    alertindex,alertstate,alerttype,alertentitydata
                    ,alertentitydata,alertentityname,alertdesc,objectsev,raisetime,alertclusterName) values 
                    (@alertcode,@alertindex,@alertstate,@alerttype,@alertentitydata,@alertentitydata
                    ,@alertentityname,@alertdesc,@objectsev,@raisetime,@alertclusterName)", m_dbConnection);

                        insertSQL.Parameters.AddWithValue("alertcode", alertCode[i]);
                        insertSQL.Parameters.AddWithValue("alertindex", alertIndex[i]);
                        insertSQL.Parameters.AddWithValue("alertstate", alertState[i]);
                        insertSQL.Parameters.AddWithValue("alerttype", alertType[i]);
                        insertSQL.Parameters.AddWithValue("alertentitydata", alertentityData[i]);
                        insertSQL.Parameters.AddWithValue("alertentityname", alertentityName[i]);
                        insertSQL.Parameters.AddWithValue("alertdesc", alertDesc[i]);
                        insertSQL.Parameters.AddWithValue("objectsev", objectSev[i]);
                        insertSQL.Parameters.AddWithValue("raisetime", raiseTime[i]);
                        insertSQL.Parameters.AddWithValue("alertclusterName", alertclusterName[i]);
                        try
                        {
                            insertSQL.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }

                    }
                    if (bw.CancellationPending == true)
                    {
                        args.Cancel = true;
                        MessageBox.Show("Cancelled");
                        return; // abort work, if it's cancelled

                    }
                    b.ReportProgress(80);
                    Thread.Sleep(100);
                    #endregion
                #region Target Details DB
                    //Target details
                    filled = 0;
                    targetPath = folderBrowserDialog1.SelectedPath + "/xms/xmcli/ShowAllTargets.out";
                    if (File.Exists(targetPath))
                    {
                        string[] targetData = System.IO.File.ReadAllLines(targetPath);
                        for (int i = 0; i < targetData.Length; i++)
                        {
                            if (targetData[i].StartsWith("Name:") && targetData[i + 1].StartsWith("Index:"))
                            {
                                filled++;
                                int length = targetData[i].Length - 5;
                                int length2 = targetData[i + 1].Length - 6;
                                targetName.Add(targetData[i].Substring(5, length).Trim());
                                targetIndex.Add(targetData[i + 1].Substring(6, length2).Trim());

                            }
                            if (targetData[i].StartsWith("Jumbo-Frames:"))
                            {
                                int length = targetData[i].Length - 13;
                                targetjumboFrames.Add(targetData[i].Substring(13, length).Trim());
                            }
                            if (targetData[i].StartsWith("MTU:"))
                            {
                                int length = targetData[i].Length - 4;
                                targetmtuData.Add(targetData[i].Substring(4, length).Trim());
                            }
                            if (targetData[i].StartsWith("Port-Address:"))
                            {
                                int length = targetData[i].Length - 13;
                                targetportAddress.Add(targetData[i].Substring(13, length).Trim());
                            }

                            if (targetData[i].StartsWith("Mac-Addr:"))
                            {
                                int length = targetData[i].Length - 9;
                                if (length < 5)
                                {
                                    targetmacAddress.Add("N/A");
                                }
                                else
                                {
                                    targetmacAddress.Add(targetData[i].Substring(9, length).Trim());
                                }
                            }

                            if (targetData[i].StartsWith("Port-Speed:"))
                            {
                                int length = targetData[i].Length - 11;
                                targetportSpeed.Add(targetData[i].Substring(11, length).Trim());
                            }
                            if (targetData[i].StartsWith("Port-State:"))
                            {
                                int length = targetData[i].Length - 11;
                                targetportState.Add(targetData[i].Substring(11, length).Trim());
                            }
                            if (targetData[i].StartsWith("Port-Type:"))
                            {
                                int length = targetData[i].Length - 10;
                                targetportType.Add(targetData[i].Substring(10, length).Trim().ToUpper());
                            }
                            if (targetData[i].StartsWith("Cluster-Id:"))
                            {
                                int len = targetData[i].Length - 11;
                                string s = targetData[i].Substring(11, len).Trim();
                                string[] arr = s.Split('\'');
                                foreach (string s1 in clusterName)
                                {
                                    if (arr.Contains(s1))
                                    {
                                        targetscClusterName.Add(s1);
                                        break;
                                    }
                                }
                            }



                        }
                        int temp = filled;
                        while (temp != 0)
                        {
                            if (!(targetjumboFrames.Count == filled))
                            {
                                targetjumboFrames.Add("N/A");
                            }
                            if (!(targetscClusterName.Count == filled))
                            {
                                targetscClusterName.Add("N/A");
                            }
                            if (!(targetportType.Count == filled))
                            {
                                targetportType.Add("N/A");
                            }
                            if (!(targetportState.Count == filled))
                            {
                                targetportState.Add("N/A");
                            }
                            if (!(targetportSpeed.Count == filled))
                            {
                                targetportSpeed.Add("N/A");
                            }
                            if (!(targetmacAddress.Count == filled))
                            {
                                targetmacAddress.Add("N/A");
                            }
                            if (!(targetportAddress.Count == filled))
                            {
                                targetportAddress.Add("N/A");
                            }
                            if (!(targetmtuData.Count == filled))
                            {
                                targetmtuData.Add("N/A");
                            }


                            temp--;
                        }

                    }

                    sql = @"CREATE TABLE targetdata (targetname varchar(100), targetindex varchar(10),targetjumboframes varchar(50),
                targetmtudata varchar(30),targetportaddress varchar(30),targetmacaddress varchar(50),targetportstate varchar(50),
                targetportspeed varchar(100),targetporttype varchar(30),targetsclusterName varchar(50));";

                    command = new SQLiteCommand(sql, m_dbConnection);
                    command.ExecuteNonQuery();


                    for (int i = 0; i < targetIndex.Count; i++)
                    {

                        SQLiteCommand insertSQL = new SQLiteCommand(@"insert into targetdata (targetname, 
                    targetindex,targetjumboframes,targetmtudata,targetportaddress,targetportstate
                    ,targetmacaddress,targetportspeed,targetporttype,targetsclusterName) values 
                    (@targetname,@targetindex,@targetjumboframes,@targetmtudata,@targetportaddress,@targetportstate,@targetmacaddress
                    ,@targetportspeed,@targetporttype,@targetscclusterName)", m_dbConnection);

                        insertSQL.Parameters.AddWithValue("targetname", targetName[i]);
                        insertSQL.Parameters.AddWithValue("targetindex", targetIndex[i]);
                        insertSQL.Parameters.AddWithValue("targetjumboframes", targetjumboFrames[i]);
                        insertSQL.Parameters.AddWithValue("targetmtudata", targetmtuData[i]);
                        insertSQL.Parameters.AddWithValue("targetportaddress", targetportAddress[i]);
                        insertSQL.Parameters.AddWithValue("targetmacaddress", targetmacAddress[i]);
                        insertSQL.Parameters.AddWithValue("targetportspeed", targetportSpeed[i]);
                        insertSQL.Parameters.AddWithValue("targetporttype", targetportType[i]);
                        insertSQL.Parameters.AddWithValue("targetscclusterName", targetscClusterName[i]);
                        insertSQL.Parameters.AddWithValue("targetportstate", targetportState[i]);
                        try
                        {
                            insertSQL.ExecuteNonQuery();

                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }

                    }
                    if (bw.CancellationPending == true)
                    {
                        args.Cancel = true;
                        MessageBox.Show("Cancelled");
                        return; // abort work, if it's cancelled
                      
                    }
                    b.ReportProgress(90);
                    Thread.Sleep(100);
                });

                    #endregion

                // what to do when progress changed (update the progress bar for example)
                bw.ProgressChanged += new ProgressChangedEventHandler(
                delegate(object o, ProgressChangedEventArgs args)
                {
                    if (bw.CancellationPending == true)
                    {
                        percent_completed.Value = 0;
                        load_button.Enabled = true;
                        delete_button.Enabled = true;
                        ok_btn.Enabled = true;
                        percent_completed.Visible = false;
                        System.Diagnostics.Process.Start(Application.ExecutablePath); // to start new instance of application
                        this.Close(); //to turn off current app

                    }
                    else
                    {
                        percent_completed.Value = args.ProgressPercentage;
                    }
                });
               
                // what to do when worker completes its task (notify the user)
                bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(
                delegate(object o, RunWorkerCompletedEventArgs args)
                {
                    if (args.Cancelled != true)
                    {
                        percent_completed.Value = 100;
                        percent_completed.Visible = false;
                        MessageBox.Show("Project created Successfully");
                        LandingPage newForm = new LandingPage(m_dbConnection, folderBrowserDialog1.SelectedPath);
                        this.Hide();
                        newForm.ShowDialog();
                        this.Close();
                    }
                    else
                    {
                        percent_completed.Value = 0;
                        load_button.Enabled = true;
                        delete_button.Enabled = true;
                        ok_btn.Enabled = true;
                        percent_completed.Visible = false;
                    }
                });

                bw.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to create project !! Please contact Administraor :" + ex.Message, "Error !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                // He catches all the exception  , Jonty Rhodes
            }
            // Finally that method ended !! Praise the lord
        }
        #endregion
        #region Get IG Names
        private void getAllInitiatorGroup()
        {
            // Added this to new module , should do other as well !!
            string path = folderBrowserDialog1.SelectedPath + "/xms/xmcli/ShowAllInitiatorGroups.out";
            if (File.Exists(path))
            {
                string[] igData = System.IO.File.ReadAllLines(path);
                for (int i = 0; i < igData.Length; i++)
                {
                    if ((igData[i].StartsWith("IG-Name:")||igData[i].StartsWith("Name:")) && igData[i + 1].StartsWith("Index:"))
                    {
                        if (igData[i].StartsWith("IG-Name:"))
                        {
                            initiatorGroupData.Add(igData[i].Substring(8, igData[i].Length - 8).Trim());
                        }
                        else
                        {
                            initiatorGroupData.Add(igData[i].Substring(5, igData[i].Length - 5).Trim());
                        }
                    }
                }

            }
        }
        #endregion
        #region Cancel Button
        private void cancel_btn_Click(object sender, EventArgs e)
        {
            if (ok_btn.Enabled == false)
            {
                DialogResult res = MessageBox.Show("Are you sure you want to cancel the log analysis ? ", "Confirm ?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (res == DialogResult.Yes)
                {
                    bw.CancelAsync();
                    load_button.Enabled = false;
                    delete_button.Enabled = false;
                    load_button.BackColor = Color.White;
                    // delete_button.ForeColor = Color.Silver;
                    delete_button.BackColor = Color.White;
                    upload_logs_1.Visible = false;
                    proj_id_lbl.Visible = false;
                    project_id_text.Visible = false;
                    cluster1_lbl.Visible = false;
                    cluster1_text.Visible = false;
                    upload_pic_1.Visible = false;
                    info_lbl.Visible = false;
                    ok_btn.Visible = false;
                    cancel_btn.Visible = false;
                }
                else
                {

                }
            }
            else
            {
                load_button.Enabled = true;
                delete_button.Enabled = true;
                // load_button.ForeColor = Color.Silver;
                load_button.BackColor = Color.White;
                // delete_button.ForeColor = Color.Silver;
                delete_button.BackColor = Color.White;
                upload_logs_1.Visible = false;
                proj_id_lbl.Visible = false;
                project_id_text.Visible = false;
                cluster1_lbl.Visible = false;
                cluster1_text.Visible = false;
                upload_pic_1.Visible = false;
                info_lbl.Visible = false;
                ok_btn.Visible = false;
                cancel_btn.Visible = false;
            }
        }
        #endregion
        #region Form Load
        private void Home_Page_Load(object sender, EventArgs e)
        {
            //Create a default directory
            System.IO.Directory.CreateDirectory("C:\\XIOLogAnalyzer");
            CheckExpiration();
            //UpdateRunCount();
        }
        #endregion
        #region Upload Logs Click
        private void upload_logs_1_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                filled = 0;
                string logpath_Clusterdetails = folderBrowserDialog1.SelectedPath + "/xms/xmcli/ShowAllClusters.out";
                if (File.Exists(logpath_Clusterdetails))
                {
                    string[] data = System.IO.File.ReadAllLines(logpath_Clusterdetails);
                    // On load to display number of clusters , TODO re-use module
                    for (int i = 0; i < data.Length; i++)
                    {
                        if ((data[i].StartsWith("Cluster-Name:") && data[i + 1].StartsWith("Index:")) || (data[i].StartsWith("Name:") && data[i + 1].StartsWith("Index:") && (!data[i + 2].StartsWith("BW(MB/s):"))))
                        {

                            filled++;
                           
                        }
                    }
                    upload_pic_1.Visible = true;
                    upload_pic_1.Image = Properties.Resources.checked_checkbox_512;
                    cluster1_text.Text = folderBrowserDialog1.SelectedPath;
                    MessageBox.Show("Logs uploaded successfully , There are "+filled+" cluster(s) in this log");
                    success = 1;
                }
                else
                {
                    success = -1;
                    upload_pic_1.Visible = true;
                    MessageBox.Show("Please select the 'latest' folder which contains the log", "Failed to Upload", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    upload_pic_1.Image = Properties.Resources.red_checkbox;
                }
            }
        }
        #endregion
        #region Load Button Click
        private void load_button_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string filename = openFileDialog1.FileName.ToString();
                string str  = filename.Substring(filename.Length-6,6);
                //check if the file selected is the right one !
                if (str=="sqlite")
                {
                    MessageBox.Show("Project loaded Successfully","Success",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    try
                    {
                        
                        SQLiteConnection conn = new SQLiteConnection("Data Source=" + openFileDialog1.InitialDirectory + openFileDialog1.FileName + ";Version=3;");
                        conn.Open();
                        LandingPage newForm = new LandingPage(conn, folderBrowserDialog1.SelectedPath);
                        this.Hide();
                        newForm.ShowDialog();
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("FAILED");
                }
            }
        }
        #endregion
        #region Delete Button
        private void delete_button_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string filename = openFileDialog1.InitialDirectory + openFileDialog1.FileName;
                if (filename.Contains(".sqlite"))
                {
                    DialogResult res = MessageBox.Show("Are you sure you want to delete this project file ?", "Confirm!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    if (res == DialogResult.Yes)
                    {
                        File.Delete(filename);
                        MessageBox.Show("File Deleted !!","Success",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Not a valid project file !", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        #endregion

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
