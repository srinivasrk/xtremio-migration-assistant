﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;
namespace xtremeIO
{
    
    public partial class Script_Generator_landingpage : Form
    {
        public String san_id;
        private SQLiteConnection m_dbConnection;
        public Dictionary<int, String> clusterlist = new Dictionary<int,String>();
        public Dictionary<String, int> cluster_vol_index = new Dictionary<String, int>();
        private String select_data_source = "host";
        private DataTable mapping_table = new DataTable();
        private DataTable lun_details_table =new DataTable();
        public DataTable host_details_table =new DataTable();
        public Dictionary<String, String> vol_name_tagging = new Dictionary<string, string>();
       // private long vol_count = 0;
        bool row_color_change = true;
        private long row_index = -1;
        private bool settargetvol;
        private DataTable cluster_table = new DataTable();
        public Dictionary<string, string> cluster_name_mapping;
        private bool format_yes = false;
        public Script_Generator_landingpage(String param1,SQLiteConnection param2,String params3,String params4)
        {
            InitializeComponent();
            san_id = param1;
            m_dbConnection = param2;
            xms_ip_lbl.Text = params3;
            xms_os_lbl.Text = params4;
        }

        private void Script_Generator_landingpage_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.Columns["disk_size"].Visible = false;
            dataGridView1.Columns["array_device"].Visible = false;
            dataGridView1.Columns["array_serial"].Visible = false;
            dataGridView1.Columns["server_name"].Visible = true;
            dataGridView1.Columns["wwpn"].Visible = true;
            dataGridView1.Columns["ip_addr"].Visible = false;
            dataGridView1.Columns["os_name"].Visible = true;
            dataGridView1.Columns["target_device"].Visible = false;
            dataGridView1.Columns["array_device_wwn"].Visible = false;
            DataTable dt = new DataTable();
            string sql = "select * from SAN_" + san_id + "_Host where server_name!='' ";
            SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);

            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
            da.Fill(dt);
           /// dt.Columns["server_name"].ColumnName = "Server Name";
         //  dt.Columns["os_name"].ColumnName = "OS Type";
          //  dt.Columns["wwpn"].ColumnName = "Server WWPN";
           // dt.Columns["ip_addr"].ColumnName = "IP Address";
            select_data_source = "host";
            dataGridView1.DataSource = dt;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;


            host_details_table.Clear();
           
             sql = "select * from SAN_" + san_id + "_Host where server_name!='' ";
             cmd = new SQLiteCommand(sql, m_dbConnection);

            da = new SQLiteDataAdapter(cmd);
            da.Fill(host_details_table);
            total_host_lbl.Text =  total_host_lbl.Text + " " + host_details_table.Rows.Count.ToString();

            try
            {
                sql = "select count(*) from ( select * from SAN_" + san_id + "_Lun where server_name!='' and array_device != ' ' and disk_size!='2880' group by array_device,array_serial) ";
                cmd = new SQLiteCommand(sql, m_dbConnection);
                Int32 count = Convert.ToInt32(cmd.ExecuteScalar());

                if (count > 0)
                {
                    total_lun_lbl.Text = total_lun_lbl.Text + " " + count.ToString();
                }
                else
                {
                    total_lun_lbl.Text = total_lun_lbl.Text + " 0";
                }
                da = new SQLiteDataAdapter(cmd);
                da.Fill(host_details_table);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " " + ex.StackTrace);
            }
           
        }
        private bool isCellSameValue(int column, int row)
        {
           
                DataGridViewCell cell1 = dataGridView1[5, row];
                DataGridViewCell cell2 = dataGridView1[5, row - 1];
              
                DataGridViewCell cell11 = dataGridView1[7-1,row];
                DataGridViewCell cell12 = dataGridView1[7-1,row-1];
             
                if (cell1.Value == null || cell2.Value == null)
                {
                    return false;
                }
                return( (cell1.Value.ToString() == cell2.Value.ToString()) && (cell11.Value.ToString() == cell12.Value.ToString()) );
            
          

        }

      
        private void host_details_btn_Click(object sender, EventArgs e)
        {
            if (generate_script_btn.Visible == true)
            {
                DialogResult result = MessageBox.Show("You will lose the From - To List generated if you move to different TAB, Do you want to continue ?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    host_details_table.Clear();
                    dataGridView1.Columns["disk_size"].Visible = false;
                    dataGridView1.Columns["array_device"].Visible = false;
                    dataGridView1.Columns["array_serial"].Visible = false;
                    dataGridView1.Columns["server_name"].Visible = true;
                    dataGridView1.Columns["wwpn"].Visible = true;
                    dataGridView1.Columns["ip_addr"].Visible = false;
                    dataGridView1.Columns["os_name"].Visible = true;
                    dataGridView1.Columns["target_device"].Visible = false;
                    dataGridView1.Columns["array_device_wwn"].Visible = false;
                    string sql = "select * from SAN_" + san_id + "_Host where server_name!='' ";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                    da.Fill(host_details_table);
                    // dt.Columns["server_name"].ColumnName = "Server Name";
                    // dt.Columns["os_name"].ColumnName = "OS Type";
                    // dt.Columns["wwpn"].ColumnName = "Server WWPN";
                    // dt.Columns["ip_addr"].ColumnName = "IP Address";
                    select_data_source = "host";
                    dataGridView1.DataSource = host_details_table;
                    dataGridView1.Columns["server_name"].DisplayIndex = 0;
                    from_to_btn.Visible = false;
                    generate_script_btn.Visible = false;
                    dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                    // total_host_lbl.Text = host_details_table.Rows[0][host_details_table.Columns.Count - 1].ToString();

                }
            }
            else
            {
                host_details_table.Clear();
                dataGridView1.Columns["disk_size"].Visible = false;
                dataGridView1.Columns["array_device"].Visible = false;
                dataGridView1.Columns["array_serial"].Visible = false;
                dataGridView1.Columns["server_name"].Visible = true;
                dataGridView1.Columns["wwpn"].Visible = true;
                dataGridView1.Columns["ip_addr"].Visible = false;
                dataGridView1.Columns["os_name"].Visible = true;
                dataGridView1.Columns["target_device"].Visible = false;
                dataGridView1.Columns["array_device_wwn"].Visible = false;
                string sql = "select * from SAN_" + san_id + "_Host where server_name!='' ";
                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);

                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                da.Fill(host_details_table);
                // dt.Columns["server_name"].ColumnName = "Server Name";
                // dt.Columns["os_name"].ColumnName = "OS Type";
                // dt.Columns["wwpn"].ColumnName = "Server WWPN";
                // dt.Columns["ip_addr"].ColumnName = "IP Address";
                select_data_source = "host";
                dataGridView1.DataSource = host_details_table;
                dataGridView1.Columns["server_name"].DisplayIndex = 0;
                from_to_btn.Visible = false;
                generate_script_btn.Visible = false;
                dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                // total_host_lbl.Text = host_details_table.Rows[0][host_details_table.Columns.Count - 1].ToString();
            }
        }

        private void lun_details_btn_Click(object sender, EventArgs e)
        {
            if (generate_script_btn.Visible == true)
            {
                DialogResult result = MessageBox.Show("You will lose the From - To List generated if you move to different TAB, Do you want to continue ?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {

                    lun_details_table.Clear();
                    dataGridView1.Columns["disk_size"].Visible = true;
                    dataGridView1.Columns["array_device"].Visible = true;
                    dataGridView1.Columns["server_name"].Visible = true;
                    dataGridView1.Columns["wwpn"].Visible = false;
                    dataGridView1.Columns["ip_addr"].Visible = false;
                    dataGridView1.Columns["os_name"].Visible = false;
                    dataGridView1.Columns["array_serial"].Visible = true;
                    dataGridView1.Columns["target_device"].Visible = false;
                    dataGridView1.Columns["array_device_wwn"].Visible = true;
                    string sql = "select * from SAN_" + san_id + "_Lun where server_name!='' and array_device!=' ' and disk_size!='2880' order by array_device ";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);

                    SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                    da.Fill(lun_details_table);
                    //  dt.Columns["server_name"].ColumnName = "Server Name";
                    //   dt.Columns["disk_size"].ColumnName = "Disk Size(KB)";
                    //    dt.Columns["array_device"].ColumnName = "Array Device";
                    select_data_source = "lun";
                    dataGridView1.DataSource = lun_details_table;
                    dataGridView1.Columns["array_device_wwn"].DisplayIndex = 4;
                    dataGridView1.Columns["array_device"].DisplayIndex = 3;
                    dataGridView1.Columns["disk_size"].DisplayIndex = 2;
                    dataGridView1.Columns["array_serial"].DisplayIndex = 1;
                    dataGridView1.Columns["server_name"].DisplayIndex = 0;
                    dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                    generate_script_btn.Visible = false;
                    from_to_btn.Visible = false;
                    // total_lun_lbl.Text = lun_details_table.Rows[0][lun_details_table.Columns.Count - 1].ToString();
                }
            }
            else
            {
                lun_details_table.Clear();
                dataGridView1.Columns["disk_size"].Visible = true;
                dataGridView1.Columns["array_device"].Visible = true;
                dataGridView1.Columns["server_name"].Visible = true;
                dataGridView1.Columns["wwpn"].Visible = false;
                dataGridView1.Columns["ip_addr"].Visible = false;
                dataGridView1.Columns["os_name"].Visible = false;
                dataGridView1.Columns["array_serial"].Visible = true;
                dataGridView1.Columns["target_device"].Visible = false;
                dataGridView1.Columns["array_device_wwn"].Visible = true;
                string sql = "select * from SAN_" + san_id + "_Lun where server_name!='' and array_device!=' ' and disk_size!='2880' order by array_device ";
                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);

                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                da.Fill(lun_details_table);
                //  dt.Columns["server_name"].ColumnName = "Server Name";
                //   dt.Columns["disk_size"].ColumnName = "Disk Size(KB)";
                //    dt.Columns["array_device"].ColumnName = "Array Device";
                select_data_source = "lun";
                dataGridView1.DataSource = lun_details_table;
                dataGridView1.Columns["array_device_wwn"].DisplayIndex = 4;
                dataGridView1.Columns["array_device"].DisplayIndex = 3;
                dataGridView1.Columns["disk_size"].DisplayIndex = 2;
                dataGridView1.Columns["array_serial"].DisplayIndex = 1;
                dataGridView1.Columns["server_name"].DisplayIndex = 0;
                dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                generate_script_btn.Visible = false;
                from_to_btn.Visible = false;
                // total_lun_lbl.Text = lun_details_table.Rows[0][lun_details_table.Columns.Count - 1].ToString();

            }
        }

        private void lun_mapping_btn_Click(object sender, EventArgs e)
        {
            if (generate_script_btn.Visible == true)
            {
                DialogResult result = MessageBox.Show("You will lose the From - To List generated if you move to different TAB, Do you want to continue ?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    mapping_table.Clear();
                    mapping_table.Columns.Clear();
                    if (cluster_name_mapping != null)
                    {
                        cluster_name_mapping.Clear();
                    }
                    cluster_vol_index.Clear();
                    dataGridView1.Columns["array_serial"].Visible = true;
                    dataGridView1.Columns["disk_size"].Visible = true;
                    dataGridView1.Columns["array_device"].Visible = true;
                    dataGridView1.Columns["server_name"].Visible = true;
                    dataGridView1.Columns["wwpn"].Visible = false;
                    dataGridView1.Columns["ip_addr"].Visible = false;
                    dataGridView1.Columns["os_name"].Visible = false;
                    dataGridView1.Columns["target_device"].Visible = false;
                    dataGridView1.Columns["array_device_wwn"].Visible = true;
                    from_to_btn.Visible = true;
                    string sql = @"select array_device_wwn, array_device,MySelect.server_name as server_name,MySelect.os_name as os_name,MySelect.wwpn as wwpn,MySelect.ip_addr as ip_addr,disk_size,array_serial 
            from (select * from san_" + san_id + "_Host where server_name!='') as MySelect join san_" + san_id + "_Lun where san_" + san_id + "_lun.server_name = MySelect.server_name and array_device!=' ' and disk_size!='2880' order by array_device,server_name,array_serial";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                    SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);

                    da.Fill(mapping_table);
                    //  dt.Columns["server_name"].ColumnName = "Server Name";
                    //   dt.Columns["os_name"].ColumnName = "OS Name";
                    //   dt.Columns["wwpn"].ColumnName = "WWPN Address";
                    //    dt.Columns["ip_addr"].ColumnName = "IP Address";
                    //    dt.Columns["disk_size"].ColumnName = "Disk Size";
                    //    dt.Columns["array_device"].ColumnName = "Array Device";
                    select_data_source = "mapping";
                    dataGridView1.Columns["array_device_wwn"].DisplayIndex = 4;
                    dataGridView1.Columns["array_device"].DisplayIndex = 2;
                    dataGridView1.Columns["disk_size"].DisplayIndex = 3;
                    dataGridView1.Columns["server_name"].DisplayIndex = 0;
                    dataGridView1.Columns["array_serial"].DisplayIndex = 1;
                    dataGridView1.DataSource = mapping_table;
                    dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                    mapping_table.Columns.Add("target_device");
                }
            }
            else
            {
                mapping_table.Clear();
                mapping_table.Columns.Clear();
                if (cluster_name_mapping != null)
                {
                    cluster_name_mapping.Clear();
                }
                cluster_vol_index.Clear();
                dataGridView1.Columns["array_serial"].Visible = true;
                dataGridView1.Columns["disk_size"].Visible = true;
                dataGridView1.Columns["array_device"].Visible = true;
                dataGridView1.Columns["server_name"].Visible = true;
                dataGridView1.Columns["wwpn"].Visible = false;
                dataGridView1.Columns["ip_addr"].Visible = false;
                dataGridView1.Columns["os_name"].Visible = false;
                dataGridView1.Columns["target_device"].Visible = false;
                dataGridView1.Columns["array_device_wwn"].Visible = true;
                from_to_btn.Visible = true;
                string sql = @"select array_device_wwn,array_device,MySelect.server_name as server_name,MySelect.os_name as os_name,MySelect.wwpn as wwpn,MySelect.ip_addr as ip_addr,disk_size,array_serial 
            from (select * from san_" + san_id + "_Host where server_name!='') as MySelect join san_" + san_id + "_Lun where san_" + san_id + "_lun.server_name = MySelect.server_name and array_device!=' ' and disk_size!='2880' order by array_device,server_name,array_serial";
                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);

                da.Fill(mapping_table);
                //  dt.Columns["server_name"].ColumnName = "Server Name";
                //   dt.Columns["os_name"].ColumnName = "OS Name";
                //   dt.Columns["wwpn"].ColumnName = "WWPN Address";
                //    dt.Columns["ip_addr"].ColumnName = "IP Address";
                //    dt.Columns["disk_size"].ColumnName = "Disk Size";
                //    dt.Columns["array_device"].ColumnName = "Array Device";
                select_data_source = "mapping";
                dataGridView1.Columns["array_device_wwn"].DisplayIndex = 4;
                dataGridView1.Columns["array_device"].DisplayIndex = 2;
                dataGridView1.Columns["disk_size"].DisplayIndex = 3;
                dataGridView1.Columns["server_name"].DisplayIndex = 0;
                dataGridView1.Columns["array_serial"].DisplayIndex = 1;
                dataGridView1.DataSource = mapping_table;
                dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                mapping_table.Columns.Add("target_device");
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
           
            if (select_data_source == "mapping")
            {
               
                e.AdvancedBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
                if (e.RowIndex < 1 || e.ColumnIndex < 0)
                {
                    return;
                }
                if (e.ColumnIndex == 7)
                {
                    if (isCellSameValue(e.ColumnIndex, e.RowIndex))
                    {
                        e.AdvancedBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
                    }
                    else
                    {
                        e.AdvancedBorderStyle.Top = dataGridView1.AdvancedCellBorderStyle.Top;
                        // dataGridView1.Rows[e.RowIndex-1].DividerHeight = 2;
                    }
                }
               
                else if (isCellSameValue(e.ColumnIndex, e.RowIndex) && e.ColumnIndex !=7)
                {
                    e.AdvancedBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
                   
                }
                
                else
                {
                    e.AdvancedBorderStyle.Top = dataGridView1.AdvancedCellBorderStyle.Top;
                   // dataGridView1.Rows[e.RowIndex-1].DividerHeight = 2;
                }
            }
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex != row_index)
            {
                settargetvol = false;
                row_index = e.RowIndex;
            }
            if (select_data_source == "mapping" && (e.ColumnIndex == 6 || e.ColumnIndex == 2 || e.ColumnIndex == 3 || e.ColumnIndex == 7 ))
            {
                if (e.RowIndex == 0)
                {
                    
                    if ((e.ColumnIndex == 2 || e.ColumnIndex == 3) && e.Value!=null)
                    {
                        String s = e.Value.ToString();
                        string[] tmp = s.Trim().Split('\n');
                        e.Value = "";
                        foreach (string t in tmp)
                        {
                            e.Value = e.Value + t + "    ";
                        }
                        return;
                    }
                    return;
                }
                if (isCellSameValue(e.ColumnIndex, e.RowIndex) && e.ColumnIndex != 2 && e.ColumnIndex !=7)
                {
                    
                   // e.Value = "";
                    e.FormattingApplied = true;
                    settargetvol = true;
                }
                else
                {
                   
                    
                    if ((e.ColumnIndex == 2 || e.ColumnIndex ==3) && e.Value != null)
                    {
                        String s = e.Value.ToString();
                        string[] tmp = s.Trim().Split('\n');
                        e.Value = "";
                        foreach (string t in tmp)
                        {
                            e.Value = e.Value + t + "    ";
                        }
                        return;
                    }
                }
            }
            if (select_data_source == "host")
            {
                if ((e.ColumnIndex == 2 || e.ColumnIndex == 3) && e.Value != null)
                {
                    String s = e.Value.ToString();
                    string[] tmp = s.Trim().Split('\n');
                    e.Value = "";
                    foreach (string t in tmp)
                    {
                        e.Value = e.Value + t + "    ";
                    }
                    return;
                }
                return;

            }
        }

        private void generate_script_Click(object sender, EventArgs e)
        {
            Xmcli_Script_Generator form1 = new Xmcli_Script_Generator(m_dbConnection,this,dataGridView1);
            form1.ShowDialog();
        }

        private void from_to_btn_Click(object sender, EventArgs e)
        {
            bool generateSuccess =  identify_cluster();
            if (generateSuccess)
            {
               generateSuccess = generate_from_to();
            }
            if (generateSuccess)
            {
                dataGridView1.Columns["target_device"].Visible = true;
                generate_script_btn.Visible = true;
            }
            else
            {
                generate_script_btn.Visible = false;
            }
        }   

        private bool generate_from_to()
        {
            string clustername = dataGridView1[0, 0].Value.ToString();
         
            int j = -1;
            try
            {
                for (int i = 0; i < dataGridView1.RowCount - 1; i++)
                {
                    if ((i + 1 < dataGridView1.RowCount - 1) && (dataGridView1[6, i].Value.ToString() == dataGridView1[6, i + 1].Value.ToString()) && (dataGridView1[5, i].Value.ToString() == dataGridView1[5, i + 1].Value.ToString()))
                    {
                        clustername = clustername + "|" + dataGridView1[0, i + 1].Value.ToString();
                        
                        continue;
                    }
                    else
                    {
                        string[] temp = clustername.Split('|');
                        if (temp.Length > 1)
                        {
                            string user_cluster_name = cluster_name_mapping[clustername];
                            //MessageBox.Show("Cluster found");
                            if (j == -1)
                            {
                                mapping_table.Rows[j+1][8] = user_cluster_name + "_Vol" + cluster_vol_index[clustername];
                                vol_name_tagging.Add(user_cluster_name.Trim() + "_Vol" + cluster_vol_index[clustername].ToString().Trim(), clustername);
                                cluster_vol_index[clustername] = cluster_vol_index[clustername] + 1;
                                
                            }
                            else
                            {
                                mapping_table.Rows[j + 1][8] = user_cluster_name + "_Vol" + cluster_vol_index[clustername];
                                vol_name_tagging.Add(user_cluster_name.Trim() + "_Vol" + cluster_vol_index[clustername].ToString().Trim(), clustername);
                                cluster_vol_index[clustername] = cluster_vol_index[clustername] + 1;
                                
                            }
                            j = i;
                            if (i + 1 < dataGridView1.Rows.Count - 1)
                            {
                                clustername = dataGridView1[0, i + 1].Value.ToString();
                            }
                        }
                        else
                        {
                            if (j == -1)
                            {
                                mapping_table.Rows[j+1][8] = dataGridView1[0, i].Value.ToString() + "_Vol" + cluster_vol_index[clustername];
                                vol_name_tagging.Add(dataGridView1[0, i].Value.ToString().Trim() + "_Vol" + cluster_vol_index[clustername].ToString().Trim(), clustername);
                                cluster_vol_index[clustername] = cluster_vol_index[clustername] + 1;
                               
                            }
                            else
                            {

                                mapping_table.Rows[j + 1][8] = dataGridView1[0, i].Value.ToString() + "_Vol" + cluster_vol_index[clustername];
                                vol_name_tagging.Add(dataGridView1[0, i].Value.ToString().Trim() + "_Vol" + cluster_vol_index[clustername].ToString().Trim(), clustername);
                                cluster_vol_index[clustername] = cluster_vol_index[clustername] + 1;
                               
                            }
                            j = i;
                            if (i + 1 < dataGridView1.Rows.Count - 1)
                            {
                                clustername = dataGridView1[0, i + 1].Value.ToString();
                            }
                            
                        }
                    }
                }
            }
                
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace );
                return false;
            }
            dataGridView1.DataSource = mapping_table;
            return true;
        }

        private bool identify_cluster()
        {
            cluster_table.Clear();
            clusterlist.Clear();
            int j = 0, clustercount = 0 ;
            cluster_vol_index.Clear();

            string sql = @"select array_device,MySelect.server_name as server_name,MySelect.os_name as os_name,MySelect.wwpn as wwpn,MySelect.ip_addr as ip_addr,disk_size,array_serial 
            from (select * from san_" + san_id + "_Host where server_name!='') as MySelect join san_" + san_id + "_Lun where san_" + san_id + "_lun.server_name = MySelect.server_name and array_device!=' ' and disk_size!='2880'group by myselect.server_name order by array_device,server_name";
            SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);

            da.Fill(cluster_table);
            clusterlist.Add(0,cluster_table.Rows[0][1].ToString());
            for (int i = 0; i < cluster_table.Rows.Count -1; i++)
            {
                if (cluster_table.Rows[i][0].ToString() == cluster_table.Rows[i + 1][0].ToString() && (cluster_table.Rows[i][6].ToString() == cluster_table.Rows[i + 1][6].ToString()))
                {
                    clusterlist[j] = clusterlist[j] + "|" + cluster_table.Rows[i + 1][1].ToString();
                    continue;
                }
                else
                {
                    j++;
                    clusterlist[j] =  cluster_table.Rows[i + 1][1].ToString();
                }
            }

           for(int i=0;i<clusterlist.Count;i++)
           {
               string[] temp = clusterlist[i].Split('|');
                if (temp.Length > 1)
                {
                    clustercount++;
                }
                else
                {
                    cluster_vol_index.Add(clusterlist[i], 0);
                    //clusterlist.Remove(i);   
                }

           }

           if (clustercount > 0)
           {
               MessageBox.Show("There are " + clustercount + " Clusters in the SAN Summary ", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);

               cluster_name_collect form1 = new cluster_name_collect(clusterlist, this);
               form1.ShowDialog();
               try
               {
                   foreach (string s in cluster_name_mapping.Keys)
                   {
                       cluster_vol_index.Add(s, 0);
                   }
               }
               catch (NullReferenceException ex)
               {
                   MessageBox.Show("Not all data was provided to generate From to List. If issues persists please contact Administrator " + ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                   return false;
               }
               return true;
           }
           else
           {
               return true;
           }
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }
    }
}
