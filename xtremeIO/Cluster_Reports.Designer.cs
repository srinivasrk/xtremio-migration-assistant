﻿namespace xtremeIO
{
    partial class Cluster_Reports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.xms_ip_lbl = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.xms_os_lbl = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.snapshot_radio_btn = new System.Windows.Forms.RadioButton();
            this.vol_radio_btn = new System.Windows.Forms.RadioButton();
            this.advance_btn = new System.Windows.Forms.Button();
            this.go_btn = new System.Windows.Forms.Button();
            this.filter_data = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.graph_data = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.graph_data)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.pictureBox2);
            this.groupBox3.Controls.Add(this.xms_ip_lbl);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.pictureBox1);
            this.groupBox3.Controls.Add(this.xms_os_lbl);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1008, 105);
            this.groupBox3.TabIndex = 28;
            this.groupBox3.TabStop = false;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            this.label5.Font = new System.Drawing.Font("Perpetua Titling MT", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(400, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(330, 24);
            this.label5.TabIndex = 21;
            this.label5.Text = "XtremIO Visual Analyzer ";
            // 
            // xms_ip_lbl
            // 
            this.xms_ip_lbl.AutoSize = true;
            this.xms_ip_lbl.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xms_ip_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xms_ip_lbl.ForeColor = System.Drawing.Color.Green;
            this.xms_ip_lbl.Location = new System.Drawing.Point(156, 53);
            this.xms_ip_lbl.Name = "xms_ip_lbl";
            this.xms_ip_lbl.Size = new System.Drawing.Size(144, 18);
            this.xms_ip_lbl.TabIndex = 10;
            this.xms_ip_lbl.Text = "XMS IP address : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.label2.Location = new System.Drawing.Point(6, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Visual Analyzer";
            // 
            // xms_os_lbl
            // 
            this.xms_os_lbl.AutoSize = true;
            this.xms_os_lbl.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xms_os_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xms_os_lbl.ForeColor = System.Drawing.Color.Green;
            this.xms_os_lbl.Location = new System.Drawing.Point(155, 16);
            this.xms_os_lbl.Name = "xms_os_lbl";
            this.xms_os_lbl.Size = new System.Drawing.Size(148, 18);
            this.xms_os_lbl.TabIndex = 11;
            this.xms_os_lbl.Text = "XMS OS version : ";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Image = global::xtremeIO.Properties.Resources.emcLogo;
            this.pictureBox2.Location = new System.Drawing.Point(878, 9);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Padding = new System.Windows.Forms.Padding(5);
            this.pictureBox2.Size = new System.Drawing.Size(127, 49);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 19;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::xtremeIO.Properties.Resources.logo1;
            this.pictureBox1.Location = new System.Drawing.Point(6, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Padding = new System.Windows.Forms.Padding(5);
            this.pictureBox1.Size = new System.Drawing.Size(127, 83);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.advance_btn);
            this.groupBox1.Controls.Add(this.go_btn);
            this.groupBox1.Controls.Add(this.filter_data);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 105);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1008, 85);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.snapshot_radio_btn);
            this.panel1.Controls.Add(this.vol_radio_btn);
            this.panel1.Location = new System.Drawing.Point(564, 20);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(270, 50);
            this.panel1.TabIndex = 24;
            this.panel1.Visible = false;
            // 
            // snapshot_radio_btn
            // 
            this.snapshot_radio_btn.AutoSize = true;
            this.snapshot_radio_btn.Location = new System.Drawing.Point(15, 30);
            this.snapshot_radio_btn.Name = "snapshot_radio_btn";
            this.snapshot_radio_btn.Size = new System.Drawing.Size(155, 17);
            this.snapshot_radio_btn.TabIndex = 1;
            this.snapshot_radio_btn.TabStop = true;
            this.snapshot_radio_btn.Text = "Show Snapshot details only";
            this.snapshot_radio_btn.UseVisualStyleBackColor = true;
            this.snapshot_radio_btn.CheckedChanged += new System.EventHandler(this.snapshot_radio_btn_CheckedChanged);
            // 
            // vol_radio_btn
            // 
            this.vol_radio_btn.AutoSize = true;
            this.vol_radio_btn.Location = new System.Drawing.Point(15, 7);
            this.vol_radio_btn.Name = "vol_radio_btn";
            this.vol_radio_btn.Size = new System.Drawing.Size(145, 17);
            this.vol_radio_btn.TabIndex = 0;
            this.vol_radio_btn.TabStop = true;
            this.vol_radio_btn.Text = "Show Volume details only";
            this.vol_radio_btn.UseVisualStyleBackColor = true;
            // 
            // advance_btn
            // 
            this.advance_btn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.advance_btn.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.advance_btn.Location = new System.Drawing.Point(439, 19);
            this.advance_btn.Name = "advance_btn";
            this.advance_btn.Size = new System.Drawing.Size(119, 51);
            this.advance_btn.TabIndex = 23;
            this.advance_btn.Text = "Show Advanced options";
            this.advance_btn.UseVisualStyleBackColor = true;
            this.advance_btn.Click += new System.EventHandler(this.advance_btn_Click);
            // 
            // go_btn
            // 
            this.go_btn.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.go_btn.Location = new System.Drawing.Point(379, 33);
            this.go_btn.Name = "go_btn";
            this.go_btn.Size = new System.Drawing.Size(43, 27);
            this.go_btn.TabIndex = 22;
            this.go_btn.Text = "Go";
            this.go_btn.UseVisualStyleBackColor = true;
            // 
            // filter_data
            // 
            this.filter_data.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.filter_data.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filter_data.FormattingEnabled = true;
            this.filter_data.Items.AddRange(new object[] {
            "Total Cluster Reads ",
            "Total Cluster Writes",
            "Logical Space Used (percent)",
            "Physical Space Used (percent)",
            "Free Logical Space in GB",
            "Free Physical Space in GB",
            "Number Of Volumes",
            ""});
            this.filter_data.Location = new System.Drawing.Point(75, 35);
            this.filter_data.Name = "filter_data";
            this.filter_data.Size = new System.Drawing.Size(287, 24);
            this.filter_data.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(12, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 17);
            this.label3.TabIndex = 18;
            this.label3.Text = "Select :";
            // 
            // graph_data
            // 
            chartArea3.AxisX.Interval = 1D;
            chartArea3.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea3.AxisX.IsLabelAutoFit = false;
            chartArea3.AxisX.LabelAutoFitMaxFontSize = 6;
            chartArea3.AxisX.LabelStyle.Angle = 30;
            chartArea3.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea3.AxisX.MaximumAutoSize = 100F;
            chartArea3.Name = "ChartArea1";
            this.graph_data.ChartAreas.Add(chartArea3);
            this.graph_data.Dock = System.Windows.Forms.DockStyle.Fill;
            legend3.Name = "Legend1";
            this.graph_data.Legends.Add(legend3);
            this.graph_data.Location = new System.Drawing.Point(0, 190);
            this.graph_data.Name = "graph_data";
            this.graph_data.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Excel;
            series3.ChartArea = "ChartArea1";
            series3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series3.LabelForeColor = System.Drawing.Color.DarkRed;
            series3.Legend = "Legend1";
            series3.Name = "Volume Size (GB)";
            this.graph_data.Series.Add(series3);
            this.graph_data.Size = new System.Drawing.Size(1008, 540);
            this.graph_data.TabIndex = 31;
            this.graph_data.Text = "chart1";
            // 
            // Cluster_Reports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.graph_data);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Name = "Cluster_Reports";
            this.Text = "Cluster_Reports";
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.graph_data)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label xms_ip_lbl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label xms_os_lbl;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton snapshot_radio_btn;
        private System.Windows.Forms.RadioButton vol_radio_btn;
        private System.Windows.Forms.Button advance_btn;
        private System.Windows.Forms.Button go_btn;
        private System.Windows.Forms.ComboBox filter_data;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataVisualization.Charting.Chart graph_data;
    }
}