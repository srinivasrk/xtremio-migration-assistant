﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace xtremeIO
{
    public partial class cluster_name_collect : Form
    {
        public Dictionary<int, String> clusterlist = new Dictionary<int, string>();
        List<int> clusIndex = new List<int>();
        public Dictionary<String, String> cluster_name_mapping = new Dictionary<string, string>();
        private int error = 0;
        Script_Generator_landingpage parentForm;
        public cluster_name_collect(Dictionary<int,String> params1,Script_Generator_landingpage params2)
        {
            InitializeComponent();
            clusterlist = params1;
            parentForm = params2;
        }

        private void cluster_name_collect_Load(object sender, EventArgs e)
        {
            ok_btn.TabIndex = 0;
            int j = 0;
            for (int i = 0; i < clusterlist.Count; i++)
            {
                if (clusterlist[i].Split('|').Length > 1)
                {

                    try
                    {
                        TextBox label = new TextBox();
                        label.Font = new Font("Times New Roman", 12, FontStyle.Bold);
                        label.BackColor = Color.White;
                        label.BorderStyle = BorderStyle.None;
                        label.Text = String.Format(clusterlist[i]);
                        Size size = TextRenderer.MeasureText(label.Text, label.Font);
                        label.ReadOnly = true;
                        //Position label on screen
                        label.Left = 10;
                        if (j != 0)
                        {
                            label.Top = (j + 1) * 100;
                        }
                        else
                        {
                            label.Top = 100;
                        }
                        label.Width = size.Width;
                        label.Height = size.Height;
                        label.Margin = new Padding(5);
                        //Create textbox
                        TextBox textBox = new TextBox();
                        //Position textbox on screen
                        textBox.Left = 10;
                        textBox.Margin = new Padding(5);
                        if (j != 0)
                        {
                            textBox.Top = ((j + 1) * 100) + 50;

                        }
                        else
                        {
                            textBox.Top = 150;
                        }
                        //Add controls to form
                        splitContainer1.Panel1.Controls.Add(label);
                        splitContainer1.Panel1.Controls.Add(textBox);
                       clusIndex.Add(i);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    j++;
                }
                else
                {
                   
                }
            }
        }

        private void ok_btn_Click(object sender, EventArgs e)
        {
            error = 0;
            int i = 0;
            foreach (var c in splitContainer1.Panel1.Controls)
            {
                if (c is TextBox)
                {
                    var textbox = (TextBox)c;
                    if (String.IsNullOrEmpty(textbox.Text))
                    {
                        error = 1;
                        MessageBox.Show("Please enter all the cluster details", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }

                }
            }
            if (error != 1)
            {
                foreach (var c in splitContainer1.Panel1.Controls)
                {
                    try
                    {
                        if (c is TextBox)
                        {
                            var textbox = (TextBox)c;
                            if (!String.IsNullOrEmpty(textbox.Text) && textbox.ReadOnly == false)
                            {
                                int j = clusIndex[i];
                                cluster_name_mapping.Add(clusterlist[j], textbox.Text);
                                i++;
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                 parentForm.cluster_name_mapping = this.cluster_name_mapping;
                 this.Close();
            }
           
        }

        private void cancel_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
