﻿namespace xtremeIO
{
    partial class san_upload
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(san_upload));
            this.upload_san_btn = new System.Windows.Forms.Button();
            this.san_sheet_upload_txt = new System.Windows.Forms.TextBox();
            this.cluster1_lbl = new System.Windows.Forms.Label();
            this.san_id_text = new System.Windows.Forms.TextBox();
            this.proj_id_lbl = new System.Windows.Forms.Label();
            this.cancel_btn = new System.Windows.Forms.Button();
            this.ok_btn = new System.Windows.Forms.Button();
            this.loading_lbl = new System.Windows.Forms.Label();
            this.percent_completed = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // upload_san_btn
            // 
            this.upload_san_btn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.upload_san_btn.Location = new System.Drawing.Point(531, 68);
            this.upload_san_btn.Name = "upload_san_btn";
            this.upload_san_btn.Size = new System.Drawing.Size(37, 24);
            this.upload_san_btn.TabIndex = 49;
            this.upload_san_btn.Text = "...";
            this.upload_san_btn.UseVisualStyleBackColor = true;
            this.upload_san_btn.Click += new System.EventHandler(this.upload_san_btn_Click);
            // 
            // san_sheet_upload_txt
            // 
            this.san_sheet_upload_txt.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.san_sheet_upload_txt.BackColor = System.Drawing.SystemColors.Window;
            this.san_sheet_upload_txt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.san_sheet_upload_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.san_sheet_upload_txt.Location = new System.Drawing.Point(319, 68);
            this.san_sheet_upload_txt.Name = "san_sheet_upload_txt";
            this.san_sheet_upload_txt.ReadOnly = true;
            this.san_sheet_upload_txt.Size = new System.Drawing.Size(206, 24);
            this.san_sheet_upload_txt.TabIndex = 52;
            // 
            // cluster1_lbl
            // 
            this.cluster1_lbl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cluster1_lbl.AutoSize = true;
            this.cluster1_lbl.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.cluster1_lbl.ForeColor = System.Drawing.Color.DarkCyan;
            this.cluster1_lbl.Location = new System.Drawing.Point(120, 68);
            this.cluster1_lbl.Name = "cluster1_lbl";
            this.cluster1_lbl.Size = new System.Drawing.Size(184, 22);
            this.cluster1_lbl.TabIndex = 51;
            this.cluster1_lbl.Text = "San Summary Sheet :";
            // 
            // san_id_text
            // 
            this.san_id_text.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.san_id_text.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.san_id_text.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.san_id_text.Location = new System.Drawing.Point(319, 9);
            this.san_id_text.Name = "san_id_text";
            this.san_id_text.Size = new System.Drawing.Size(206, 25);
            this.san_id_text.TabIndex = 48;
            // 
            // proj_id_lbl
            // 
            this.proj_id_lbl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.proj_id_lbl.AutoSize = true;
            this.proj_id_lbl.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.proj_id_lbl.ForeColor = System.Drawing.Color.DarkCyan;
            this.proj_id_lbl.Location = new System.Drawing.Point(95, 9);
            this.proj_id_lbl.Name = "proj_id_lbl";
            this.proj_id_lbl.Size = new System.Drawing.Size(209, 22);
            this.proj_id_lbl.TabIndex = 50;
            this.proj_id_lbl.Text = "Enter a unique SAN ID :";
            // 
            // cancel_btn
            // 
            this.cancel_btn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.cancel_btn.BackColor = System.Drawing.SystemColors.Window;
            this.cancel_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cancel_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.cancel_btn.ForeColor = System.Drawing.Color.Red;
            this.cancel_btn.Location = new System.Drawing.Point(347, 152);
            this.cancel_btn.Name = "cancel_btn";
            this.cancel_btn.Size = new System.Drawing.Size(88, 43);
            this.cancel_btn.TabIndex = 55;
            this.cancel_btn.Text = "Cancel";
            this.cancel_btn.UseVisualStyleBackColor = false;
            this.cancel_btn.Click += new System.EventHandler(this.cancel_btn_Click);
            // 
            // ok_btn
            // 
            this.ok_btn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ok_btn.BackColor = System.Drawing.SystemColors.Window;
            this.ok_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ok_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.ok_btn.ForeColor = System.Drawing.Color.DarkGreen;
            this.ok_btn.Location = new System.Drawing.Point(251, 152);
            this.ok_btn.Name = "ok_btn";
            this.ok_btn.Size = new System.Drawing.Size(83, 43);
            this.ok_btn.TabIndex = 54;
            this.ok_btn.Text = "OK";
            this.ok_btn.UseVisualStyleBackColor = false;
            this.ok_btn.Click += new System.EventHandler(this.ok_btn_Click);
            // 
            // loading_lbl
            // 
            this.loading_lbl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.loading_lbl.AutoSize = true;
            this.loading_lbl.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loading_lbl.ForeColor = System.Drawing.Color.Red;
            this.loading_lbl.Location = new System.Drawing.Point(464, 168);
            this.loading_lbl.Name = "loading_lbl";
            this.loading_lbl.Size = new System.Drawing.Size(0, 15);
            this.loading_lbl.TabIndex = 56;
            // 
            // percent_completed
            // 
            this.percent_completed.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.percent_completed.Location = new System.Drawing.Point(0, 235);
            this.percent_completed.Name = "percent_completed";
            this.percent_completed.Size = new System.Drawing.Size(727, 23);
            this.percent_completed.TabIndex = 57;
            // 
            // san_upload
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(727, 258);
            this.ControlBox = false;
            this.Controls.Add(this.percent_completed);
            this.Controls.Add(this.loading_lbl);
            this.Controls.Add(this.cancel_btn);
            this.Controls.Add(this.ok_btn);
            this.Controls.Add(this.upload_san_btn);
            this.Controls.Add(this.san_sheet_upload_txt);
            this.Controls.Add(this.cluster1_lbl);
            this.Controls.Add(this.san_id_text);
            this.Controls.Add(this.proj_id_lbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "san_upload";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SAN Summary";
            this.Load += new System.EventHandler(this.san_upload_search_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button upload_san_btn;
        private System.Windows.Forms.TextBox san_sheet_upload_txt;
        private System.Windows.Forms.Label cluster1_lbl;
        private System.Windows.Forms.TextBox san_id_text;
        private System.Windows.Forms.Label proj_id_lbl;
        private System.Windows.Forms.Button cancel_btn;
        private System.Windows.Forms.Button ok_btn;
        private System.Windows.Forms.Label loading_lbl;
        private System.Windows.Forms.ProgressBar percent_completed;
    }
}