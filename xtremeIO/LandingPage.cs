﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SQLite;
using Microsoft.Office.Interop.Excel;
using Outlook = Microsoft.Office.Interop.Outlook;
namespace xtremeIO
{
    public partial class LandingPage : Form
    {
        #region Global Variable
        SQLiteConnection m_dbConnection;
        Microsoft.Office.Interop.Excel.Application excel;
        Microsoft.Office.Interop.Excel.Workbook worKbooK;
      
        Microsoft.Office.Interop.Excel.Range celLrangE;
        private System.Data.DataTable data;
        public LandingPage(SQLiteConnection connection ,string s1)
        {
            InitializeComponent();
            m_dbConnection = connection;
         
        }
        #endregion
        #region Form Load
        private void Form1_Load(object sender, EventArgs e)
        {

            try
            {
                xms_os_lbl.Text = "XMS OS version : ";
                xms_ip_lbl.Text = "XMS IP address : ";
                //MessageBox.Show(logPath);
                string sql = "select * from xmsinfo";
                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                SQLiteDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    xms_ip_lbl.Text = xms_ip_lbl.Text + dr[1].ToString();
                    xms_os_lbl.Text = xms_os_lbl.Text + dr[3].ToString();
                }

                xms_ip_lbl.Visible = true;
                xms_os_lbl.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
        #region Volume Details

        private void volume_btn_Click(object sender, EventArgs e)
        {
           
                Volume_Details newForm = new Volume_Details(m_dbConnection, xms_ip_lbl.Text, xms_os_lbl.Text, this);
                newForm.ShowDialog();
           
        }
        #endregion
        #region Cluster Details
        private void cluster_btn_Click(object sender, EventArgs e)
        {

            Cluster_Details newForm = new Cluster_Details(m_dbConnection, xms_ip_lbl.Text, xms_os_lbl.Text, this);
            newForm.ShowDialog();
           
        }
        #endregion
        #region Initiator Details
        private void initiator_btn_Click(object sender, EventArgs e)
        {

            Initiator_Details newForm = new Initiator_Details(m_dbConnection, xms_ip_lbl.Text, xms_os_lbl.Text, this);
            newForm.ShowDialog();
           
        }
        #endregion
        #region Alert Details
        private void alert_btn_Click(object sender, EventArgs e)
        {

            Alerts_Warnings newForm = new Alerts_Warnings(m_dbConnection, xms_ip_lbl.Text, xms_os_lbl.Text, this);
            newForm.ShowDialog();
        }
        #endregion
        #region CG details
        private void consistency_btn_Click(object sender, EventArgs e)
        {

            Consistency_Group newForm = new Consistency_Group(m_dbConnection, xms_ip_lbl.Text, xms_os_lbl.Text, this);
            newForm.ShowDialog();
        }
        #endregion
        #region Harfware Details
        private void hardware_btn_Click(object sender, EventArgs e)
        {

            Hardware_Details newForm = new Hardware_Details(m_dbConnection, xms_ip_lbl.Text, xms_os_lbl.Text, this);
            newForm.ShowDialog();
            
          
        }
        #endregion
        #region XMS Info Page
        private void xMSInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
          
                Xms_Information newForm = new Xms_Information(m_dbConnection, xms_ip_lbl.Text, xms_os_lbl.Text, this);
                newForm.ShowDialog();

        }
        #endregion
        #region Generate Excel Data
        private void generateexceltoolstrip_Click(object sender, EventArgs e)
        {
           
            percent_completed.Visible = true;
            try
            {
                BackgroundWorker bw = new BackgroundWorker();
                bw.WorkerReportsProgress = true;

                //BG TASK
                

                    saveFileDialog1.Filter = "All files (*.*)|*.*" + "Excel files *.xls";
                    saveFileDialog1.FilterIndex = 1;
                    excel = new Microsoft.Office.Interop.Excel.Application();

                    if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK && saveFileDialog1.FileName.Length > 0)
                    {
                        MessageBox.Show("The Excel report generation will take 2 to 5 minutes", "Informations", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        string sql;
                        excel.Visible = false;
                        excel.DisplayAlerts = false;
                        bw.DoWork += new DoWorkEventHandler(
                            delegate(object o, DoWorkEventArgs args)
                            {
                                BackgroundWorker b = o as BackgroundWorker;
                                worKbooK = excel.Workbooks.Add(Type.Missing);
                                //for (int i = 1; i <= 2; i++)
                                //    ((Microsoft.Office.Interop.Excel.Worksheet)excel.ActiveWorkbook.Sheets[i]).Delete();


                              
                                b.ReportProgress(10);
                              

                                sql = "select * from alertdata";
                                data = getDatatable(sql);
                                data.Columns["alertcode"].ColumnName = "Alert Code";
                                data.Columns["alertstate"].ColumnName = "Alert State";
                                data.Columns["alerttype"].ColumnName = "Alert Type";
                                data.Columns["alertentitydata"].ColumnName = "Alert Entity Data";
                                data.Columns["alertdesc"].ColumnName = "Alert Description";
                                data.Columns["objectsev"].ColumnName = "Alert Severity";
                                data.Columns["raisetime"].ColumnName = "Raise Time";
                                data.Columns["alertclusterName"].ColumnName = "Cluster Name";
                                data.Columns["alertindex"].ColumnName = "Alert Index";
                                data.Columns["alertentityname"].ColumnName = "Entity Name";
                                b.ReportProgress(30);
                                Worksheet ws7 =
                                      (Microsoft.Office.Interop.Excel.Worksheet)worKbooK.Sheets.Add();
                                ws7 = (Microsoft.Office.Interop.Excel.Worksheet)worKbooK.ActiveSheet;


                                ws7.Name = "Alerts and Warnings";
                                Update_excel_data(data, ws7);
                                ws7.Cells[1, 1].EntireRow.Font.Bold = true;

                                sql = "select * from targetdata";
                                data = getDatatable(sql);
                                data.Columns["targetname"].ColumnName = "Target Name";
                                data.Columns["targetindex"].ColumnName = "Target Index";
                                data.Columns["targetjumboframes"].ColumnName = "Jumbo Frames (?)";
                                data.Columns["targetmtudata"].ColumnName = "Size of MTU";
                                data.Columns["targetportaddress"].ColumnName = "Target port address";
                                data.Columns["targetmacaddress"].ColumnName = "Target MAC address";
                                data.Columns["targetportstate"].ColumnName = "Port State";
                                data.Columns["targetportspeed"].ColumnName = "Port Speed";
                                data.Columns["targetporttype"].ColumnName = "Port Type";
                                data.Columns["targetsclusterName"].ColumnName = "Target Cluster Name";

                                Worksheet ws4 =
                                      (Microsoft.Office.Interop.Excel.Worksheet)worKbooK.Sheets.Add();
                                ws4 = (Microsoft.Office.Interop.Excel.Worksheet)worKbooK.ActiveSheet;
                                ws4.Name = "Target Details";
                                Update_excel_data(data, ws4);
                                ws4.Cells[1, 1].EntireRow.Font.Bold = true;
                                b.ReportProgress(50);

                                

                                sql = "select * from scDetails";
                                data = getDatatable(sql);
                                data.Columns["scname"].ColumnName = "Storage Contoller Name";
                                data.Columns["scindex"].ColumnName = "Stroage Controller Index";
                                data.Columns["clustername"].ColumnName = "Cluster Name";
                                data.Columns["ipA"].ColumnName = "SC IP A";
                                data.Columns["ipB"].ColumnName = "SC IP B";
                                data.Columns["mgmtaddr"].ColumnName = "Management IP Address";
                                data.Columns["gatewayaddr"].ColumnName = "Mgmt Gateway Address";
                                data.Columns["subnetaddr"].ColumnName = "Subnet Address";
                                Worksheet ws5 =
                                      (Microsoft.Office.Interop.Excel.Worksheet)worKbooK.Sheets.Add();
                                ws5 = (Microsoft.Office.Interop.Excel.Worksheet)worKbooK.ActiveSheet;
                                ws5.Name = "Storage Controller Details";
                                Update_excel_data(data, ws5);
                                ws5.Cells[1, 1].EntireRow.Font.Bold = true;
                              
                                sql = "select * from initiatordetails order by igname";
                                data = getDatatable(sql);
                                data.Columns["initiatorname"].ColumnName = "Initiator Name";
                                data.Columns["initiatorindex"].ColumnName = "Initiator Index";
                                data.Columns["clustername"].ColumnName = "Cluster Name";
                                data.Columns["porttype"].ColumnName = "Port Type";
                                data.Columns["portaddr"].ColumnName = "Port Address";
                                data.Columns["igname"].ColumnName = "Initiator Group Name";
                                data.Columns["initiatorOS"].ColumnName = "Initiator OS";
                               
                                b.ReportProgress(60);
                                data.Columns["Initiator Group Name"].SetOrdinal(2);
                                // Do SAME AS VOLUME

                                Worksheet ws6 =
                                      (Microsoft.Office.Interop.Excel.Worksheet)worKbooK.Sheets.Add();
                                ws6 = (Microsoft.Office.Interop.Excel.Worksheet)worKbooK.ActiveSheet;
                                ws6.Name = "Initiator Details";

                                if (data.Rows.Count == 1)
                                {
                                    Update_excel_data(data, ws6);
                                }
                                else
                                {
                                    //Manually add into excel
                                    int flag = 1, start = 0, rowcount = 2; ;
                                    for (int i = 0; i < data.Rows.Count; i++)
                                    {

                                        DataRow dr = data.Rows[i];
                                        if (i + 1 < data.Rows.Count)
                                        
                                        {
                                            DataRow dr1 = data.Rows[i + 1];
                                            if (dr1[2].ToString() == dr[2].ToString())
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                if (flag == 1)
                                                    flag = 0;
                                                else if (flag == 0)
                                                    flag = 1;

                                                //write everything till i to excel

                                                ws6.Cells.Font.Size = 15;

                                                for (int z = start; z <= i; z++)
                                                {
                                                    DataRow datarow = data.Rows[z];
                                                    rowcount += 1;
                                                    for (int k = 1; k <= data.Columns.Count; k++)
                                                    {

                                                        if (rowcount == 3 && start == 0)
                                                        {
                                                            ws6.Cells[2, k] = data.Columns[k - 1].ColumnName;
                                                            ws6.Cells.Font.Color = System.Drawing.Color.Black;

                                                        }

                                                        ws6.Cells[rowcount, k] = datarow[k - 1].ToString();
                                                        if (flag == 0)
                                                        {
                                                            ws6.Cells[rowcount, k].Interior.Color = XlRgbColor.rgbLightGray;
                                                        }


                                                    }

                                                }

                                                celLrangE = ws6.Range[ws6.Cells[1, 1], ws6.Cells[rowcount, data.Columns.Count]];
                                                celLrangE.EntireColumn.AutoFit();
                                                ws6.Cells[2, 1].EntireRow.Font.Bold = true;



                                                Microsoft.Office.Interop.Excel.Borders border = celLrangE.Borders;
                                                border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                                                border.Weight = 2d;
                                                start = i + 1;
                                            }
                                        }
                                    }
                                       for(int k=start;k<data.Rows.Count;k++)
                                        {
                                            rowcount++;
                                            DataRow datarow = data.Rows[k];
                                            for (int k1 = 1; k1 <= data.Columns.Count; k1++)
                                            {
                                               
                                                if (rowcount == 3 && start == 0)
                                                {
                                                    ws6.Cells[2, k1] = data.Columns[k1 - 1].ColumnName;
                                                    ws6.Cells.Font.Color = System.Drawing.Color.Black;

                                                }

                                                ws6.Cells[rowcount, k1] = datarow[k1 - 1].ToString();
                                               


                                            }
                                          
                                        }

                                    }


                                
                                ws6.Cells[1, 1].EntireRow.Font.Bold = true;

                               

                                sql = "select * from volumedetails left join lunmapping using (volumename) where volumename!=\"\" order by volumename";     
                                data = getDatatable(sql);
                                data.Columns.Remove("realvolsize");
                                data.Columns["volumename"].ColumnName = "Volume Name";
                                data.Columns["igname"].ColumnName = "Initiator Group Name";
                                data.Columns["volumeindex"].ColumnName = "Volume Index";
                                data.Columns["clustername"].ColumnName = "Cluster Name";
                                data.Columns["volsize"].ColumnName = "Volume Size";
                                data.Columns["lbsize"].ColumnName = "Lb Size";
                                data.Columns["physpaceused"].ColumnName = "Physical Space Used";
                                data.Columns["createdfrom"].ColumnName = "Created From Volume";
                                data.Columns["smallIO"].ColumnName = "Small IO Alerts";
                                data.Columns["unalignedIO"].ColumnName = "Unaligned IO Alerts";
                                data.Columns["VAAITP"].ColumnName = "VAAI TP Alerts";
                                data.Columns["naaID"].ColumnName = "NAA Id";
                                data.Columns["creationTime"].ColumnName = "Creation Time";
                                data.Columns["Initiator Group Name"].SetOrdinal(2);

                                Worksheet ws3 =
                                     (Microsoft.Office.Interop.Excel.Worksheet)worKbooK.Sheets.Add();
                                ws3 = (Microsoft.Office.Interop.Excel.Worksheet)worKbooK.ActiveSheet;
                                ws3.Name = "Volume Details";
                                if (data.Rows.Count == 1)
                                {
                                    Update_excel_data(data, ws3);
                                }
                                else
                                {
                                    //Manually add into excel
                                    int flag = 1, start = 0, rowcount = 2;
                                    for (int i = 0; i < data.Rows.Count; i++)
                                    {

                                        DataRow dr = data.Rows[i];
                                        if (i + 1 < data.Rows.Count)
                                        {
                                            DataRow dr1 = data.Rows[i + 1];
                                            if (dr1[2].ToString() == dr[2].ToString())
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                if (flag == 1)
                                                    flag = 0;
                                                else if (flag == 0)
                                                    flag = 1;

                                                //write everything till i to excel

                                                ws3.Cells.Font.Size = 15;

                                                for (int z = start; z <= i; z++)
                                                {
                                                    DataRow datarow = data.Rows[z];
                                                    rowcount += 1;
                                                    for (int k = 1; k <= data.Columns.Count; k++)
                                                    {

                                                        if (rowcount == 3 && start == 0)
                                                        {
                                                            ws3.Cells[2, k] = data.Columns[k - 1].ColumnName;
                                                            ws3.Cells.Font.Color = System.Drawing.Color.Black;

                                                        }

                                                        ws3.Cells[rowcount, k] = datarow[k - 1].ToString();
                                                        if (flag == 0)
                                                        {
                                                            ws3.Cells[rowcount, k].Interior.Color = XlRgbColor.rgbLightGray;
                                                        }


                                                    }

                                                }

                                                celLrangE = ws3.Range[ws3.Cells[1, 1], ws3.Cells[rowcount, data.Columns.Count]];
                                                celLrangE.EntireColumn.AutoFit();
                                                ws3.Cells[2, 1].EntireRow.Font.Bold = true;



                                                Microsoft.Office.Interop.Excel.Borders border = celLrangE.Borders;
                                                border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                                                border.Weight = 2d;
                                                start = i + 1;
                                            }
                                        }

                                       

                                    }

                                    for (int k = start; k < data.Rows.Count; k++)
                                    {
                                        rowcount++;
                                        DataRow datarow = data.Rows[k];
                                        for (int k1 = 1; k1 <= data.Columns.Count; k1++)
                                        {
                                           
                                            if (rowcount == 3 && start == 0)
                                            {
                                                ws3.Cells[2, k1] = data.Columns[k1 - 1].ColumnName;
                                                ws3.Cells.Font.Color = System.Drawing.Color.Black;

                                            }

                                            ws3.Cells[rowcount, k1] = datarow[k1 - 1].ToString();



                                        }
                                     
                                    }

                                }
                                ws3.Cells[1, 1].EntireRow.Font.Bold = true;

                              
                                b.ReportProgress(80);
                              
                               // Update_excel_data(data, ws3);
                               

                                sql = "select * from clusterdetails";
                                data = getDatatable(sql);
                                data.Columns["clustername"].ColumnName = "Cluster Name";
                                data.Columns["clusterindex"].ColumnName = "Cluster Index";
                                data.Columns["psnt"].ColumnName = "PSNT";
                                data.Columns["num_of_vol"].ColumnName = "Number of Volumes";
                                data.Columns["num_of_bricks"].ColumnName = "Number of Bricks Name";
                                data.Columns["size_capacity"].ColumnName = "Size and Capacity";
                                data.Columns["dedup_ratio"].ColumnName = "Deduplication Ratio";
                                data.Columns["free_phy_space"].ColumnName = "Free Physical Space";
                                data.Columns["thin_provisioning_savings"].ColumnName = "Thin Provisioning Savings";
                                data.Columns["free_logical_space"].ColumnName = "Free Logical Space";
                                data.Columns["sw_version"].ColumnName = "Software Version";
                                Worksheet ws2 =
                                      (Microsoft.Office.Interop.Excel.Worksheet)worKbooK.Sheets.Add();
                                ws2 = (Microsoft.Office.Interop.Excel.Worksheet)worKbooK.ActiveSheet;
                                ws2.Name = "Cluster Info";
                                Update_excel_data(data, ws2);
                                ws2.Cells[1, 1].EntireRow.Font.Bold = true;
                                b.ReportProgress(90);
                               

                                sql = "select * from xmsinfo";
                                data = getDatatable(sql);
                                data.Columns["xmsname"].ColumnName = "XMS Name";
                                data.Columns["xmsip"].ColumnName = "XMS IP Address";
                                data.Columns["xmssubnet"].ColumnName = "XMS Subnet Address";
                                data.Columns["xmsos"].ColumnName = "XMS OS";
                                data.Columns["xmsgateway"].ColumnName = "XMS Gateway Address";
                                data.Columns["dedupratio"].ColumnName = "Dedup Ratio";
                                data.Columns["compressionfactor"].ColumnName = "Compression Factor";
                                data.Columns["clustertime"].ColumnName = "Cluster Time";
                                //  data.Columns["compressionfactor"].ColumnName = "Compression Factor";
                                data.Columns["freelogicalspace"].ColumnName = "Free Logical Space";
                                data.Columns["freephyspace"].ColumnName = "Free Physical Space";
                                data.Columns["freelogicalpercent"].ColumnName = "Free Logical Percent";
                                data.Columns["freephypercent"].ColumnName = "Free Physical Percent";
                                data.Columns["ntpservers"].ColumnName = "NTP servers";
                                data.Columns["numofig"].ColumnName = "Number of Initiator Groups";
                                data.Columns["numofcluster"].ColumnName = "Number of Clusters";
                                data.Columns["numofvol"].ColumnName = "Number of Volumes";
                                data.Columns["overallefficiency"].ColumnName = "Overall Efficiency";
                                data.Columns["thinprovsaving"].ColumnName = "Thin Provisioning Savings";
                                Worksheet worksheet =
                                      (Microsoft.Office.Interop.Excel.Worksheet)worKbooK.Sheets.Add();
                                worksheet = (Microsoft.Office.Interop.Excel.Worksheet)worKbooK.ActiveSheet;
                                worksheet.Name = "XMS Info";
                                Update_excel_data(data, worksheet);
                                worksheet.Cells[1, 1].EntireRow.Font.Bold = true;
                               
                                

                                try
                                {
                                    
                                    worKbooK.SaveAs(saveFileDialog1.FileName);
                                    worKbooK.Close();
                                    excel.Quit();
                                    MessageBox.Show("Excel Report generated Successfully");
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message,"Error - CHECK IF FILE IS OPEN",MessageBoxButtons.OK,MessageBoxIcon.Error);
                                }
                            });

                       
                    }
                    else
                    {
                        MessageBox.Show("File Not Exported!!");
                    }
                
                bw.ProgressChanged += new ProgressChangedEventHandler(
                delegate(object o, ProgressChangedEventArgs args)
                {
                    percent_completed.Value = args.ProgressPercentage;
                });
               
                // what to do when worker completes its task (notify the user)
                bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(
                delegate(object o, RunWorkerCompletedEventArgs args)
                {
                    percent_completed.Value = 100;
                    percent_completed.Visible = false;
                    

                });

                bw.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

           
        }
        #endregion
        #region Get Data for Excel
        private System.Data.DataTable getDatatable(string sql)
        {
            try
            {
                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                excel = new Microsoft.Office.Interop.Excel.Application();
                System.Data.DataTable data = new System.Data.DataTable();
                da.Fill(data);
                return data;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
        #endregion
        #region Write Data to Excel
        private void Update_excel_data(System.Data.DataTable data,Worksheet ws)
        {
            try
            {
               
                ws.Cells.Font.Size = 15;
                int rowcount = 2,flag=0;
                foreach (DataRow datarow in data.Rows)
                {
                  
                    rowcount += 1;
                    for (int i = 1; i <= data.Columns.Count; i++)
                    {

                        if (rowcount == 3)
                        {
                            ws.Cells[2, i] = data.Columns[i - 1].ColumnName;
                            ws.Cells.Font.Color = System.Drawing.Color.Black;

                        }

                        ws.Cells[rowcount, i] = datarow[i - 1].ToString();
                        if (flag == 0)
                        {
                            ws.Cells[rowcount, i].Interior.Color = XlRgbColor.rgbAliceBlue;
                            
                        }
                       
                        if (rowcount > 3)
                        {
                            if (i == data.Columns.Count)
                            {
                                if (rowcount % 2 == 0)
                                {
                                    celLrangE = ws.Range[ws.Cells[rowcount, 1], ws.Cells[rowcount, data.Columns.Count]];
                                   
                                }

                            }
                        }

                    }
                    if (flag == 0)
                    {
                        flag = 1;
                    }
                    else
                    {
                        flag = 0;
                    }
                }
               
                celLrangE = ws.Range[ws.Cells[1, 1], ws.Cells[rowcount, data.Columns.Count]];
                celLrangE.EntireColumn.AutoFit();
                ws.Cells[2, 1].EntireRow.Font.Bold = true;
              
              //  FormatCondition format = ws.Rows.FormatConditions.Add(XlFormatConditionType.xlExpression, XlFormatConditionOperator.xlEqual, "=MOD(ROW(E2)| 2) = 0", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
               // format.Interior.Color = XlRgbColor.rgbAliceBlue;

                Microsoft.Office.Interop.Excel.Borders border = celLrangE.Borders;
                border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                border.Weight = 2d;

                //  celLrangE = worksheet.Range[worksheet.Cells[1, 1], worksheet.Cells[2, data.Columns.Count]];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
        #region VOlume Reports
        private void volume_reports_btn_Click(object sender, EventArgs e)
        {
            Volume_Reports newForm = new Volume_Reports(m_dbConnection, xms_ip_lbl.Text, xms_os_lbl.Text, this);
            newForm.ShowDialog();
        }
        #endregion
        #region Report Details
        private void show_reports_Click(object sender, EventArgs e)
        {
            volume_btn.Visible = false;
            cluster_btn.Visible = false;
            initiator_btn.Visible = false;
            hardware_btn.Visible = false;
            alert_btn.Visible = false;
            consistency_btn.Visible = false;
            volume_reports_btn.Visible = true;
            cluster_reports_btn.Visible = true;
            bw_reports_btn.Visible = true;
            show_reports_btn.Visible = false;
            back_btn.Visible = true;
            scripts_btn.Visible = false;
        }
        #endregion
        #region Bandwidth Reports
        private void bw_reports_btn_Click(object sender, EventArgs e)
        {
            Bw_IO_Reports newForm = new Bw_IO_Reports(m_dbConnection, xms_ip_lbl.Text, xms_os_lbl.Text, this);
            newForm.ShowDialog();
        }
        #endregion
        #region Back Button
        private void back_btn_Click(object sender, EventArgs e)
        {
            volume_btn.Visible = true;
            cluster_btn.Visible = true;
            initiator_btn.Visible = true;
            hardware_btn.Visible = true;
            alert_btn.Visible = true;
            consistency_btn.Visible = true;
            volume_reports_btn.Visible = false;
            cluster_reports_btn.Visible = false;
            bw_reports_btn.Visible = false;
            show_reports_btn.Visible = true;
            back_btn.Visible = false;
            scripts_btn.Visible = true;
            upload_san_btn.Visible = false;
            view_san_btn.Visible = false;
        }
        #endregion
        #region Cluster Reports
        private void cluster_reports_btn_Click(object sender, EventArgs e)
        {
            Cluster_Reports_1 newForm = new Cluster_Reports_1(m_dbConnection, xms_ip_lbl.Text, xms_os_lbl.Text, this);
            newForm.ShowDialog();
        }
        #endregion
        #region Extra Stuff( Menu bar and Closing)
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
           DialogResult res = MessageBox.Show("Do you really want to exit the application ?","Are you Sure ?",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
           if (res == DialogResult.Yes)
           {
               e.Cancel = false;
           }
           else
           {
               e.Cancel = true;
           }
        }

        private void reportABugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Outlook.Application outlookApp = new Outlook.Application();
            Outlook._MailItem oMailItem = (Outlook._MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
            Outlook.Inspector oInspector = oMailItem.GetInspector;
            Outlook.Recipients oRecips = (Outlook.Recipients)oMailItem.Recipients;
            List<string> lstAllRecipients = new List<string>();
            //Below is hardcoded - can be replaced with db data
            lstAllRecipients.Add("ps.xtremio.log.analyzer.dev@emc.com");
           
            foreach (String recipient in lstAllRecipients)
            {
                Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(recipient);
                oRecip.Resolve();
            }

            //Add CC
           // Outlook.Recipient oCCRecip = oRecips.Add("ps.xtremio.log.analyzer.dev@emc.com");
          //  oCCRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
          //  oCCRecip.Resolve();

            //Add Subject
            oMailItem.Subject = "XtremIO Log Analyzer : Bug Report ";

            // body, bcc etc...

            //Display the mailbox
            oMailItem.Display(true);
        }

        private void fAQToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://10.63.6.103/XIO/aboutus.html");
        }

        private void aboutUsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://10.63.6.103/XIO/aboutus.html");
        }

        private void quitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }
        #endregion

        private void scripts_btn_Click(object sender, EventArgs e)
        {
            volume_btn.Visible = false;
            cluster_btn.Visible = false;
            initiator_btn.Visible = false;
            hardware_btn.Visible = false;
            alert_btn.Visible = false;
            scripts_btn.Visible = false;
            show_reports_btn.Visible = false;
            consistency_btn.Visible = false;
            upload_san_btn.Visible = true;
            view_san_btn.Visible = true;
            back_btn.Visible = true;
        }

        private void upload_san_btn_Click(object sender, EventArgs e)
        {
            san_upload form1 = new san_upload(m_dbConnection,xms_ip_lbl.Text,xms_os_lbl.Text);
            form1.ShowDialog();
            
        }

        private void view_san_btn_Click(object sender, EventArgs e)
        {
            san_search form1 = new san_search(m_dbConnection,xms_ip_lbl.Text,xms_os_lbl.Text);
            form1.ShowDialog();
        }
    }


}

