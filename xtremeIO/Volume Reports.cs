﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;
namespace xtremeIO
{
    public partial class Volume_Reports : Form
    {
        #region Global Varaible Declaration
        SQLiteConnection m_dbConnection;
        LandingPage back_instance;
        public Volume_Reports(SQLiteConnection conn, string ip, string os, LandingPage back_data)
        {
            InitializeComponent();
            back_instance = back_data;
            m_dbConnection = conn;
            xms_ip_lbl.Text = ip;
            xms_os_lbl.Text = os;
        }
        #endregion
        #region Load Form
        private void Reports_Load(object sender, EventArgs e)
        {
            filter_data.SelectedIndex = 0;
            go_btn_Click(null, null);
        }
        #endregion
        #region Go Button Click
        private void go_btn_Click(object sender, EventArgs e)
        {

            foreach (var series in graph_data.Series)
            {
                series.Points.Clear();
            }
            switch (filter_data.SelectedIndex)
            {
                case 0:
                    {
                        int yaxis = 0;
                        double temp = 0;
                        if (panel1.Visible == true)
                        {
                            if (vol_radio_btn.Checked)
                            {
                                string sql = "select * from volumedetails where createdfrom = 'N/A' and volumename!='' order by realvolsize desc limit 10";
                                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                                DataTable dt = new DataTable();
                                da.Fill(dt);
                                foreach (DataRow dr in dt.Rows)
                                {
                                    temp = double.Parse(dr["realvolsize"].ToString());
                                    if (temp > 0)
                                    {
                                        yaxis = 1;
                                    }

                                    if (dr["volumename"].ToString().Length > 50)
                                    {
                                        dr["volumename"] = dr["volumename"].ToString().Substring(0, 50);
                                    }
                                    graph_data.Series[0].Points.AddXY(dr["volumename"].ToString(), dr["realvolsize"].ToString());
                                    if (yaxis == 0)
                                    {
                                        graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 1;
                                    }
                                    else
                                    {
                                        graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 0;
                                        graph_data.ChartAreas["ChartArea1"].AxisY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
                                    }
                                }
                            }
                            else if (snapshot_radio_btn.Checked)
                            {
                                string sql = "select * from volumedetails where createdfrom != 'N/A' and volumename!='' order by realvolsize desc limit 10";
                                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                                DataTable dt = new DataTable();
                                da.Fill(dt);
                                foreach (DataRow dr in dt.Rows)
                                {
                                    temp = double.Parse(dr["realvolsize"].ToString());
                                    if (temp > 0)
                                    {
                                        yaxis = 1;
                                    }
                                    if (dr["volumename"].ToString().Length > 50)
                                    {
                                        dr["volumename"] = dr["volumename"].ToString().Substring(0, 50);
                                    }
                                    graph_data.Series[0].Points.AddXY(dr["volumename"].ToString(), dr["realvolsize"].ToString());
                                    if (yaxis == 0)
                                    {
                                        graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 1;
                                    }
                                    else
                                    {
                                        graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 0;
                                        graph_data.ChartAreas["ChartArea1"].AxisY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
                                    }
                                }
                            }
                            else
                            {

                                string sql = "select * from volumedetails where volumename!='' order by realvolsize desc limit 10";
                                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                                DataTable dt = new DataTable();
                                da.Fill(dt);
                                foreach (DataRow dr in dt.Rows)
                                {
                                    temp = double.Parse(dr["realvolsize"].ToString());
                                    if (temp > 0)
                                    {
                                        yaxis = 1;
                                    }
                                    if (dr["volumename"].ToString().Length > 50)
                                    {
                                        dr["volumename"] = dr["volumename"].ToString().Substring(0, 50);
                                    }
                                    graph_data.Series[0].Points.AddXY(dr["volumename"].ToString(), dr["realvolsize"].ToString());
                                    if (yaxis == 0)
                                    {
                                        graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 1;
                                    }
                                    else
                                    {
                                        graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 0;
                                        graph_data.ChartAreas["ChartArea1"].AxisY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
                                    }
                                }
                            }

                        }
                        else
                        {
                            string sql = "select * from volumedetails where volumename!='' order by realvolsize desc limit 10";
                            SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                            DataTable dt = new DataTable();
                            da.Fill(dt);
                            foreach (DataRow dr in dt.Rows)
                            {
                                temp = double.Parse(dr["realvolsize"].ToString());
                                if (temp > 0)
                                {
                                    yaxis = 1;
                                }
                                if (dr["volumename"].ToString().Length > 50)
                                {
                                    dr["volumename"] = dr["volumename"].ToString().Substring(0, 50);
                                }
                                graph_data.Series[0].Points.AddXY(dr["volumename"].ToString(), dr["realvolsize"].ToString());
                                if (yaxis == 0)
                                {
                                    graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 1;
                                }
                                else
                                {
                                    graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 0;
                                    graph_data.ChartAreas["ChartArea1"].AxisY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
                                }
                            }

                        }
                        break;
                    }

                case 1:
                    {
                        int yaxis = 0;
                        double temp = 0;
                        if (panel1.Visible == true)
                        {
                            if (vol_radio_btn.Checked)
                            {
                                string sql = "select * from volumedetails where createdfrom = 'N/A' and volumename!='' order by realvolsize limit 10";
                                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                                DataTable dt = new DataTable();
                                da.Fill(dt);
                                foreach (DataRow dr in dt.Rows)
                                {
                                    temp = double.Parse(dr["realvolsize"].ToString());
                                    if (temp > 0)
                                    {
                                        yaxis = 1;
                                    }
                                    if (dr["volumename"].ToString().Length > 50)
                                    {
                                        dr["volumename"] = dr["volumename"].ToString().Substring(0, 50);
                                    }
                                    graph_data.Series[0].Points.AddXY(dr["volumename"].ToString(), dr["realvolsize"].ToString());
                                    if (yaxis == 0)
                                    {
                                        graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 1;
                                    }
                                    else
                                    {
                                        graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 0;
                                        graph_data.ChartAreas["ChartArea1"].AxisY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
                                    }
                                }
                            }
                            else if (snapshot_radio_btn.Checked)
                            {
                                string sql = "select * from volumedetails where createdfrom != 'N/A' and volumename!='' order by realvolsize limit 10";
                                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                                DataTable dt = new DataTable();
                                da.Fill(dt);
                                foreach (DataRow dr in dt.Rows)
                                {
                                    temp = double.Parse(dr["realvolsize"].ToString());
                                    if (temp > 0)
                                    {
                                        yaxis = 1;
                                    }
                                    if (dr["volumename"].ToString().Length > 50)
                                    {
                                        dr["volumename"] = dr["volumename"].ToString().Substring(0, 50);
                                    }
                                    graph_data.Series[0].Points.AddXY(dr["volumename"].ToString(), dr["realvolsize"].ToString());
                                    if (yaxis == 0)
                                    {
                                        graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 1;
                                    }
                                    else
                                    {
                                        graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 0;
                                        graph_data.ChartAreas["ChartArea1"].AxisY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
                                    }
                                }
                            }
                            else
                            {

                                string sql = "select * from volumedetails where volumename!=''order by realvolsize limit 10";
                                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                                DataTable dt = new DataTable();
                                da.Fill(dt);
                                foreach (DataRow dr in dt.Rows)
                                {
                                    temp = double.Parse(dr["realvolsize"].ToString());
                                    if (temp > 0)
                                    {
                                        yaxis = 1;
                                    }
                                    if (dr["volumename"].ToString().Length > 50)
                                    {
                                        dr["volumename"] = dr["volumename"].ToString().Substring(0, 50);
                                    }
                                    graph_data.Series[0].Points.AddXY(dr["volumename"].ToString(), dr["realvolsize"].ToString());
                                    if (yaxis == 0)
                                    {
                                        graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 1;
                                    }
                                    else
                                    {
                                        graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 0;
                                        graph_data.ChartAreas["ChartArea1"].AxisY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
                                    }
                                }
                            }

                        }
                        else
                        {
                            string sql = "select * from volumedetails where volumename!='' order by realvolsize limit 10";
                            SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                            DataTable dt = new DataTable();
                            da.Fill(dt);
                            foreach (DataRow dr in dt.Rows)
                            {
                                temp = double.Parse(dr["realvolsize"].ToString());
                                if (temp > 0)
                                {
                                    yaxis = 1;
                                }
                                if (dr["volumename"].ToString().Length > 50)
                                {
                                    dr["volumename"] = dr["volumename"].ToString().Substring(0, 50);
                                }
                                graph_data.Series[0].Points.AddXY(dr["volumename"].ToString(), dr["realvolsize"].ToString());
                                if (yaxis == 0)
                                {
                                    graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 1;
                                }
                                else
                                {
                                    graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 0;
                                    graph_data.ChartAreas["ChartArea1"].AxisY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
                                }
                            }

                        }
                        break;

                    }



            }
        }
        #endregion Go Button Click
        #region Advance Show

        private void advance_btn_Click_1(object sender, EventArgs e)
        {
            if (advance_btn.Text == "Show Advanced options")
            {
                panel1.Visible = true;
                advance_btn.Text = "Hide Advanced options";
            }
            else if (advance_btn.Text == "Hide Advanced options")
            {
                panel1.Visible = false;
                snapshot_radio_btn.Checked = false;
                vol_radio_btn.Checked = false;
                advance_btn.Text = "Show Advanced options";
            }
        }
        #endregion

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}

