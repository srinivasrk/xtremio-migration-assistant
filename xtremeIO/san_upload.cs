﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ExcelLibrary.Office.Excel;
using Microsoft.Office.Interop.Excel;
using System.Data.SQLite;
namespace xtremeIO
{
    public partial class san_upload : Form
    {
        private Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
        System.Data.DataTable host_details_dt = new System.Data.DataTable();
        System.Data.DataTable lun_details_dt = new System.Data.DataTable();
        BackgroundWorker bw;
        private String ip_addr, os_version;
        private string san_sheet;
        private SQLiteConnection m_dbConnection;
        private Microsoft.Office.Interop.Excel.Workbook excel_workbook;
        public san_upload(SQLiteConnection conn,string params1,string params2 )
        {
            InitializeComponent();
            m_dbConnection = conn;
            ip_addr = params1;
            os_version = params2;
        }

        private void cancel_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void upload_san_btn_Click(object sender, EventArgs e)
        {
            host_details_dt.Clear();
            OpenFileDialog of = new OpenFileDialog();
            of.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
            DialogResult result = of.ShowDialog();
            if (result == DialogResult.OK)
            {
                san_sheet = of.FileName;
                san_sheet_upload_txt.Text = san_sheet;

            }
        }

        private bool Host_analyze(string san_sheet)
        {
            excel_workbook = (Microsoft.Office.Interop.Excel.Workbook) excel.Workbooks.Open(san_sheet);
            Microsoft.Office.Interop.Excel.Worksheet xlWorksheet = (Microsoft.Office.Interop.Excel.Worksheet)excel_workbook.Worksheets[3];
            Range excelRange = xlWorksheet.UsedRange;
            int rowCount = excelRange.Rows.Count;
            int colCount = excelRange.Columns.Count;
            
           // MessageBox.Show(rowCount.ToString() + " : " + colCount.ToString());
            
            for (int i = 2; i <= rowCount; i++)
            {
                 DataRow oRow = host_details_dt.NewRow();
                 //loading_lbl.Text = "Loading Host Details ... Reading row " + i.ToString();
                for (int j = 1; j <= colCount; j++)
                {
                    if (j == 2 || j == 3  || j == 18 || j==23)
                    {
                        if (excelRange.Cells[i, j].Value2 != null)
                        {
                            //  MessageBox.Show(excelRange.Cells[i,j].Value2);
                            if (i == 2)
                            {
                              
                                    host_details_dt.Columns.Add(excelRange.Cells[i, j].Value2.ToString());
                              
                            }
                            try
                            {
                                if (i != 2)
                                {
                                    //Index did not work for some reason , so going with header names :(
                                    if (j == 3)
                                    {
                                        oRow[excelRange.Cells[2, j].Value2.ToString()] = excelRange.Cells[i, j].Value2.ToString() +" | "+excelRange.Cells[i,j+1].Value2.ToString();
                                    }
                                    else
                                    {
                                        oRow[excelRange.Cells[2, j].Value2.ToString()] = excelRange.Cells[i, j].Value2.ToString();
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message.ToString());
                            }
                        }
                    }
                   

                }
               host_details_dt.Rows.Add(oRow);
            }

            foreach (DataColumn dc in host_details_dt.Columns)
            {
                if (dc.ColumnName.ToLower().Contains("server name"))
                    return true;
            }
            return false;
            
        }

        private bool Lun_Analyze(string san_sheet)
        {
            Microsoft.Office.Interop.Excel.Worksheet xlWorksheet = (Microsoft.Office.Interop.Excel.Worksheet)excel_workbook.Worksheets[5];
            Range excelRange = xlWorksheet.UsedRange;
            int rowCount = excelRange.Rows.Count;
            int colCount = excelRange.Columns.Count;

           
            // MessageBox.Show(rowCount.ToString() + " : " + colCount.ToString());

            for (int i = 2; i <= rowCount; i++)
            {
                DataRow oRow = lun_details_dt.NewRow();
                //loading_lbl.Text = "Loading Lun Details ... Reading row " + i.ToString();
                for (int j = 1; j <= colCount; j++)
                {
                    if(j==2 || j==10 || j==11 || j==12 || j==13)
                    {
                        if (excelRange.Cells[i, j].Value2 != null)
                        {
                            //  MessageBox.Show(excelRange.Cells[i,j].Value2);
                            if (i == 2)
                            {
                                lun_details_dt.Columns.Add(excelRange.Cells[i, j].Value2.ToString());
                                //MessageBox.Show("Added col " + excelRange.Cells[i, j].Value2.ToString());
                            }
                            try
                            {
                                if (i != 2)
                                {
                                    //Index did not work for some reason , so going with header names :(

                                    oRow[excelRange.Cells[2, j].Value2.ToString()] = excelRange.Cells[i, j].Value2.ToString();
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message.ToString());
                            }
                        }
                    }
                }
                lun_details_dt.Rows.Add(oRow);
            }

            foreach (DataColumn dc in lun_details_dt.Columns)
            {
                if (dc.ColumnName.Trim().ToLower().Contains("array device"))
                    return true;
            }
            return false;
        }

        private void ok_btn_Click(object sender, EventArgs e)
        {
            DialogResult _ans = MessageBox.Show("The analysis can take 5 to 10 minutes do you want to continue ?", "Confirm?", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (_ans == DialogResult.OK)
            {
                bw = new BackgroundWorker();
                bw.WorkerReportsProgress = true;
                bw.WorkerSupportsCancellation = true;

                bw.DoWork += new DoWorkEventHandler(
                delegate(object o, DoWorkEventArgs args)
                {
                    BackgroundWorker b = o as BackgroundWorker;
                    if (san_id_text.Text != "" && san_sheet_upload_txt.Text != "")
                    {
                        bool valid = Host_analyze(san_sheet);
                        b.ReportProgress(30);
                        bool valid1 = Lun_Analyze(san_sheet);
                        b.ReportProgress(60);
                        if (valid == true && valid1 == true)
                        {
                            try
                            {
                                //Create host details table , Check unique ?
                                string sql = "create table SAN_" + san_id_text.Text + @"_Host (server_name varchar(100) ,os_name varchar(50), wwpn varchar(200), ip_addr varchar(100))";
                                SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                                command.ExecuteNonQuery().ToString();

                                foreach (DataRow dr in host_details_dt.Rows)
                                {
                                    SQLiteCommand sql_cmd = new SQLiteCommand("insert into SAN_" + san_id_text.Text + "_Host values (@server_name, @os_name, @wwpn ,@ip_addr)", m_dbConnection);
                                    sql_cmd.Parameters.AddWithValue("server_name", dr[0].ToString());
                                    sql_cmd.Parameters.AddWithValue("os_name", dr[1].ToString());

                                    sql_cmd.Parameters.AddWithValue("wwpn", dr[2].ToString());
                                    sql_cmd.Parameters.AddWithValue("ip_addr", dr[3].ToString());
                                    sql_cmd.ExecuteNonQuery();
                                }
                                b.ReportProgress(80);
                                //loading_lbl.Text = "Loading ... Updating Host details";
                                //Create lun details table , Insert only unique values
                                sql = "create table SAN_" + san_id_text.Text + @"_Lun (server_name varchar(100) ,disk_size varchar(50),array_serial varchar(200), array_device varchar(200), array_device_wwn varchar(200),PRIMARY KEY(server_name,array_serial,array_device))";
                                command = new SQLiteCommand(sql, m_dbConnection);
                                command.ExecuteNonQuery().ToString();
                                //loading_lbl.Text = "Loading ... Updating Lun details";
                                foreach (DataRow dr in lun_details_dt.Rows)
                                {
                                    SQLiteCommand sql_cmd = new SQLiteCommand("insert or ignore into SAN_" + san_id_text.Text + "_Lun(server_name,disk_size,array_serial,array_device,array_device_wwn) values (@server_name, @disk_size,@array_serial, @array_device,@device_wwn)", m_dbConnection);
                                    sql_cmd.Parameters.AddWithValue("server_name", dr[0].ToString());
                                    sql_cmd.Parameters.AddWithValue("disk_size", dr[1].ToString());
                                    sql_cmd.Parameters.AddWithValue("array_serial", dr[2].ToString());
                                    sql_cmd.Parameters.AddWithValue("array_device", dr[3].ToString());
                                    sql_cmd.Parameters.AddWithValue("device_wwn", dr[4].ToString());
                                    sql_cmd.ExecuteNonQuery();
                                }
                                b.ReportProgress(100);
                                /*loading_lbl.Text = "Completed";
                                MessageBox.Show("Loaded Successfully", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                Script_Generator_landingpage form1 = new Script_Generator_landingpage((san_id_text.Text), m_dbConnection, ip_addr, os_version);
                                form1.ShowDialog();
                                excel_workbook.Close();
                                excel.Quit();
                                this.Close();*/

                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message.ToString());
                            }
                        }
                        else
                        {
                            MessageBox.Show("Unable to load San Summary details!! Check the file content!", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please enter all details", "Missing Details", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                });

                bw.ProgressChanged += new ProgressChangedEventHandler(
                delegate(object o, ProgressChangedEventArgs args)
                {
                   
                        percent_completed.Value = args.ProgressPercentage;
                });
                bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(
               delegate(object o, RunWorkerCompletedEventArgs args)
               {
                   if (args.Cancelled != true)
                   {
                       percent_completed.Value = 100;
                       percent_completed.Visible = false;
                       loading_lbl.Text = "Completed";
                       MessageBox.Show("Loaded Successfully", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                       Script_Generator_landingpage form1 = new Script_Generator_landingpage((san_id_text.Text), m_dbConnection, ip_addr, os_version);
                       form1.ShowDialog();
                       excel_workbook.Close(0);
                       excel.Quit();
                       this.Close();
                   }
                  
               });

                bw.RunWorkerAsync();
            }

        }

        private void san_upload_search_Load(object sender, EventArgs e)
        {

        }

       
    }
}
