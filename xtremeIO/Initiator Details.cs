﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Data.SQLite;
namespace xtremeIO
{
    public partial class Initiator_Details : Form
    {
        #region Global Variables
        public string logPath;
        LandingPage backInstance;
        DataTable xio_dt = new DataTable();
        SQLiteConnection m_dbconnection;
        
        public string[] initiatorText;
        public string[] initiatorData;
        List<string> clusterNames = new List<string>();
        List<string> igNames = new List<string>();
        List<string> lunMappingNames = new List<string>();
        Dictionary<string, int> allInitiatorNames = new Dictionary<string, int>();
        Dictionary<string, int> allVolNames = new Dictionary<string, int>();
        private SQLiteDataReader dr;
        #endregion
        #region Load Form
        public Initiator_Details(SQLiteConnection path, string xms_ip, string xms_os, LandingPage formInstance)
        {
            m_dbconnection = path;

            InitializeComponent();
            xms_ip_lbl.Text = xms_ip;
            xms_os_lbl.Text = xms_os;
            xms_os_lbl.Visible = true;
            xms_ip_lbl.Visible = true;
            backInstance = formInstance;
        }
        #endregion
        #region Host Name changed
        private void initiatorList_SelectedIndexChanged(object sender, EventArgs e)
        {
            xio_dt.Clear();
            
            target_connected_lbl.Items.Clear();
        
           
            string ininame = initiatorList.Text;
            initiator_name_lbl.Text = "Host Name : ";
            
            cluster_name_lbl.Text = "Cluster Name : ";
            

           
            string sql = "select * from initiatordetails where igname='" + ininame + "' group by igname";
            SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                initiator_name_lbl.Text = initiator_name_lbl.Text + dr[5].ToString();
              //  OS_type_lbl.Text = OS_type_lbl.Text + dr[6].ToString().ToUpper();
                cluster_name_lbl.Text = cluster_name_lbl.Text + dr[2].ToString();
              //  port_type_lbl.Text = port_type_lbl.Text + dr[3].ToString().ToUpper();
              //  port_address_lbl.Text = port_address_lbl.Text + dr[4].ToString();
               
              
                findLunMapping(dr[5].ToString());
                //  dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                //   dataGridView1.Columns[dataGridView1.ColumnCount].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
              //  var eventArgs = new DataGridViewCellEventArgs(0, 0);

                break;
            }
            sql = "select * from initiatordetails i left join initiatorconnectivity using(initiatorname) where igname='"+ininame+"'";
            cmd = new SQLiteCommand(sql, m_dbconnection);
            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            initiator_data.DataSource = dt;

            sql = @"select targetlist from initiatordetails i1 , initiatorconnectivity i2 where igname='"+ininame+"' and i1.initiatorname = i2.initiatorname";
            SQLiteCommand cmd1 = new SQLiteCommand(sql, m_dbconnection);
            SQLiteDataReader dr1 = cmd1.ExecuteReader();
            if (dr1.HasRows)
            {
                while (dr1.Read())
                {
                    string[] temp1 = dr1[0].ToString().Split(';');
                    foreach (string s1 in temp1)
                    {
                        if (!target_connected_lbl.Items.Contains(s1.ToString()))
                        {
                            target_connected_lbl.Items.Add(s1.ToString());
                        }
                    }

                }
               
            }
            else
            {
                target_connected_lbl.Items.Add("N/A");
            
            }

            fillTargetData();

        }
        #endregion
        #region Get XIO Target
        private void fillTargetData()
        {
            foreach (string s in target_connected_lbl.Items)
            {
                try
                {
                    if (s != "N/A")
                    {
                        
                        
                        string[] temp = s.Split('[');
                        string targetname = temp[0];
                        string sqlquery = "select * from targetdata where targetname='" + targetname + "' and targetsclusterName='" + dr[2].ToString() + "'";
                        SQLiteCommand cmd = new SQLiteCommand(sqlquery, m_dbconnection);
                        SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                        da.Fill(xio_dt);

                        xio_target_data.DataSource = xio_dt;
                    }
                    else
                    {
                        xio_target_data.DataSource = null;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error Occoured in processing !!" + ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        #endregion
        #region Get Lun Mapping
        private void findLunMapping(string igName)
        {
            lun_mapped_lbl.Text = "Total Lun Mapped :";
            try
            {
                DataTable dt = new DataTable();
                string sql = @"select v.volumename,igname,clustername,volsize,lbsize,physpaceused,lbnotation,createdfrom,smallIO,unalignedIO,VAAITP,naaID,creationTime from volumedetails v,lunMapping m where v.volumename = m.volumename
                and igname='" + igName + "'";
                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);

                da.Fill(dt);
               
                dt.Columns["volumename"].ColumnName = "Volume Name";
               
                dt.Columns["clustername"].ColumnName = "Cluster Name";
                dt.Columns["volsize"].ColumnName = "Volume Size";
                dt.Columns["igname"].ColumnName = "Host Name";
                dt.Columns["lbnotation"].ColumnName = "Logical Block Notation";
                dt.Columns["lbsize"].ColumnName = "Lb Size";
                dt.Columns["physpaceused"].ColumnName = "Physical Space Used";
                dt.Columns["createdfrom"].ColumnName = "Created From Volume";
                dt.Columns["smallIO"].ColumnName = "Small IO Alerts";
                dt.Columns["unalignedIO"].ColumnName = "Unaligned IO Alerts";
                dt.Columns["VAAITP"].ColumnName = "VAAI TP Alerts";
                dt.Columns["naaID"].ColumnName = "NAA Id";
                dt.Columns["creationTime"].ColumnName = "Creation Time";

                dataGridView1.DataSource = dt;
                sql = "select volumename,count(*) from lunMapping where igname='" + igName + "'";

                cmd = new SQLiteCommand(sql, m_dbconnection);
                SQLiteDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    lun_mapped_lbl.Text = lun_mapped_lbl.Text + dr[1].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message +" : error Mapping");
            }
        }
        #endregion
        #region Load Form
        private void Initiator_Details_Load(object sender, EventArgs e)
        {
            int num = 0;
            DataTable dt = new DataTable();
            string sql = "select igname,clustername,porttype from initiatordetails order by igname";
            SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
            SQLiteDataReader dr = cmd.ExecuteReader();
          
            while (dr.Read())
            {
                if(!initiatorList.Items.Contains(dr[0].ToString()))
                {
                    initiatorList.Items.Add(dr[0].ToString());
                    num++;
                }
             
              
            }
            if (num != 0)
            {
                total_initiator_lbl.Text = total_initiator_lbl.Text + num.ToString();
                initiatorList.SelectedIndex = 0;
                initiatorList_SelectedIndexChanged(null, null);
            }
            else
            {
                MessageBox.Show("No Hosts Found in the logs !!", "No Hosts", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        #endregion
        #region GO Button Click
        //Not used
        /*   private void go_btn_Click(object sender, EventArgs e)
        {
            total_initiator_lbl.Text = "Total Number of Initiators :";
            if (clustername_combobox.Text != "No Filter" && porttype_combobox.Text != "No Filter")
            {
                try
                {
                    int num = 0;
                    initiatorList.Items.Clear();
                    DataTable dt = new DataTable();
                    string sql = "select initiatorname from initiatordetails where clustername='" + clustername_combobox.Text + "' and porttype='" + porttype_combobox.Text + "'";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataReader dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            initiatorList.Items.Add(dr[0].ToString());
                            num++;
                        }
                        total_initiator_lbl.Text = total_initiator_lbl.Text + num.ToString();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + ": Error Go Button");
                }
            }
            else if (clustername_combobox.Text != "No Filter" && porttype_combobox.Text == "No Filter")
            {

                try
                {
                    int num = 0;
                    DataTable dt = new DataTable();
                    string sql = "select initiatorname from initiatordetails where clustername='" + clustername_combobox.Text + "'";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataReader dr = cmd.ExecuteReader();
                    initiatorList.Items.Clear();
                    while (dr.Read())
                    {
                        initiatorList.Items.Add(dr[0].ToString());
                        num++;
                    }
                    total_initiator_lbl.Text = total_initiator_lbl.Text + num.ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + ": Error Go Button");
                }
            }
            else if (clustername_combobox.Text == "No Filter" && porttype_combobox.Text != "No Filter")
            {
                try
                {
                    int num = 0;
                    initiatorList.Items.Clear();
                    DataTable dt = new DataTable();
                    string sql = "select initiatorname from initiatordetails where porttype='" + porttype_combobox.Text + "'";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        num++;
                        initiatorList.Items.Add(dr[0].ToString());
                    }
                    total_initiator_lbl.Text = total_initiator_lbl.Text + num.ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else if (clustername_combobox.Text == "No Filter" && porttype_combobox.Text == "No Filter")
            {
                try
                {
                    int num = 0;
                    initiatorList.Items.Clear();
                    DataTable dt = new DataTable();
                    string sql = "select initiatorname from initiatordetails order by initiatorname";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        initiatorList.Items.Add(dr[0].ToString());
                        num++;
                    }
                    total_initiator_lbl.Text = total_initiator_lbl.Text + num.ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            initiatorList.SelectedIndex = 0;
            initiatorList_SelectedIndexChanged(null, null);
        }*/
        #endregion

        #region Unchanged Old Data 
        //Do not remove this
        private void target_connected_lbl_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (target_connected_lbl.SelectedItem.ToString() != "N/A")
                {
                   
                    string s1 = target_connected_lbl.SelectedItem.ToString();
                    string[] temp = s1.Split('[');
                    string targetname = temp[0];
                    string sqlquery = "select * from targetdata where targetname='" + targetname + "' and targetsclusterName='" + dr[2].ToString() + "'";
                    SQLiteCommand cmd = new SQLiteCommand(sqlquery, m_dbconnection);
                    SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    xio_target_data.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Occoured in processing !!" + ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}

