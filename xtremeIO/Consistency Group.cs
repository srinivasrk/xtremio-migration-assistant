﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;
namespace xtremeIO
{
    public partial class Consistency_Group : Form
    {
        #region Global Variable Declation
        LandingPage form1_data;
        SQLiteConnection m_dbConnection;
       
        public Consistency_Group(SQLiteConnection conn , string xmsip , string xmsos , LandingPage back)
        {
            form1_data = back;
           
            InitializeComponent();

            xms_ip_lbl.Text =xmsip;
            xms_os_lbl.Text =xmsos;
            m_dbConnection = conn;
        }
        #endregion
        #region Form Load
        private void Consistency_Group_Load(object sender, EventArgs e)
        {
            try
            {
                string sql = "select cgname from cgdetails";
                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                SQLiteDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        cg_list.Items.Add(dr[0].ToString());
                    }
                    cg_list.SelectedIndex = 0;
                    cg_list_SelectedIndexChanged(null, null);
                }
                else
                {
                    MessageBox.Show("No Consistency groups exists in this XMS");
                    this.Close();
                    form1_data.ShowDialog();

                }
            }
            catch(Exception ex)
            {
                //Empty catch to handle the Out of Debugging run
            }
        }
           
    

        #endregion
        #region CG List Click
        private void cg_list_SelectedIndexChanged(object sender, EventArgs e)
        {
          
            string sql = "select * from cgdetails where cgname='"+cg_list.Text.Trim()+"'";
            SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                cg_name_lbl.Text = "Consistency Group Name : " + dr[0].ToString();
                if (dr[0].ToString().StartsWith("RP") || dr[0].ToString().StartsWith("rp"))
                {
                    cg_pic.Image = Properties.Resources.EMC_RP1;
                }
                else
                {
                    cg_pic.Image = Properties.Resources.XtremIO_storage_2;
                }
                cg_index_lbl.Text = "CG Index : " + dr[1].ToString();
                cluster_name_lbl.Text = "Cluster Name : " + dr[2].ToString();
                num_of_vol_lbl.Text = "Number of Volumes in CG : " + dr[3].ToString();
            }
            sql = @"select volname,volumeindex,clustername,volsize,lbsize,physpaceused,
            lbnotation,createdfrom,smallIO,unalignedIO,VAAITP,naaID,creationTime from cgmapping cg , 
            volumedetails v where cg.cgname='" +cg_list.Text+"' and cg.volname =  v.volumename";
            cmd = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
            DataTable dt1 = new DataTable();
            da.Fill(dt1);
          
            dt1.Columns["volname"].ColumnName = "Volume Name";
            dt1.Columns["volumeindex"].ColumnName = "Volume Index";
            dt1.Columns["clustername"].ColumnName = "Cluster Name";
            dt1.Columns["volsize"].ColumnName = "Volume Size";
            dt1.Columns["lbsize"].ColumnName = "Lb Size";
            dt1.Columns["physpaceused"].ColumnName = "Logical Space Used";
            dt1.Columns["createdfrom"].ColumnName = "Created From Volume";
            dt1.Columns["smallIO"].ColumnName = "Small IO Alerts";
            dt1.Columns["unalignedIO"].ColumnName = "Unaligned IO Alerts";
            dt1.Columns["VAAITP"].ColumnName = "VAAI TP Alerts";
            dt1.Columns["naaID"].ColumnName = "NAA Id";
            dt1.Columns["creationTime"].ColumnName = "Creation Time";
            
            cg_vol_mapping.DataSource = dt1;
           
            cg_vol_mapping_CellContentClick(null, null);
        }
        #endregion
        #region Volume Mapping for CG
        private void cg_vol_mapping_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string s = "", realvolsize="";
            vol_percent_used_lbl.Text = "Logical Percent Used : ";
          
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            if (!(cg_vol_mapping.SelectedCells != null))
            {
                MessageBox.Show("yes");
            }
            int selectedindex = cg_vol_mapping.Rows[cg_vol_mapping.CurrentCell.RowIndex].Cells[0].RowIndex;
            string selectedval = cg_vol_mapping.Rows[selectedindex].Cells[0].Value.ToString();
            string sql = "select * from volumedetails where volumename='" + selectedval + "'";
            if (selectedval != "")
            {
                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                try
                {

                    SQLiteDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        // fill data on lbls
                        vol_index_lbl.Text = "Volume Index : " + dr[1].ToString();
                        s = dr[3].ToString();
                        realvolsize = dr[7].ToString();
                    }

                    if (s[s.Length - 1] == 'T')
                    {
                        s = s.Trim();
                        double totalsize = double.Parse(s.Substring(0, s.Length - 1));
                        totalsize = (totalsize * 1024);
                        double usedspace = double.Parse(realvolsize.ToString());
                        double percentused = (usedspace * 100) / totalsize;
                        setvolpic(percentused);
                        vol_percent_used_lbl.Text = vol_percent_used_lbl.Text + Math.Round(percentused, 2) + "%";
                    }
                    else if (s[s.Length - 1] == 'K')
                    {
                        s = s.Trim();
                        double totalsize = double.Parse(s.Substring(0, s.Length - 1));
                        totalsize = totalsize / (1024 * 1024);
                        double usedspace = double.Parse(realvolsize.ToString());
                        double percentused = (usedspace * 100) / totalsize;
                        setvolpic(percentused);
                        vol_percent_used_lbl.Text = vol_percent_used_lbl.Text + Math.Round(percentused, 2) + "%";
                    }
                    else if (s[s.Length - 1] == 'P')
                    {
                        s = s.Trim();
                        double totalsize = double.Parse(s.Substring(0, s.Length - 1));
                        totalsize = (totalsize * 1024 * 1024);
                        double usedspace = double.Parse(realvolsize.ToString());
                        double percentused = (usedspace * 100) / totalsize;
                        setvolpic(percentused);
                        vol_percent_used_lbl.Text = vol_percent_used_lbl.Text + Math.Round(percentused, 2) + "%";
                    }
                    else if (s[s.Length - 1] == 'M')
                    {
                        s = s.Trim();
                        double totalsize = double.Parse(s.Substring(0, s.Length - 1));
                        totalsize = (totalsize / 1024);
                        double usedspace = double.Parse(realvolsize.ToString());
                        double percentused = (usedspace * 100) / totalsize;
                        setvolpic(percentused);
                        vol_percent_used_lbl.Text = vol_percent_used_lbl.Text + Math.Round(percentused, 2) + "%";
                    }
                    else if (s[s.Length - 1] == 'G')
                    {
                        s = s.Trim();
                        double totalsize = double.Parse(s.Substring(0, s.Length - 1));
                        double usedspace = double.Parse(realvolsize.ToString());
                        double percentused = (usedspace * 100) / totalsize;
                        setvolpic(percentused);
                        vol_percent_used_lbl.Text = vol_percent_used_lbl.Text + Math.Round(percentused, 2) + "%";
                    }
                    else if (s.Length == 1)
                    {
                        s = s.Trim();

                        setvolpic(0);
                        vol_percent_used_lbl.Text = "0%";
                    }


                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            
            }
        }
        #endregion
        #region Get Pic For CG
        private void setvolpic(double percentused)
        {
            if (Math.Round(percentused, 2) == 0)
            {
                vol_pic.Image = Properties.Resources.database_512;
            }
            else if (Math.Round(percentused, 2) <= 10)
            {
                vol_pic.Image = Properties.Resources.vol_10percent;
            }
            else if (Math.Round(percentused, 2) <= 30)
            {
                vol_pic.Image = Properties.Resources.vol_25percent;
            }
            else if (Math.Round(percentused, 2) <= 50)
            {
                vol_pic.Image = Properties.Resources.vol_50percent;
            }
            else if (Math.Round(percentused, 2) <= 75)
            {
                vol_pic.Image = Properties.Resources.vol_75percent;
            }
            else if (Math.Round(percentused, 2) <= 90)
            {
                vol_pic.Image = Properties.Resources.vol_90percent;
            }
            else if (Math.Round(percentused, 2) > 90)
            {
                vol_pic.Image = Properties.Resources.vol_100percent;
            }
        }
        #endregion
        #region CG Mapping Click
        private void cg_vol_mapping_SelectionChanged(object sender, EventArgs e)
        {
            cg_vol_mapping_CellContentClick(null, null);
        }

        #endregion



    }
}
