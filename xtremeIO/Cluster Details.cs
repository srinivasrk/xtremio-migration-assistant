﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SQLite;
namespace xtremeIO
{
    public partial class Cluster_Details : Form
    {
        #region Global Variable Declaration
        LandingPage form1_data;
        public string logPath;
        public string[] clusterData;
        SQLiteConnection m_dbConnection;
        List<string> clusterNames = new List<string>();
        List<string> mgmtIPaddrA = new List<string>();
        List<string> mgmtIPaddrB = new List<string>();
        List<string> mgmtIP = new List<string>();
        List<string> healthState = new List<string>();
        List<string> gatewayIP = new List<string>();
        List<string> subnetAddr = new List<string>();
        Dictionary<string, int> allScNames = new Dictionary<string, int>();
        
        public Cluster_Details(SQLiteConnection path, string xms_ip, string xms_os, LandingPage form1_instance)
        {
            form1_data = form1_instance;
            InitializeComponent();
            if (path != null)
            {
                m_dbConnection = path;
                xms_ip_lbl.Text = xms_ip;
                xms_os_lbl.Text = xms_os;
            }
            else
            {
                MessageBox.Show("Error loading path , Please reload the application", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion
        #region Form Load
        private void Cluster_Details_Load(object sender, EventArgs e)
        {
            try
            {
                string sql = "select clustername from clusterdetails order by clustername";
                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                SQLiteDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    clusterList.Items.Add(dr[0].ToString());
                }
                clusterList.SelectedIndex = 0;
                clusterList_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
        #region Old Back Button
        private void back_btn_Click(object sender, EventArgs e)
        {
            this.Hide();
            form1_data.Show();
            this.Close();
        }
        #endregion
        #region Cluster List Click
        private void clusterList_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string clustername = clusterList.Text;
            string sql = @"select clustername, clusterindex ,psnt ,num_of_vol ,
                num_of_bricks ,size_capacity ,dedup_ratio ,free_phy_space ,free_logical_space ,
                sw_version ,thin_provisioning_savings from clusterdetails where clustername='" + clustername + "'";
            SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
            try
            {
                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
               
                da.Fill(dt);
                dt.Columns["clustername"].ColumnName = "Cluster Name";
                dt.Columns["clusterindex"].ColumnName = "Cluster Index";
                dt.Columns["psnt"].ColumnName = "PSNT";
                dt.Columns["num_of_vol"].ColumnName = "Number of Volumes";
                dt.Columns["num_of_bricks"].ColumnName = "Number of Bricks";
                dt.Columns["size_capacity"].ColumnName = "Size and Capacity";
                dt.Columns["dedup_ratio"].ColumnName = "Deduplication Ratio";
                dt.Columns["free_phy_space"].ColumnName = "Free Physical Space(%)";
                dt.Columns["thin_provisioning_savings"].ColumnName = "Thin Provisioning Savings(%)";
                dt.Columns["free_logical_space"].ColumnName = "Free Logical Space(%)";
                dt.Columns["sw_version"].ColumnName = "Software Version";
                for (int i = 0; i < 5; i++)
                {
                    dt.Rows.Add();
                }
                clusterInfoView.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            findControllerDetails(clustername);
        }
        #endregion
        #region Find Controller Details
        private void findControllerDetails(string clustername)
        {
            storageController.TabPages.Clear();

            string sql = "Select * from scDetails where clustername='"+clustername+"'";
            SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                TabPage page = new TabPage(dr[0].ToString());
                DataGridView dgv = new DataGridView();
                dgv.Dock = DockStyle.Fill;
                dgv.Columns.Add("Col1", "Storage Controller Info");
                dgv.Rows.Add("Mgmt IP Address : " + dr[5].ToString());
                dgv.Rows.Add("Mgmt Subnet Address : " + dr[8].ToString());
                dgv.Rows.Add("Mgmt Gateway Address : " + dr[7].ToString());
                dgv.Rows.Add("SC 1 IP Address : " + dr[3].ToString());
                dgv.Rows.Add("SC 2 IP Address : " + dr[4].ToString());
                dgv.Rows.Add("State : " + dr[6].ToString());
                dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                dgv.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                dgv.Columns[dgv.ColumnCount - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dgv.ColumnHeadersDefaultCellStyle = clusterInfoView.ColumnHeadersDefaultCellStyle;
                dgv.RowsDefaultCellStyle = clusterInfoView.RowsDefaultCellStyle;
                dgv.ColumnHeadersHeight = 40;
                dgv.AllowUserToAddRows = false;
                dgv.AllowUserToDeleteRows = false;
                dgv.AllowUserToOrderColumns = false;
                dgv.ColumnHeadersDefaultCellStyle.Font = new Font("Times New Roman", 12);
                dgv.RowsDefaultCellStyle.Font = new Font("Times New Roman", 12);
                page.Controls.Add(dgv);
                storageController.TabPages.Add(page);
            }
        }

        #endregion

        private void pictureBox2_Click(object sender, EventArgs e)
        {
        }

    }
}
