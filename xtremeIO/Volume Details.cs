﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Data.SQLite;
namespace xtremeIO
{
    public partial class Volume_Details : Form
    {
        #region Global Variable Load
        public string[] volumeText;
        LandingPage form1_data;
        SQLiteConnection m_dbConnection;
        int totalrowcount=0;
        Dictionary<string,int> allVolNames = new Dictionary<string,int>();
        Dictionary<string, int> tempAllVolNames = new Dictionary<string, int>();
        Dictionary<string,string> itemsToRemove = new Dictionary<string,string>();
        List<string> clusterNames = new List<string>();
        private string realvolsize;
        public Volume_Details(SQLiteConnection conn,string xms_ip,string xms_os,LandingPage form1_instance)
        {
            form1_data = form1_instance;
            InitializeComponent();
            if (conn != null)
            {
                m_dbConnection = conn;
                xms_ip_lbl.Text = xms_ip;
                xms_os_lbl.Text = xms_os;
                
            }
            else
            {
                MessageBox.Show("Error loading path , Please reload the application", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }
        #endregion
        #region Volume List Changed
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            getHostMapping();
            vol_percent_used_lbl.Text = "Logical Percent Used : ";
            total_snap_lbl.Text = "Total Number Of SnapShots : ";
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            string selectedval = volumeList.Text;
            string sql = "select * from volumedetails where volumename='"+selectedval+"'";
            SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
            try
            {
                
                SQLiteDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    // fill data on lbls
                    vol_name_lbl.Text = "Volume Name : " + dr[0].ToString();
                    vol_index_lbl.Text = "Volume Index : " + dr[1].ToString();
                    cluster_name_lbl.Text = "Cluster Name : " + dr[2].ToString();
                    vol_size_lbl.Text = "Volume Size : " + dr[3].ToString();
                    created_from_lbl.Text = "Created From : " + dr[8].ToString();
                    s = dr[3].ToString();
                    used_size_lbl.Text = "Logical Volume Used : " + dr[5].ToString() + " " + dr[6].ToString();
                    realvolsize = dr[7].ToString();
                    created_on_lbl.Text = "Created On : " + dr[13].ToString();
                    small_io_lbl.Text = "Small IO Alerts : " + dr[9].ToString();
                    unaligned_io_lbl.Text = "Unaligned IO Alert : " + dr[10].ToString();
                    vaai_tp_lbl.Text = "VAAI TP Alerts : " + dr[11].ToString();
                    naa_id_lbl.Text = "NAA ID : " + dr[12].ToString();
                    
                }
              
                if (s[s.Length - 1] == 'T')
                {
                    s = s.Trim();
                    double totalsize = double.Parse(s.Substring(0, s.Length - 1));
                    totalsize = (totalsize * 1024);
                    double usedspace = double.Parse(realvolsize.ToString());
                    double percentused = (usedspace * 100) / totalsize;
                    setvolpic(percentused);
                    vol_percent_used_lbl.Text = vol_percent_used_lbl.Text + Math.Round(percentused, 2) + "%";
                }
                else if (s[s.Length - 1] == 'K')
                {
                    s = s.Trim();
                    double totalsize = double.Parse(s.Substring(0, s.Length - 1));
                    totalsize = totalsize / (1024 * 1024);
                    double usedspace = double.Parse(realvolsize.ToString());
                    double percentused = (usedspace * 100) / totalsize;
                    setvolpic(percentused);
                    vol_percent_used_lbl.Text = vol_percent_used_lbl.Text + Math.Round(percentused, 2) + "%";
                }
                else if (s[s.Length - 1] == 'P')
                {
                    s = s.Trim();
                    double totalsize = double.Parse(s.Substring(0, s.Length - 1));
                    totalsize = (totalsize * 1024 * 1024);
                    double usedspace = double.Parse(realvolsize.ToString());
                    double percentused = (usedspace * 100) / totalsize;
                    setvolpic(percentused);
                    vol_percent_used_lbl.Text = vol_percent_used_lbl.Text + Math.Round(percentused, 2) + "%";
                }
                else if (s[s.Length - 1] == 'M')
                {
                    s = s.Trim();
                    double totalsize = double.Parse(s.Substring(0, s.Length - 1));
                    totalsize = (totalsize / 1024);
                    double usedspace = double.Parse(realvolsize.ToString());
                    double percentused = (usedspace * 100) / totalsize;
                    setvolpic(percentused);
                    vol_percent_used_lbl.Text = vol_percent_used_lbl.Text + Math.Round(percentused, 2) + "%";
                }
                else if (s[s.Length - 1] == 'G')
                {
                    s = s.Trim();
                    double totalsize = double.Parse(s.Substring(0, s.Length - 1));
                    double usedspace = double.Parse(realvolsize.ToString());
                    double percentused = (usedspace * 100) / totalsize;
                    setvolpic(percentused);
                    vol_percent_used_lbl.Text = vol_percent_used_lbl.Text + Math.Round(percentused, 2) + "%";
                }
                else if (s.Length == 1)
                {
                    s = s.Trim();
                   
                    setvolpic(0);
                    vol_percent_used_lbl.Text = "0%";
                }
              

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


           


            sql = "select * from volumedetails where createdfrom ='" + selectedval + "'";
            string sql1 = "select count(*) from volumedetails where createdfrom ='" + selectedval + "'";
            SQLiteCommand cmd1 = new SQLiteCommand(sql, m_dbConnection);
            SQLiteCommand cmd2 = new SQLiteCommand(sql1, m_dbConnection);
            try
            {

                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd1);
                da.Fill(dt1);
                dt1.Columns.Remove("realvolsize");
                dt1.Columns["volumename"].ColumnName = "Volume Name";
                dt1.Columns["volumeindex"].ColumnName = "Volume Index";
                dt1.Columns["clustername"].ColumnName = "Cluster Name";
                dt1.Columns["volsize"].ColumnName = "Volume Size";
                dt1.Columns["lbsize"].ColumnName = "Lb Size";
                dt1.Columns["physpaceused"].ColumnName = "Logical Space Used";
                dt1.Columns["createdfrom"].ColumnName = "Created From Volume";
                dt1.Columns["smallIO"].ColumnName = "Small IO Alerts";
                dt1.Columns["unalignedIO"].ColumnName = "Unaligned IO Alerts";
                dt1.Columns["VAAITP"].ColumnName = "VAAI TP Alerts";
                dt1.Columns["naaID"].ColumnName = "NAA Id";
                dt1.Columns["creationTime"].ColumnName = "Creation Time";
                for (int i = 0; i < 5; i++)
                {
                    dt1.Rows.Add();
                }
                if (dt1.Rows.Count > 5)
                {
                    snapshotDetailsGrid.DataSource = dt1;
                    status_lbl.Text = "";
                }
                else
                {
                    snapshotDetailsGrid.DataSource = null;
                    status_lbl.Text = "No Snapshots available for the Volume selected";
                }
                SQLiteDataReader dr = cmd2.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        total_snap_lbl.Text = total_snap_lbl.Text + dr[0].ToString();
                    }
                }
                else
                {
                    total_snap_lbl.Text = total_snap_lbl.Text + "0";
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
        #region Set Vol Used Pic
        private void setvolpic(double percentused)
        {
            if (Math.Round(percentused, 2) == 0)
            {
                vol_pic.Image = Properties.Resources.database_512;
            }
            else if (Math.Round(percentused, 2) <= 10)
            {
                vol_pic.Image = Properties.Resources.vol_10percent;
            }
            else if (Math.Round(percentused, 2) <= 30)
            {
                vol_pic.Image = Properties.Resources.vol_25percent;
            }
            else if (Math.Round(percentused, 2) <= 50)
            {
                vol_pic.Image = Properties.Resources.vol_50percent;
            }
            else if (Math.Round(percentused, 2) <= 75)
            {
                vol_pic.Image = Properties.Resources.vol_75percent;
            }
            else if (Math.Round(percentused, 2) <= 90)
            {
                vol_pic.Image = Properties.Resources.vol_90percent;
            }
            else if (Math.Round(percentused, 2) > 90)
            {
                vol_pic.Image = Properties.Resources.vol_100percent;
            }
        }
        #endregion
        #region Form Load
        private void Volume_Details_Load(object sender, EventArgs e)
        {
            
            string sql = "select * from volumedetails";
            SQLiteCommand cmd = new SQLiteCommand(sql,m_dbConnection);
            try
            {

                SQLiteDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    volumeList.Items.Add(dr[0].ToString());
                    totalrowcount++;
                }
                sql = "select count(*) from volumedetails";
                cmd = new SQLiteCommand(sql, m_dbConnection);
                SQLiteDataReader dr1 = cmd.ExecuteReader();
                total_vol_lbl.Text = total_vol_lbl.Text + dr1[0].ToString();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            clustername_combobox.Items.Add("No Filter");
            clustername_combobox.SelectedIndex = clustername_combobox.Items.IndexOf("No Filter");
            voltype_combobox.SelectedIndex = voltype_combobox.Items.IndexOf("All");
            sql = "select clustername from volumedetails";
            cmd = new SQLiteCommand(sql, m_dbConnection);
            try
            {
                SQLiteDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (!clustername_combobox.Items.Contains(dr[0].ToString()))
                    {
                        clustername_combobox.Items.Add(dr[0].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            if (totalrowcount != 0)
            {
                volumeList.SelectedIndex = 0;
                listBox1_SelectedIndexChanged(null, null);
                data_filter_combobox.SelectedIndex = data_filter_combobox.Items.IndexOf("No Filter");
                getHostMapping();
            }
            else
            {
                MessageBox.Show("No Volumes Found in the Logs !! ", "No Volumes", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        #endregion
        #region Get Host Mapping
        private void getHostMapping()
        {
            string sql = "select igname from lunmapping where volumename='" + volumeList.Text + "'";
            SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < 5; i++)
            {
                dt.Rows.Add();
            }
            dt.Columns[0].ColumnName = " Host Names";
            hostmapping_data.DataSource = dt;
            
        }
        #endregion
        #region Old back Button
        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            form1_data.Show();
            this.Close();
        }
        #endregion
        #region Go Button Click
        private void button1_Click_1(object sender, EventArgs e)
        {
            total_vol_lbl.Text = "Items Displayed : ";
            string clustername = clustername_combobox.Text;
            string filter = data_filter_combobox.Text;
            #region No Filter No Filter ALL
            if (clustername != "No Filter" && filter == "No Filter" && voltype_combobox.Text =="All")
            {
                string sql = "select volumename,(select count(*) from volumedetails where clustername='"+clustername+"') from volumedetails where clustername='" + clustername + "'";
                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                volumeList.Items.Clear();
                try
                {
                    SQLiteDataReader dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        vol_size_lbl.Visible = true;
                        cluster_name_lbl.Visible = true;
                        used_size_lbl.Visible = true;
                        created_from_lbl.Visible = true;
                        vol_name_lbl.Visible = true;
                        vol_index_lbl.Visible = true;
                        created_on_lbl.Visible = true;
                        vol_percent_used_lbl.Visible = true;
                        vol_pic.Visible = true;
                        small_io_lbl.Visible = true;
                        unaligned_io_lbl.Visible = true;
                        vaai_tp_lbl.Visible = true;
                        naa_id_lbl.Visible = true;
                        pictureBox3.Visible = true;
                        pictureBox4.Visible = true;
                        while (dr.Read())
                        {
                            volumeList.Items.Add(dr[0].ToString());
                            total_vol_lbl.Text = "Items Displayed : ";
                            total_vol_lbl.Text = total_vol_lbl.Text + dr[1].ToString();
                        }
                        volumeList.SelectedIndex = 0;
                    }
                    else
                    {
                        MessageBox.Show("No Data for Selected Filter", "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        volumeList.Text = "";
                        vol_size_lbl.Visible = false;
                        cluster_name_lbl.Visible = false;
                        used_size_lbl.Visible = false;
                        created_from_lbl.Visible = false;
                        vol_name_lbl.Visible = false;
                        vol_index_lbl.Visible = false;
                        created_on_lbl.Visible = false;
                        vol_percent_used_lbl.Visible = false;
                        vol_pic.Visible = false;
                        small_io_lbl.Visible = false;
                        unaligned_io_lbl.Visible = false;
                        vaai_tp_lbl.Visible = false;
                        naa_id_lbl.Visible = false;
                        pictureBox3.Visible = false;
                        pictureBox4.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            #endregion
            #region No Filter No Filter VolumeDetails Only
            if (clustername != "No Filter" && filter == "No Filter" && voltype_combobox.Text == "Volume Details Only")
            {
                string sql = "select volumename, (select count(*) from volumedetails where clustername='" + clustername + "' and createdfrom='N/A')  from volumedetails where clustername='" + clustername + "' and createdfrom='N/A'";
                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                volumeList.Items.Clear();
                try
                {
                    int num = 0;
                    SQLiteDataReader dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        vol_size_lbl.Visible = true;
                        cluster_name_lbl.Visible = true;
                        used_size_lbl.Visible = true;
                        created_from_lbl.Visible = true;
                        vol_name_lbl.Visible = true;
                        vol_index_lbl.Visible = true;
                        created_on_lbl.Visible = true;
                        vol_percent_used_lbl.Visible = true;
                        vol_pic.Visible = true;
                        small_io_lbl.Visible = true;
                        unaligned_io_lbl.Visible = true;
                        vaai_tp_lbl.Visible = true;
                        naa_id_lbl.Visible = true;
                        pictureBox3.Visible = true;
                        pictureBox4.Visible = true;
                        while (dr.Read())
                        {
                            volumeList.Items.Add(dr[0].ToString());
                            num++;
                        }
                        volumeList.SelectedIndex = 0;
                        total_vol_lbl.Text = "Items Displayed : ";
                        total_vol_lbl.Text = total_vol_lbl.Text + num.ToString();
                    }
                    else
                    {
                        MessageBox.Show("No Data for Selected Filter", "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        volumeList.Text = "";
                        vol_size_lbl.Visible = false;
                        cluster_name_lbl.Visible = false;
                        used_size_lbl.Visible = false;
                        created_from_lbl.Visible = false;
                        vol_name_lbl.Visible = false;
                        vol_index_lbl.Visible = false;
                        created_on_lbl.Visible = false;
                        vol_percent_used_lbl.Visible = false;
                        vol_pic.Visible = false;
                        small_io_lbl.Visible = false;
                        unaligned_io_lbl.Visible = false;
                        vaai_tp_lbl.Visible = false;
                        naa_id_lbl.Visible = false;
                        pictureBox3.Visible = false;
                        pictureBox4.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            #endregion
            #region No Filter No Filter SnapShot Details only
            if (clustername != "No Filter" && filter == "No Filter" && voltype_combobox.Text == "Snapshot Details Only")
            {
                string sql = "select volumename , (select count(*) from volumedetails where clustername='"+clustername+"' and createdfrom != 'N/A') from volumedetails where clustername='" + clustername + "' and createdfrom!='N/A'";
                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                volumeList.Items.Clear();
                try
                {
                    SQLiteDataReader dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        vol_size_lbl.Visible = true;
                        cluster_name_lbl.Visible = true;
                        used_size_lbl.Visible = true;
                        created_from_lbl.Visible = true;
                        vol_name_lbl.Visible = true;
                        vol_index_lbl.Visible = true;
                        created_on_lbl.Visible = true;
                        vol_percent_used_lbl.Visible = true;
                        vol_pic.Visible = true;
                        small_io_lbl.Visible = true;
                        unaligned_io_lbl.Visible = true;
                        vaai_tp_lbl.Visible = true;
                        naa_id_lbl.Visible = true;
                        pictureBox3.Visible = true;
                        pictureBox4.Visible = true;
                        while (dr.Read())
                        {
                            volumeList.Items.Add(dr[0].ToString());
                            total_vol_lbl.Text = "Items Displayed : ";
                            total_vol_lbl.Text = total_vol_lbl.Text + dr[1].ToString();
                        }
                        volumeList.SelectedIndex = 0;
                    }
                    else
                    {
                        MessageBox.Show("No Data for Selected Filter", "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        volumeList.Text = "";
                        vol_size_lbl.Visible = false;
                        cluster_name_lbl.Visible = false;
                        used_size_lbl.Visible = false;
                        created_from_lbl.Visible = false;
                        vol_name_lbl.Visible = false;
                        vol_index_lbl.Visible = false;
                        created_on_lbl.Visible = false;
                        vol_percent_used_lbl.Visible = false;
                        vol_pic.Visible = false;
                        small_io_lbl.Visible = false;
                        unaligned_io_lbl.Visible = false;
                        vaai_tp_lbl.Visible = false;
                        naa_id_lbl.Visible = false;
                        pictureBox3.Visible = false;
                        pictureBox4.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            #endregion
            #region No Filter No Filter All
            if (clustername != "No Filter" && filter != "No Filter" && voltype_combobox.Text == "All")
            {
                if(filter=="Top 10 used volumes")
                {
                    volumeList.Items.Clear();
                    string sql = "select volumename ,  (select count(*) from volumedetails where clustername='" + clustername + "')  from volumedetails where clustername='" + clustername + "' order by realvolsize desc limit 10";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                    volumeList.Items.Clear();
                    try
                    {
                        int num = 0;
                        SQLiteDataReader dr = cmd.ExecuteReader();
                        if (dr.HasRows)
                        {
                            vol_size_lbl.Visible = true;
                            cluster_name_lbl.Visible = true;
                            used_size_lbl.Visible = true;
                            created_from_lbl.Visible = true;
                            vol_name_lbl.Visible = true;
                            vol_index_lbl.Visible = true;
                            created_on_lbl.Visible = true;
                            vol_percent_used_lbl.Visible = true;
                            vol_pic.Visible = true;
                            small_io_lbl.Visible = true;
                            unaligned_io_lbl.Visible = true;
                            vaai_tp_lbl.Visible = true;
                            naa_id_lbl.Visible = true;
                            pictureBox3.Visible = true;
                            pictureBox4.Visible = true;
                            while (dr.Read())
                            {
                                volumeList.Items.Add(dr[0].ToString());
                                num++;
                            }
                            volumeList.SelectedIndex = 0;
                            total_vol_lbl.Text = "Items Displayed : ";
                            total_vol_lbl.Text = total_vol_lbl.Text + num.ToString();
                        }
                        else
                        {
                            MessageBox.Show("No Data for Selected Filter", "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            volumeList.Text = "";
                            vol_size_lbl.Visible = false;
                            cluster_name_lbl.Visible = false;
                            used_size_lbl.Visible = false;
                            created_from_lbl.Visible = false;
                            vol_name_lbl.Visible = false;
                            vol_index_lbl.Visible = false;
                            created_on_lbl.Visible = false;
                            vol_percent_used_lbl.Visible = false;
                            vol_pic.Visible = false;
                            small_io_lbl.Visible = false;
                            unaligned_io_lbl.Visible = false;
                            vaai_tp_lbl.Visible = false;
                            naa_id_lbl.Visible = false;
                            pictureBox3.Visible = false;
                            pictureBox4.Visible = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
           
               
                if (filter == "Least 10 used volumes" && voltype_combobox.Text == "All")
                {
                    volumeList.Items.Clear();
                    string sql = "select volumename , (select count(*) from volumedetails where clustername='" + clustername + "')  from volumedetails where clustername='" + clustername + "' order by realvolsize limit 10";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                    volumeList.Items.Clear();
                    try
                    {
                        int num = 0;
                        SQLiteDataReader dr = cmd.ExecuteReader();
                        if (dr.HasRows)
                        {
                            vol_size_lbl.Visible = true;
                            cluster_name_lbl.Visible = true;
                            used_size_lbl.Visible = true;
                            created_from_lbl.Visible = true;
                            vol_name_lbl.Visible = true;
                            vol_index_lbl.Visible = true;
                            created_on_lbl.Visible = true;
                            vol_percent_used_lbl.Visible = true;
                            vol_pic.Visible = true;
                            small_io_lbl.Visible = true;
                            unaligned_io_lbl.Visible = true;
                            vaai_tp_lbl.Visible = true;
                            naa_id_lbl.Visible = true;
                            pictureBox3.Visible = true;
                            pictureBox4.Visible = true;
                            while (dr.Read())
                            {
                                volumeList.Items.Add(dr[0].ToString());
                                num++;
                            }
                            volumeList.SelectedIndex = 0;
                            total_vol_lbl.Text = "Items Displayed : ";
                            total_vol_lbl.Text = total_vol_lbl.Text + num.ToString();
                        }
                        else
                        {
                            MessageBox.Show("No Data for Selected Filter", "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            volumeList.Text = "";
                            vol_size_lbl.Visible = false;
                            cluster_name_lbl.Visible = false;
                            used_size_lbl.Visible = false;
                            created_from_lbl.Visible = false;
                            vol_name_lbl.Visible = false;
                            vol_index_lbl.Visible = false;
                            created_on_lbl.Visible = false;
                            vol_percent_used_lbl.Visible = false;
                            vol_pic.Visible = false;
                            small_io_lbl.Visible = false;
                            unaligned_io_lbl.Visible = false;
                            vaai_tp_lbl.Visible = false;
                            naa_id_lbl.Visible = false;
                            pictureBox3.Visible = false;
                            pictureBox4.Visible = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

            }
            #endregion
            #region No Filter No Filter Volume Details
            if (clustername != "No Filter" && filter != "No Filter" && voltype_combobox.Text == "Volume Details Only")
            {
                if (filter == "Top 10 used volumes")
                {
                    volumeList.Items.Clear();
                    string sql = "select volumename ,  (select count(*) from volumedetails where clustername='" + clustername + "' and createdfrom='N/A')  from volumedetails where clustername='" + clustername + "' and createdfrom='N/A' order by realvolsize desc limit 10";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                    volumeList.Items.Clear();
                    try
                    {
                        int num = 0;
                        SQLiteDataReader dr = cmd.ExecuteReader();
                        if (dr.HasRows)
                        {
                            vol_size_lbl.Visible = true;
                            cluster_name_lbl.Visible = true;
                            used_size_lbl.Visible = true;
                            created_from_lbl.Visible = true;
                            vol_name_lbl.Visible = true;
                            vol_index_lbl.Visible = true;
                            created_on_lbl.Visible = true;
                            vol_percent_used_lbl.Visible = true;
                            vol_pic.Visible = true;
                            small_io_lbl.Visible = true;
                            unaligned_io_lbl.Visible = true;
                            vaai_tp_lbl.Visible = true;
                            naa_id_lbl.Visible = true;
                            pictureBox3.Visible = true;
                            pictureBox4.Visible = true;
                            while (dr.Read())
                            {
                                volumeList.Items.Add(dr[0].ToString());
                                num++;
                            }
                            volumeList.SelectedIndex = 0;
                            total_vol_lbl.Text = "Items Displayed : ";
                            total_vol_lbl.Text = total_vol_lbl.Text + num.ToString();
                        }
                        else
                        {
                            MessageBox.Show("No Data for Selected Filter", "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            volumeList.Text = "";
                            vol_size_lbl.Visible = false;
                            cluster_name_lbl.Visible = false;
                            used_size_lbl.Visible = false;
                            created_from_lbl.Visible = false;
                            vol_name_lbl.Visible = false;
                            vol_index_lbl.Visible = false;
                            created_on_lbl.Visible = false;
                            vol_percent_used_lbl.Visible = false;
                            vol_pic.Visible = false;
                            small_io_lbl.Visible = false;
                            unaligned_io_lbl.Visible = false;
                            vaai_tp_lbl.Visible = false;
                            naa_id_lbl.Visible = false;
                            pictureBox3.Visible = false;
                            pictureBox4.Visible = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                if (filter == "Least 10 used volumes")
                {
                    volumeList.Items.Clear();
                    string sql = "select volumename ,  (select count(*) from volumedetails where clustername='" + clustername + "' and createdfrom='N/A') from volumedetails where clustername='" + clustername + "' and createdfrom='N/A' order by realvolsize limit 10";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                    volumeList.Items.Clear();
                    try
                    {
                        int num = 0;
                        SQLiteDataReader dr = cmd.ExecuteReader();
                        if (dr.HasRows)
                        {
                            vol_size_lbl.Visible = true;
                            cluster_name_lbl.Visible = true;
                            used_size_lbl.Visible = true;
                            created_from_lbl.Visible = true;
                            vol_name_lbl.Visible = true;
                            vol_index_lbl.Visible = true;
                            created_on_lbl.Visible = true;
                            vol_percent_used_lbl.Visible = true;
                            vol_pic.Visible = true;
                            small_io_lbl.Visible = true;
                            unaligned_io_lbl.Visible = true;
                            vaai_tp_lbl.Visible = true;
                            naa_id_lbl.Visible = true;
                            pictureBox3.Visible = true;
                            pictureBox4.Visible = true;
                            while (dr.Read())
                            {
                                volumeList.Items.Add(dr[0].ToString());
                                num++;
                            }
                            volumeList.SelectedIndex = 0;
                            total_vol_lbl.Text = "Items Displayed : ";
                            total_vol_lbl.Text = total_vol_lbl.Text + num.ToString();
                        }
                        else
                        {
                            MessageBox.Show("No Data for Selected Filter", "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            volumeList.Text = "";
                            vol_size_lbl.Visible = false;
                            cluster_name_lbl.Visible = false;
                            used_size_lbl.Visible = false;
                            created_from_lbl.Visible = false;
                            vol_name_lbl.Visible = false;
                            vol_index_lbl.Visible = false;
                            created_on_lbl.Visible = false;
                            vol_percent_used_lbl.Visible = false;
                            vol_pic.Visible = false;
                            small_io_lbl.Visible = false;
                            unaligned_io_lbl.Visible = false;
                            vaai_tp_lbl.Visible = false;
                            naa_id_lbl.Visible = false;
                            pictureBox3.Visible = false;
                            pictureBox4.Visible = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

            }
            #endregion
            #region No Filter No Filter Snapshot Details
            if (clustername != "No Filter" && filter != "No Filter" && voltype_combobox.Text == "Snapshot Details Only")
            {
                if (filter == "Top 10 used volumes")
                {
                    volumeList.Items.Clear();
                    string sql = "select volumename , (select count(*) from volumedetails where clustername='" + clustername + "' and createdfrom!='N/A'  order by realvolsize desc limit 10)  from volumedetails where clustername='" + clustername + "' and createdfrom!='N/A' order by realvolsize desc limit 10";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                    volumeList.Items.Clear();
                    try
                    {
                        int num = 0;
                        SQLiteDataReader dr = cmd.ExecuteReader();
                        if (dr.HasRows)
                        {
                            vol_size_lbl.Visible = true;
                            cluster_name_lbl.Visible = true;
                            used_size_lbl.Visible = true;
                            created_from_lbl.Visible = true;
                            vol_name_lbl.Visible = true;
                            vol_index_lbl.Visible = true;
                            created_on_lbl.Visible = true;
                            vol_percent_used_lbl.Visible = true;
                            vol_pic.Visible = true;
                            small_io_lbl.Visible = true;
                            unaligned_io_lbl.Visible = true;
                            vaai_tp_lbl.Visible = true;
                            naa_id_lbl.Visible = true;
                            pictureBox3.Visible = true;
                            pictureBox4.Visible = true;
                            while (dr.Read())
                            {
                                volumeList.Items.Add(dr[0].ToString());
                                num++;
                            }
                            volumeList.SelectedIndex = 0;
                            total_vol_lbl.Text = "Items Displayed : ";
                            total_vol_lbl.Text = total_vol_lbl.Text + num.ToString();
                        }
                        else
                        {
                            MessageBox.Show("No Data for Selected Filter", "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            volumeList.Text = "";
                            vol_size_lbl.Visible = false;
                            cluster_name_lbl.Visible = false;
                            used_size_lbl.Visible = false;
                            created_from_lbl.Visible = false;
                            vol_name_lbl.Visible = false;
                            vol_index_lbl.Visible = false;
                            created_on_lbl.Visible = false;
                            vol_percent_used_lbl.Visible = false;
                            vol_pic.Visible = false;
                            small_io_lbl.Visible = false;
                            unaligned_io_lbl.Visible = false;
                            vaai_tp_lbl.Visible = false;
                            naa_id_lbl.Visible = false;
                            pictureBox3.Visible = false;
                            pictureBox4.Visible = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                if (filter == "Least 10 used volumes")
                {
                    volumeList.Items.Clear();
                    string sql = "select volumename from volumedetails where clustername='" + clustername + "' and createdfrom !='N/A' order by realvolsize limit 10";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                    volumeList.Items.Clear();
                    try
                    {
                        int num = 0;
                        SQLiteDataReader dr = cmd.ExecuteReader();
                        if (dr.HasRows)
                        {
                            vol_size_lbl.Visible = true;
                            cluster_name_lbl.Visible = true;
                            used_size_lbl.Visible = true;
                            created_from_lbl.Visible = true;
                            vol_name_lbl.Visible = true;
                            vol_index_lbl.Visible = true;
                            created_on_lbl.Visible = true;
                            vol_percent_used_lbl.Visible = true;
                            vol_pic.Visible = true;
                            small_io_lbl.Visible = true;
                            unaligned_io_lbl.Visible = true;
                            vaai_tp_lbl.Visible = true;
                            naa_id_lbl.Visible = true;
                            pictureBox3.Visible = true;
                            pictureBox4.Visible = true;
                            while (dr.Read())
                            {
                                volumeList.Items.Add(dr[0].ToString());
                                num++;
                            }
                            volumeList.SelectedIndex = 0;
                            total_vol_lbl.Text = "Items Displayed : ";
                            total_vol_lbl.Text = total_vol_lbl.Text + num.ToString();
                        }
                        else
                        {
                            MessageBox.Show("No Data for Selected Filter", "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            volumeList.Text = "";
                            vol_size_lbl.Visible = false;
                            cluster_name_lbl.Visible = false;
                            used_size_lbl.Visible = false;
                            created_from_lbl.Visible = false;
                            vol_name_lbl.Visible = false;
                            vol_index_lbl.Visible = false;
                            created_on_lbl.Visible = false;
                            vol_percent_used_lbl.Visible = false;
                            vol_pic.Visible = false;
                            small_io_lbl.Visible = false;
                            unaligned_io_lbl.Visible = false;
                            vaai_tp_lbl.Visible = false;
                            naa_id_lbl.Visible = false;
                            pictureBox3.Visible = false;
                            pictureBox4.Visible = false;
                        }
                        
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

            }
            #endregion
            #region No Filter No Filter All
            if (clustername == "No Filter" && filter != "No Filter" && voltype_combobox.Text == "All")
            {
                if (filter == "Top 10 used volumes")
                {
                    volumeList.Items.Clear();
                    string sql = "select volumename from volumedetails order by realvolsize desc limit 10";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                    volumeList.Items.Clear();
                    try
                    {
                        int num = 0;
                        SQLiteDataReader dr = cmd.ExecuteReader();
                        if (dr.HasRows)
                        {
                            vol_size_lbl.Visible = true;
                            cluster_name_lbl.Visible = true;
                            used_size_lbl.Visible = true;
                            created_from_lbl.Visible = true;
                            vol_name_lbl.Visible = true;
                            vol_index_lbl.Visible = true;
                            created_on_lbl.Visible = true;
                            vol_percent_used_lbl.Visible = true;
                            vol_pic.Visible = true;
                            small_io_lbl.Visible = true;
                            unaligned_io_lbl.Visible = true;
                            vaai_tp_lbl.Visible = true;
                            naa_id_lbl.Visible = true;
                            pictureBox3.Visible = true;
                            pictureBox4.Visible = true;
                            while (dr.Read())
                            {
                                volumeList.Items.Add(dr[0].ToString());
                                num++;
                            }
                            volumeList.SelectedIndex = 0;
                            total_vol_lbl.Text = "Items Displayed : ";
                            total_vol_lbl.Text = total_vol_lbl.Text + num.ToString();
                        }
                        else
                        {
                            MessageBox.Show("No Data for Selected Filter", "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            volumeList.Text = "";
                            vol_size_lbl.Visible = false;
                            cluster_name_lbl.Visible = false;
                            used_size_lbl.Visible = false;
                            created_from_lbl.Visible = false;
                            vol_name_lbl.Visible = false;
                            vol_index_lbl.Visible = false;
                            created_on_lbl.Visible = false;
                            vol_percent_used_lbl.Visible = false;
                            vol_pic.Visible = false;
                            small_io_lbl.Visible = false;
                            unaligned_io_lbl.Visible = false;
                            vaai_tp_lbl.Visible = false;
                            naa_id_lbl.Visible = false;
                            pictureBox3.Visible = false;
                            pictureBox4.Visible = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                if (filter == "Least 10 used volumes")
                {
                    volumeList.Items.Clear();
                    string sql = "select volumename from volumedetails order by realvolsize limit 10";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                    volumeList.Items.Clear();
                    try
                    {
                        int num = 0;
                        SQLiteDataReader dr = cmd.ExecuteReader();
                        if (dr.HasRows)
                        {
                            vol_size_lbl.Visible = true;
                            cluster_name_lbl.Visible = true;
                            used_size_lbl.Visible = true;
                            created_from_lbl.Visible = true;
                            vol_name_lbl.Visible = true;
                            vol_index_lbl.Visible = true;
                            created_on_lbl.Visible = true;
                            vol_percent_used_lbl.Visible = true;
                            vol_pic.Visible = true;
                            small_io_lbl.Visible = true;
                            unaligned_io_lbl.Visible = true;
                            vaai_tp_lbl.Visible = true;
                            naa_id_lbl.Visible = true;
                            pictureBox3.Visible = true;
                            pictureBox4.Visible = true;
                            while (dr.Read())
                            {
                                volumeList.Items.Add(dr[0].ToString());
                                num++;
                            }
                            volumeList.SelectedIndex = 0;
                            total_vol_lbl.Text = "Items Displayed : ";
                            total_vol_lbl.Text = total_vol_lbl.Text + num.ToString();
                        }
                        else
                        {
                            MessageBox.Show("No Data for Selected Filter", "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            volumeList.Text = "";
                            vol_size_lbl.Visible = false;
                            cluster_name_lbl.Visible = false;
                            used_size_lbl.Visible = false;
                            created_from_lbl.Visible = false;
                            vol_name_lbl.Visible = false;
                            vol_index_lbl.Visible = false;
                            created_on_lbl.Visible = false;
                            vol_percent_used_lbl.Visible = false;
                            vol_pic.Visible = false;
                            small_io_lbl.Visible = false;
                            unaligned_io_lbl.Visible = false;
                            vaai_tp_lbl.Visible = false;
                            naa_id_lbl.Visible = false;
                            pictureBox3.Visible = false;
                            pictureBox4.Visible = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

            }
            #endregion
            #region No Filter No Filter Volume Details only
            if (clustername == "No Filter" && filter != "No Filter" && voltype_combobox.Text == "Volume Details Only")
            {
                if (filter == "Top 10 used volumes")
                {
                    volumeList.Items.Clear();
                    string sql = "select volumename from volumedetails where createdfrom='N/A' order by realvolsize desc limit 10";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                    volumeList.Items.Clear();
                    try
                    {
                        int num = 0;
                        SQLiteDataReader dr = cmd.ExecuteReader();
                        if (dr.HasRows)
                        {
                            vol_size_lbl.Visible = true;
                            cluster_name_lbl.Visible = true;
                            used_size_lbl.Visible = true;
                            created_from_lbl.Visible = true;
                            vol_name_lbl.Visible = true;
                            vol_index_lbl.Visible = true;
                            created_on_lbl.Visible = true;
                            vol_percent_used_lbl.Visible = true;
                            vol_pic.Visible = true;
                            small_io_lbl.Visible = true;
                            unaligned_io_lbl.Visible = true;
                            vaai_tp_lbl.Visible = true;
                            naa_id_lbl.Visible = true;
                            pictureBox3.Visible = true;
                            pictureBox4.Visible = true;
                            while (dr.Read())
                            {
                                volumeList.Items.Add(dr[0].ToString());
                                num++;
                            }
                            volumeList.SelectedIndex = 0;
                            total_vol_lbl.Text = "Items Displayed : ";
                            total_vol_lbl.Text = total_vol_lbl.Text + num.ToString();
                        }
                        else
                        {
                            MessageBox.Show("No Data for Selected Filter", "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            volumeList.Text = "";
                            vol_size_lbl.Visible = false;
                            cluster_name_lbl.Visible = false;
                            used_size_lbl.Visible = false;
                            created_from_lbl.Visible = false;
                            vol_name_lbl.Visible = false;
                            vol_index_lbl.Visible = false;
                            created_on_lbl.Visible = false;
                            vol_percent_used_lbl.Visible = false;
                            vol_pic.Visible = false;
                            small_io_lbl.Visible = false;
                            unaligned_io_lbl.Visible = false;
                            vaai_tp_lbl.Visible = false;
                            naa_id_lbl.Visible = false;
                            pictureBox3.Visible = false;
                            pictureBox4.Visible = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                if (filter == "Least 10 used volumes")
                {
                    volumeList.Items.Clear();
                    string sql = "select volumename from volumedetails where createdfrom='N/A' order by realvolsize limit 10";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                    volumeList.Items.Clear();
                    try
                    {
                        int num = 0;
                        SQLiteDataReader dr = cmd.ExecuteReader();
                        if (dr.HasRows)
                        {
                            vol_size_lbl.Visible = true;
                            cluster_name_lbl.Visible = true;
                            used_size_lbl.Visible = true;
                            created_from_lbl.Visible = true;
                            vol_name_lbl.Visible = true;
                            vol_index_lbl.Visible = true;
                            created_on_lbl.Visible = true;
                            vol_percent_used_lbl.Visible = true;
                            vol_pic.Visible = true;
                            small_io_lbl.Visible = true;
                            unaligned_io_lbl.Visible = true;
                            vaai_tp_lbl.Visible = true;
                            naa_id_lbl.Visible = true;
                            pictureBox3.Visible = true;
                            pictureBox4.Visible = true;
                            while (dr.Read())
                            {
                                volumeList.Items.Add(dr[0].ToString());
                                num++;
                            }
                            volumeList.SelectedIndex = 0;
                            total_vol_lbl.Text = "Items Displayed : ";
                            total_vol_lbl.Text = total_vol_lbl.Text + num.ToString();
                        }
                        else
                        {
                            MessageBox.Show("No Data for Selected Filter", "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            volumeList.Text = "";
                            vol_size_lbl.Visible = false;
                            cluster_name_lbl.Visible = false;
                            used_size_lbl.Visible = false;
                            created_from_lbl.Visible = false;
                            vol_name_lbl.Visible = false;
                            vol_index_lbl.Visible = false;
                            created_on_lbl.Visible = false;
                            vol_percent_used_lbl.Visible = false;
                            vol_pic.Visible = false;
                            small_io_lbl.Visible = false;
                            unaligned_io_lbl.Visible = false;
                            vaai_tp_lbl.Visible = false;
                            naa_id_lbl.Visible = false;
                            pictureBox3.Visible = false;
                            pictureBox4.Visible = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                
                }

            }
            #endregion
            #region No Filter No filter Snapshot Only
            if (clustername == "No Filter" && filter != "No Filter" && voltype_combobox.Text == "Snapshot Details Only")
            {
                if (filter == "Top 10 used volumes")
                {
                    volumeList.Items.Clear();
                    string sql = "select volumename from volumedetails where createdfrom !='N/A' order by realvolsize desc limit 10";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                    volumeList.Items.Clear();
                    try
                    {
                        int num = 0;
                        SQLiteDataReader dr = cmd.ExecuteReader();
                        if (dr.HasRows)
                        {
                            vol_size_lbl.Visible = true;
                            cluster_name_lbl.Visible = true;
                            used_size_lbl.Visible = true;
                            created_from_lbl.Visible = true;
                            vol_name_lbl.Visible = true;
                            vol_index_lbl.Visible = true;
                            created_on_lbl.Visible = true;
                            vol_percent_used_lbl.Visible = true;
                            vol_pic.Visible = true;
                            small_io_lbl.Visible = true;
                            unaligned_io_lbl.Visible = true;
                            vaai_tp_lbl.Visible = true;
                            naa_id_lbl.Visible = true;
                            pictureBox3.Visible = true;
                            pictureBox4.Visible = true;
                            while (dr.Read())
                            {
                                volumeList.Items.Add(dr[0].ToString());
                                num++;

                            }
                            volumeList.SelectedIndex = 0;
                            total_vol_lbl.Text = "Items Displayed : ";
                            total_vol_lbl.Text = total_vol_lbl.Text + num.ToString();
                        }
                        else
                        {
                            MessageBox.Show("No Data for Selected Filter", "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            volumeList.Text = "";
                            vol_size_lbl.Visible = false;
                            cluster_name_lbl.Visible = false;
                            used_size_lbl.Visible = false;
                            created_from_lbl.Visible = false;
                            vol_name_lbl.Visible = false;
                            vol_index_lbl.Visible = false;
                            created_on_lbl.Visible = false;
                            vol_percent_used_lbl.Visible = false;
                            vol_pic.Visible = false;
                            small_io_lbl.Visible = false;
                            unaligned_io_lbl.Visible = false;
                            vaai_tp_lbl.Visible = false;
                            naa_id_lbl.Visible = false;
                            pictureBox3.Visible = false;
                            pictureBox4.Visible = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                if (filter == "Least 10 used volumes")
                {
                    volumeList.Items.Clear();
                    string sql = "select volumename from volumedetails where createdfrom !='N/A' order by realvolsize limit 10";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                    volumeList.Items.Clear();
                    try
                    {
                        int num = 0;
                        SQLiteDataReader dr = cmd.ExecuteReader();
                        if (dr.HasRows)
                        {
                            vol_size_lbl.Visible = true;
                            cluster_name_lbl.Visible = true;
                            used_size_lbl.Visible = true;
                            created_from_lbl.Visible = true;
                            vol_name_lbl.Visible = true;
                            vol_index_lbl.Visible = true;
                            created_on_lbl.Visible = true;
                            vol_percent_used_lbl.Visible = true;
                            vol_pic.Visible = true;
                            small_io_lbl.Visible = true;
                            unaligned_io_lbl.Visible = true;
                            vaai_tp_lbl.Visible = true;
                            naa_id_lbl.Visible = true;
                            pictureBox3.Visible = true;
                            pictureBox4.Visible = true;
                            while (dr.Read())
                            {
                                volumeList.Items.Add(dr[0].ToString());
                                total_vol_lbl.Text = "Items Displayed : ";
                                total_vol_lbl.Text = total_vol_lbl.Text + "10";
                            }
                            volumeList.SelectedIndex = 0;
                        }
                        else
                        {
                            MessageBox.Show("No Data for Selected Filter", "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            volumeList.Text = "";
                            vol_size_lbl.Visible = false;
                            cluster_name_lbl.Visible = false;
                            used_size_lbl.Visible = false;
                            created_from_lbl.Visible = false;
                            vol_name_lbl.Visible = false;
                            vol_index_lbl.Visible = false;
                            created_on_lbl.Visible = false;
                            vol_percent_used_lbl.Visible = false;
                            vol_pic.Visible = false;
                            small_io_lbl.Visible = false;
                            unaligned_io_lbl.Visible = false;
                            vaai_tp_lbl.Visible = false;
                            naa_id_lbl.Visible = false;
                            pictureBox3.Visible = false;
                            pictureBox4.Visible = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

            }
            #endregion
            #region No Filter No Filter All
            if (clustername == "No Filter" && filter == "No Filter" && voltype_combobox.Text == "All")
            {
                volumeList.Items.Clear();
                string sql = "select * , (select count(*) from volumedetails) from volumedetails";
                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                try
                {

                    SQLiteDataReader dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        vol_size_lbl.Visible = true;
                        cluster_name_lbl.Visible = true;
                        used_size_lbl.Visible = true;
                        created_from_lbl.Visible = true;
                        vol_name_lbl.Visible = true;
                        vol_index_lbl.Visible = true;
                        created_on_lbl.Visible = true;
                        vol_percent_used_lbl.Visible = true;
                        vol_pic.Visible = true;
                        small_io_lbl.Visible = true;
                        unaligned_io_lbl.Visible = true;
                        vaai_tp_lbl.Visible = true;
                        naa_id_lbl.Visible = true;
                        pictureBox3.Visible = true;
                        pictureBox4.Visible = true;
                        while (dr.Read())
                        {
                            volumeList.Items.Add(dr[0].ToString());
                            total_vol_lbl.Text = "Items Displayed : ";
                            total_vol_lbl.Text = total_vol_lbl.Text + dr[14].ToString();

                        }
                        volumeList.SelectedIndex = 0;
                    }
                    else
                    {
                        MessageBox.Show("No Data for Selected Filter", "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        volumeList.Text = "";
                        vol_size_lbl.Visible = false;
                        cluster_name_lbl.Visible = false;
                        used_size_lbl.Visible = false;
                        created_from_lbl.Visible = false;
                        vol_name_lbl.Visible = false;
                        vol_index_lbl.Visible = false;
                        created_on_lbl.Visible = false;
                        vol_percent_used_lbl.Visible = false;
                        vol_pic.Visible = false;
                        small_io_lbl.Visible = false;
                        unaligned_io_lbl.Visible = false;
                        vaai_tp_lbl.Visible = false;
                        naa_id_lbl.Visible = false;
                        pictureBox3.Visible = false;
                        pictureBox4.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            #endregion
            #region No Filter No FIlter VOlume only
            if (clustername == "No Filter" && filter == "No Filter" && voltype_combobox.Text == "Volume Details Only")
            {
                volumeList.Items.Clear();
                string sql = "select * , (select count(*) from volumedetails where createdfrom='N/A') from volumedetails where createdfrom='N/A'";
                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                try
                {
                    int num = 0;
                    SQLiteDataReader dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        vol_size_lbl.Visible = true;
                        cluster_name_lbl.Visible = true;
                        used_size_lbl.Visible = true;
                        created_from_lbl.Visible = true;
                        vol_name_lbl.Visible = true;
                        vol_index_lbl.Visible = true;
                        created_on_lbl.Visible = true;
                        vol_percent_used_lbl.Visible = true;
                        vol_pic.Visible = true;
                        small_io_lbl.Visible = true;
                        unaligned_io_lbl.Visible = true;
                        vaai_tp_lbl.Visible = true;
                        naa_id_lbl.Visible = true;
                        pictureBox3.Visible = true;
                        pictureBox4.Visible = true;
                        while (dr.Read())
                        {
                            volumeList.Items.Add(dr[0].ToString());
                            total_vol_lbl.Text = "Items Displayed : ";
                            num++;
                        }
                        volumeList.SelectedIndex = 0;
                        total_vol_lbl.Text = total_vol_lbl.Text + num.ToString();
                    }
                    else
                    {
                        MessageBox.Show("No Data for Selected Filter", "No Data Found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        volumeList.Text = "";
                        vol_size_lbl.Visible = false;
                        cluster_name_lbl.Visible = false;
                        used_size_lbl.Visible = false;
                        created_from_lbl.Visible = false;
                        vol_name_lbl.Visible = false;
                        vol_index_lbl.Visible = false;
                        created_on_lbl.Visible = false;
                        vol_percent_used_lbl.Visible = false;
                        vol_pic.Visible = false;
                        small_io_lbl.Visible = false;
                        unaligned_io_lbl.Visible = false;
                        vaai_tp_lbl.Visible = false;
                        naa_id_lbl.Visible = false;
                        pictureBox3.Visible = false;
                        pictureBox4.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            #endregion
            #region No FIlter No Filter Snapshot Only
            if (clustername == "No Filter" && filter == "No Filter" && voltype_combobox.Text == "Snapshot Details Only")
            {
                volumeList.Items.Clear();
                string sql = "select * , (select count(*) from volumedetails where createdfrom !='N/A')  from volumedetails where createdfrom !='N/A'";
                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                try
                {
                    int num = 0;
                    SQLiteDataReader dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        vol_size_lbl.Visible = true;
                        cluster_name_lbl.Visible = true;
                        used_size_lbl.Visible = true;
                        created_from_lbl.Visible = true;
                        vol_name_lbl.Visible = true;
                        vol_index_lbl.Visible = true;
                        created_on_lbl.Visible = true;
                        vol_percent_used_lbl.Visible = true;
                        vol_pic.Visible = true;
                        small_io_lbl.Visible = true;
                        unaligned_io_lbl.Visible = true;
                        vaai_tp_lbl.Visible = true;
                        naa_id_lbl.Visible = true;
                        pictureBox3.Visible = true;
                        pictureBox4.Visible = true;
                        while (dr.Read())
                        {
                            volumeList.Items.Add(dr[0].ToString());
                            total_vol_lbl.Text = "Items Displayed : ";
                            num++;
                           
                        }
                        volumeList.SelectedIndex = 0;
                        
                        total_vol_lbl.Text = total_vol_lbl.Text + num.ToString();
                    }
                    else
                    {
                        MessageBox.Show("No Data for Selected Filter","No Data Found",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                        volumeList.Text = "";
                        vol_size_lbl.Visible = false;
                        cluster_name_lbl.Visible = false;
                        used_size_lbl.Visible = false;
                        created_from_lbl.Visible = false;
                        vol_name_lbl.Visible = false;
                        vol_index_lbl.Visible = false;
                        created_on_lbl.Visible = false;
                        vol_percent_used_lbl.Visible = false;
                        vol_pic.Visible = false;
                        small_io_lbl.Visible = false;
                        unaligned_io_lbl.Visible = false;
                        vaai_tp_lbl.Visible = false;
                        naa_id_lbl.Visible = false;
                        pictureBox3.Visible = false;
                        pictureBox4.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
           
           
            listBox1_SelectedIndexChanged(null, null);
        }
            #endregion
            #endregion
      

        public string s { get; set; }

         
    }
}
