﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;
namespace xtremeIO
{
    public partial class Cluster_Reports_1 : Form
    {
        #region Global Variable Declaration
        SQLiteConnection m_dbConnection;
        LandingPage back_instance;
        public Cluster_Reports_1(SQLiteConnection conn, string ip, string os, LandingPage back_data)
        {
            InitializeComponent();
            back_instance = back_data;
            m_dbConnection = conn;
            xms_ip_lbl.Text = ip;
            xms_os_lbl.Text = os;
        }
        #endregion
        #region Form Load
        private void Cluster_Reports_1_Load(object sender, EventArgs e)
        {
            filter_data.SelectedIndex = 0;
            go_btn_Click(null, null);
        }
        #endregion
        #region GO Button Click
        private void go_btn_Click(object sender, EventArgs e)
        {
            foreach (var series in graph_data.Series)
            {
                series.Points.Clear();
            }

            switch (filter_data.SelectedIndex)
            {
                #region Cluster Reads
                case 0:
                    {
                        int yaxis = 0;
                        double temp = 0;
                        string sql = "select clustername,totalreads from clusterdetails";
                        SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                        SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        foreach (DataRow dr in dt.Rows)
                        {
                            temp = double.Parse(dr["totalreads"].ToString());
                            if (temp > 0)
                            {
                                yaxis = 1;
                            }

                            if (dr["clustername"].ToString().Length > 50)
                            {
                                dr["clustername"] = dr["clustername"].ToString().Substring(0, 50);
                            }
                            graph_data.Series[0].Points.AddXY(dr["clustername"].ToString(), dr["totalreads"].ToString());
                        }
                        graph_data.Series[0].LegendText = "Total Reads in GB";
                        if (yaxis == 0)
                        {
                            graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 1;
                        }
                        else
                        {
                            graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 0;
                            graph_data.ChartAreas["ChartArea1"].AxisY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
                        }
                        
                    }
                    break;
                #endregion
                #region Total Writes
                case 1:
                    {
                        int yaxis = 0;
                        double temp=0;
                        string sql = "select clustername,totalwrites from clusterdetails";
                        SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                        SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        foreach (DataRow dr in dt.Rows)
                        {
                            temp = double.Parse(dr["totalwrites"].ToString());
                            if (temp > 0)
                            {
                                yaxis = 1;
                            }

                            if (dr["clustername"].ToString().Length > 50)
                            {
                                dr["clustername"] = dr["clustername"].ToString().Substring(0, 50);
                            }
                            graph_data.Series[0].Points.AddXY(dr["clustername"].ToString(), dr["totalwrites"].ToString());
                        }
                        graph_data.Series[0].LegendText = "Total Writes in GB";
                        if (yaxis == 0)
                        {
                            graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 1;
                        }
                       else
                        {
                            graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 0;
                            graph_data.ChartAreas["ChartArea1"].AxisY.IntervalType =System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
                        }

                    }
                    break;
                #endregion
                #region Free Logical Space
                case 2:
                    {
                        int yaxis = 0;
                        string sql = "select clustername,free_logical_space from clusterdetails";
                        SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                        SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        foreach (DataRow dr in dt.Rows)
                        {

                            if (dr["clustername"].ToString().Length > 50)
                            {
                                dr["clustername"] = dr["clustername"].ToString().Substring(0, 50);
                            }
                            if (dr["free_logical_space"].ToString() != "N/A")
                            {
                                double temp = double.Parse(dr["free_logical_space"].ToString());
                                if (temp > 0)
                                {
                                    yaxis = 1;
                                }
                                graph_data.Series[0].Points.AddXY(dr["clustername"].ToString(), 100 - temp);
                            }
                            else
                            {
                                MessageBox.Show("Logical space for the cluster is not available ! ");
                                break;
                            }
                           
                        }
                        graph_data.Series[0].LegendText = "Logical Space used in %";
                        if (yaxis == 0)
                        {
                            graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 1;
                        }
                       else
                        {
                            graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 0;
                            graph_data.ChartAreas["ChartArea1"].AxisY.IntervalType =System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
                        }
                    }
                    break;
                #endregion
                #region Free Physical Space
                case 3:
                    {
                        int yaxis = 0;
                        string sql = "select clustername,free_phy_space from clusterdetails";
                        SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                        SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        foreach (DataRow dr in dt.Rows)
                        {

                            if (dr["clustername"].ToString().Length > 50)
                            {
                                dr["clustername"] = dr["clustername"].ToString().Substring(0, 50);
                            }
                            if (dr["free_phy_space"].ToString() != "N/A")
                            {
                                double temp = double.Parse(dr["free_phy_space"].ToString());
                                if (temp > 0)
                                {
                                    yaxis = 1;
                                }
                                graph_data.Series[0].Points.AddXY(dr["clustername"].ToString(), 100 - temp);
                            }
                            else
                            {
                                MessageBox.Show("Physical space for the cluster is not available ! ");
                                break;
                            }
                        }
                        graph_data.Series[0].LegendText = "Physical Space used in %";
                        if (yaxis == 0)
                        {
                            graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 1;
                        }
                        else
                        {
                            graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 0;
                            graph_data.ChartAreas["ChartArea1"].AxisY.IntervalType =System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
                        }
                    }
                    break;
                #endregion
                #region Free Logical Space GB
                case 4:
                    {
                        int yaxis = 0;
                        string sql = "select clustername,freelogicalspace_gb from clusterdetails";
                        SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                        SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        foreach (DataRow dr in dt.Rows)
                        {

                            if (dr["clustername"].ToString().Length > 50)
                            {
                                dr["clustername"] = dr["clustername"].ToString().Substring(0, 50);
                            }
                            if (dr["freelogicalspace_gb"].ToString() != "N/A")
                            {
                                double temp = double.Parse(dr["freelogicalspace_gb"].ToString());
                                if (temp > 0)
                                {
                                    yaxis = 1;
                                }
                                graph_data.Series[0].Points.AddXY(dr["clustername"].ToString(), temp);
                            }
                            else
                            {
                                MessageBox.Show("Logical space for the cluster is not available ! ");
                                break;
                            }
                        }
                        graph_data.Series[0].LegendText = "Logical Space Free in GB";
                        if (yaxis == 0)
                        {
                            graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 1;
                        }
                        else
                        {
                            graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 0;
                            graph_data.ChartAreas["ChartArea1"].AxisY.IntervalType =System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
                        }
                    }
                    break;
                #endregion
                #region Free Physical Space GB
                case 5:
                    {
                        int yaxis = 0;
                        
                        string sql = "select clustername,freephyspace_gb from clusterdetails";
                        SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                        SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        foreach (DataRow dr in dt.Rows)
                        {

                            if (dr["clustername"].ToString().Length > 50)
                            {
                                dr["clustername"] = dr["clustername"].ToString().Substring(0, 50);
                            }
                            if (dr["freephyspace_gb"].ToString() != "N/A")
                            {
                                double temp = double.Parse(dr["freephyspace_gb"].ToString());
                                if (temp > 0)
                                {
                                    yaxis = 1;
                                }
                                graph_data.Series[0].Points.AddXY(dr["clustername"].ToString(), temp);
                            }
                            else
                            {
                                MessageBox.Show("Physical space for the cluster is not available ! ");
                                break;
                            }
                        }
                        graph_data.Series[0].LegendText = "Physical Space Free in GB";
                        if (yaxis == 0)
                        {
                            graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 1;
                        }
                        else
                        {
                            graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 0;
                            graph_data.ChartAreas["ChartArea1"].AxisY.IntervalType =System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
                        }
                    }
                    break;
                #endregion
                #region Number of Volumes
                case 6:
                    {
                        int yaxis = 0;
                       
                        string sql = "select clustername,num_of_vol from clusterdetails";
                        SQLiteCommand cmd = new SQLiteCommand(sql, m_dbConnection);
                        SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        foreach (DataRow dr in dt.Rows)
                        {

                            if (dr["clustername"].ToString().Length > 50)
                            {
                                dr["clustername"] = dr["clustername"].ToString().Substring(0, 50);
                            }
                            if (dr["num_of_vol"].ToString() != "N/A")
                            {
                                double temp = double.Parse(dr["num_of_vol"].ToString());
                                if (temp > 0)
                                {
                                    yaxis = 1;
                                }
                                graph_data.Series[0].Points.AddXY(dr["clustername"].ToString(), temp);
                            }
                            else
                            {
                                graph_data.Series[0].Points.AddXY(dr["clustername"].ToString(), 0);
                            }
                        }
                        graph_data.Series[0].LegendText = "Total Number of volumes";
                        if (yaxis == 0)
                        {
                            graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 1;
                        }
                        else
                        {
                            graph_data.ChartAreas["ChartArea1"].AxisY.Interval = 0;
                            graph_data.ChartAreas["ChartArea1"].AxisY.IntervalType =System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
                        }
                    }
                    break;
                #endregion
            }
        }
        #endregion

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
       

    }
}
