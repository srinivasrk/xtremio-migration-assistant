﻿namespace xtremeIO
{
    partial class Volume_Details
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.xms_os_lbl = new System.Windows.Forms.Label();
            this.xms_ip_lbl = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.voltype_combobox = new System.Windows.Forms.ComboBox();
            this.clustername_combobox = new System.Windows.Forms.ComboBox();
            this.data_filter_combobox = new System.Windows.Forms.ComboBox();
            this.go_btn = new System.Windows.Forms.Button();
            this.total_initiator_lbl = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.created_from_lbl = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.total_vol_lbl = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.created_on_lbl = new System.Windows.Forms.Label();
            this.naa_id_lbl = new System.Windows.Forms.Label();
            this.vaai_tp_lbl = new System.Windows.Forms.Label();
            this.unaligned_io_lbl = new System.Windows.Forms.Label();
            this.small_io_lbl = new System.Windows.Forms.Label();
            this.used_size_lbl = new System.Windows.Forms.Label();
            this.vol_size_lbl = new System.Windows.Forms.Label();
            this.cluster_name_lbl = new System.Windows.Forms.Label();
            this.vol_index_lbl = new System.Windows.Forms.Label();
            this.vol_percent_used_lbl = new System.Windows.Forms.Label();
            this.vol_name_lbl = new System.Windows.Forms.Label();
            this.vol_pic = new System.Windows.Forms.PictureBox();
            this.volumeList = new System.Windows.Forms.ComboBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.snapshotDetailsGrid = new System.Windows.Forms.DataGridView();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.status_lbl = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.hostmapping_data = new System.Windows.Forms.DataGridView();
            this.total_snap_lbl = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vol_pic)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.snapshotDetailsGrid)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hostmapping_data)).BeginInit();
            this.SuspendLayout();
            // 
            // xms_os_lbl
            // 
            this.xms_os_lbl.AutoSize = true;
            this.xms_os_lbl.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xms_os_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xms_os_lbl.ForeColor = System.Drawing.Color.Green;
            this.xms_os_lbl.Location = new System.Drawing.Point(156, 9);
            this.xms_os_lbl.Name = "xms_os_lbl";
            this.xms_os_lbl.Size = new System.Drawing.Size(148, 18);
            this.xms_os_lbl.TabIndex = 11;
            this.xms_os_lbl.Text = "XMS OS version : ";
            // 
            // xms_ip_lbl
            // 
            this.xms_ip_lbl.AutoSize = true;
            this.xms_ip_lbl.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xms_ip_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xms_ip_lbl.ForeColor = System.Drawing.Color.Green;
            this.xms_ip_lbl.Location = new System.Drawing.Point(156, 38);
            this.xms_ip_lbl.Name = "xms_ip_lbl";
            this.xms_ip_lbl.Size = new System.Drawing.Size(144, 18);
            this.xms_ip_lbl.TabIndex = 10;
            this.xms_ip_lbl.Text = "XMS IP address : ";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.xms_ip_lbl);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.xms_os_lbl);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1142, 105);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(1007, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 20);
            this.label2.TabIndex = 31;
            this.label2.Text = "Beta Version";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Image = global::xtremeIO.Properties.Resources.dell_EMC_logo;
            this.pictureBox2.Location = new System.Drawing.Point(997, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Padding = new System.Windows.Forms.Padding(5);
            this.pictureBox2.Size = new System.Drawing.Size(139, 83);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 30;
            this.pictureBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(147)))), ((int)(((byte)(206)))));
            this.label1.Location = new System.Drawing.Point(-4, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 20);
            this.label1.TabIndex = 22;
            this.label1.Text = "Migration Assistant";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.Control;
            this.label3.Font = new System.Drawing.Font("Perpetua Titling MT", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(400, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(369, 24);
            this.label3.TabIndex = 21;
            this.label3.Text = "XtremIO Migration Assistant";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::xtremeIO.Properties.Resources.XtremIO_logo;
            this.pictureBox1.Location = new System.Drawing.Point(12, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Padding = new System.Windows.Forms.Padding(5);
            this.pictureBox1.Size = new System.Drawing.Size(127, 83);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.voltype_combobox);
            this.groupBox5.Controls.Add(this.clustername_combobox);
            this.groupBox5.Controls.Add(this.data_filter_combobox);
            this.groupBox5.Controls.Add(this.go_btn);
            this.groupBox5.Controls.Add(this.total_initiator_lbl);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Location = new System.Drawing.Point(0, 105);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(1142, 58);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(335, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 17);
            this.label6.TabIndex = 16;
            this.label6.Text = "Vol/SnapShot";
            // 
            // voltype_combobox
            // 
            this.voltype_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.voltype_combobox.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voltype_combobox.FormattingEnabled = true;
            this.voltype_combobox.Items.AddRange(new object[] {
            "All",
            "Volume Details Only",
            "Snapshot Details Only"});
            this.voltype_combobox.Location = new System.Drawing.Point(434, 16);
            this.voltype_combobox.Name = "voltype_combobox";
            this.voltype_combobox.Size = new System.Drawing.Size(165, 27);
            this.voltype_combobox.TabIndex = 2;
            // 
            // clustername_combobox
            // 
            this.clustername_combobox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.clustername_combobox.BackColor = System.Drawing.SystemColors.Window;
            this.clustername_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.clustername_combobox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.clustername_combobox.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clustername_combobox.FormattingEnabled = true;
            this.clustername_combobox.Location = new System.Drawing.Point(175, 18);
            this.clustername_combobox.Name = "clustername_combobox";
            this.clustername_combobox.Size = new System.Drawing.Size(142, 27);
            this.clustername_combobox.TabIndex = 1;
            // 
            // data_filter_combobox
            // 
            this.data_filter_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.data_filter_combobox.Enabled = false;
            this.data_filter_combobox.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.data_filter_combobox.FormattingEnabled = true;
            this.data_filter_combobox.Items.AddRange(new object[] {
            "No Filter",
            "Top 10 used volumes",
            "Least 10 used volumes"});
            this.data_filter_combobox.Location = new System.Drawing.Point(404, 18);
            this.data_filter_combobox.Name = "data_filter_combobox";
            this.data_filter_combobox.Size = new System.Drawing.Size(12, 27);
            this.data_filter_combobox.TabIndex = 14;
            this.data_filter_combobox.Visible = false;
            // 
            // go_btn
            // 
            this.go_btn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.go_btn.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.go_btn.Location = new System.Drawing.Point(628, 16);
            this.go_btn.Name = "go_btn";
            this.go_btn.Size = new System.Drawing.Size(43, 27);
            this.go_btn.TabIndex = 3;
            this.go_btn.Text = "Go";
            this.go_btn.UseVisualStyleBackColor = true;
            this.go_btn.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // total_initiator_lbl
            // 
            this.total_initiator_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.total_initiator_lbl.AutoSize = true;
            this.total_initiator_lbl.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.total_initiator_lbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total_initiator_lbl.ForeColor = System.Drawing.Color.Black;
            this.total_initiator_lbl.Location = new System.Drawing.Point(6, 20);
            this.total_initiator_lbl.Name = "total_initiator_lbl";
            this.total_initiator_lbl.Size = new System.Drawing.Size(163, 17);
            this.total_initiator_lbl.TabIndex = 6;
            this.total_initiator_lbl.Text = "Filter by Cluster Name:";
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.splitContainer1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(0, 163);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1142, 568);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(3, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 15);
            this.label4.TabIndex = 22;
            this.label4.Text = "Volume List";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 18);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.splitContainer1.Panel1.Controls.Add(this.created_from_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.pictureBox3);
            this.splitContainer1.Panel1.Controls.Add(this.total_vol_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.pictureBox4);
            this.splitContainer1.Panel1.Controls.Add(this.created_on_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.naa_id_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.vaai_tp_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.unaligned_io_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.small_io_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.used_size_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.vol_size_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.cluster_name_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.vol_index_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.vol_percent_used_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.vol_name_lbl);
            this.splitContainer1.Panel1.Controls.Add(this.vol_pic);
            this.splitContainer1.Panel1.Controls.Add(this.volumeList);
            this.splitContainer1.Panel1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Panel2.Controls.Add(this.total_snap_lbl);
            this.splitContainer1.Panel2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.splitContainer1.Size = new System.Drawing.Size(1136, 547);
            this.splitContainer1.SplitterDistance = 317;
            this.splitContainer1.TabIndex = 19;
            // 
            // created_from_lbl
            // 
            this.created_from_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.created_from_lbl.AutoSize = true;
            this.created_from_lbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.created_from_lbl.Location = new System.Drawing.Point(142, 286);
            this.created_from_lbl.Name = "created_from_lbl";
            this.created_from_lbl.Size = new System.Drawing.Size(109, 17);
            this.created_from_lbl.TabIndex = 53;
            this.created_from_lbl.Text = "<created from>";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox3.Image = global::xtremeIO.Properties.Resources.cluster4;
            this.pictureBox3.Location = new System.Drawing.Point(104, 192);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(32, 27);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 52;
            this.pictureBox3.TabStop = false;
            // 
            // total_vol_lbl
            // 
            this.total_vol_lbl.AutoSize = true;
            this.total_vol_lbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total_vol_lbl.Location = new System.Drawing.Point(298, 7);
            this.total_vol_lbl.Name = "total_vol_lbl";
            this.total_vol_lbl.Size = new System.Drawing.Size(127, 17);
            this.total_vol_lbl.TabIndex = 51;
            this.total_vol_lbl.Text = "Items Displayed : ";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox4.Image = global::xtremeIO.Properties.Resources.clock_128;
            this.pictureBox4.Location = new System.Drawing.Point(639, 194);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(29, 25);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 50;
            this.pictureBox4.TabStop = false;
            // 
            // created_on_lbl
            // 
            this.created_on_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.created_on_lbl.AutoSize = true;
            this.created_on_lbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.created_on_lbl.Location = new System.Drawing.Point(670, 202);
            this.created_on_lbl.Name = "created_on_lbl";
            this.created_on_lbl.Size = new System.Drawing.Size(95, 17);
            this.created_on_lbl.TabIndex = 49;
            this.created_on_lbl.Text = "<created on>";
            // 
            // naa_id_lbl
            // 
            this.naa_id_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.naa_id_lbl.AutoSize = true;
            this.naa_id_lbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.naa_id_lbl.Location = new System.Drawing.Point(882, 272);
            this.naa_id_lbl.Name = "naa_id_lbl";
            this.naa_id_lbl.Size = new System.Drawing.Size(65, 17);
            this.naa_id_lbl.TabIndex = 48;
            this.naa_id_lbl.Text = "<naaID>";
            // 
            // vaai_tp_lbl
            // 
            this.vaai_tp_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.vaai_tp_lbl.AutoSize = true;
            this.vaai_tp_lbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vaai_tp_lbl.Location = new System.Drawing.Point(882, 241);
            this.vaai_tp_lbl.Name = "vaai_tp_lbl";
            this.vaai_tp_lbl.Size = new System.Drawing.Size(65, 17);
            this.vaai_tp_lbl.TabIndex = 47;
            this.vaai_tp_lbl.Text = "<vaaitp>";
            // 
            // unaligned_io_lbl
            // 
            this.unaligned_io_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.unaligned_io_lbl.AutoSize = true;
            this.unaligned_io_lbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unaligned_io_lbl.Location = new System.Drawing.Point(882, 145);
            this.unaligned_io_lbl.Name = "unaligned_io_lbl";
            this.unaligned_io_lbl.Size = new System.Drawing.Size(101, 17);
            this.unaligned_io_lbl.TabIndex = 46;
            this.unaligned_io_lbl.Text = "<unalignedio>";
            // 
            // small_io_lbl
            // 
            this.small_io_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.small_io_lbl.AutoSize = true;
            this.small_io_lbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.small_io_lbl.Location = new System.Drawing.Point(882, 106);
            this.small_io_lbl.Name = "small_io_lbl";
            this.small_io_lbl.Size = new System.Drawing.Size(72, 17);
            this.small_io_lbl.TabIndex = 45;
            this.small_io_lbl.Text = "<smallio>";
            // 
            // used_size_lbl
            // 
            this.used_size_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.used_size_lbl.AutoSize = true;
            this.used_size_lbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.used_size_lbl.Location = new System.Drawing.Point(142, 241);
            this.used_size_lbl.Name = "used_size_lbl";
            this.used_size_lbl.Size = new System.Drawing.Size(86, 17);
            this.used_size_lbl.TabIndex = 44;
            this.used_size_lbl.Text = "<used size>";
            // 
            // vol_size_lbl
            // 
            this.vol_size_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.vol_size_lbl.AutoSize = true;
            this.vol_size_lbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vol_size_lbl.Location = new System.Drawing.Point(142, 145);
            this.vol_size_lbl.Name = "vol_size_lbl";
            this.vol_size_lbl.Size = new System.Drawing.Size(75, 17);
            this.vol_size_lbl.TabIndex = 43;
            this.vol_size_lbl.Text = "<vol size>";
            // 
            // cluster_name_lbl
            // 
            this.cluster_name_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cluster_name_lbl.AutoSize = true;
            this.cluster_name_lbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cluster_name_lbl.Location = new System.Drawing.Point(142, 194);
            this.cluster_name_lbl.Name = "cluster_name_lbl";
            this.cluster_name_lbl.Size = new System.Drawing.Size(110, 17);
            this.cluster_name_lbl.TabIndex = 42;
            this.cluster_name_lbl.Text = "<cluster name>";
            // 
            // vol_index_lbl
            // 
            this.vol_index_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.vol_index_lbl.AutoSize = true;
            this.vol_index_lbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vol_index_lbl.Location = new System.Drawing.Point(463, 105);
            this.vol_index_lbl.Name = "vol_index_lbl";
            this.vol_index_lbl.Size = new System.Drawing.Size(86, 17);
            this.vol_index_lbl.TabIndex = 41;
            this.vol_index_lbl.Text = "<vol index>";
            // 
            // vol_percent_used_lbl
            // 
            this.vol_percent_used_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.vol_percent_used_lbl.AutoSize = true;
            this.vol_percent_used_lbl.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vol_percent_used_lbl.Location = new System.Drawing.Point(463, 274);
            this.vol_percent_used_lbl.Name = "vol_percent_used_lbl";
            this.vol_percent_used_lbl.Size = new System.Drawing.Size(133, 15);
            this.vol_percent_used_lbl.TabIndex = 40;
            this.vol_percent_used_lbl.Text = "Logical Percent Used : ";
            // 
            // vol_name_lbl
            // 
            this.vol_name_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.vol_name_lbl.AutoSize = true;
            this.vol_name_lbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vol_name_lbl.Location = new System.Drawing.Point(463, 72);
            this.vol_name_lbl.Name = "vol_name_lbl";
            this.vol_name_lbl.Size = new System.Drawing.Size(85, 17);
            this.vol_name_lbl.TabIndex = 39;
            this.vol_name_lbl.Text = "<vol name>";
            // 
            // vol_pic
            // 
            this.vol_pic.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.vol_pic.Image = global::xtremeIO.Properties.Resources.database_512;
            this.vol_pic.Location = new System.Drawing.Point(491, 135);
            this.vol_pic.Name = "vol_pic";
            this.vol_pic.Size = new System.Drawing.Size(142, 136);
            this.vol_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.vol_pic.TabIndex = 16;
            this.vol_pic.TabStop = false;
            // 
            // volumeList
            // 
            this.volumeList.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.volumeList.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.volumeList.FormattingEnabled = true;
            this.volumeList.Location = new System.Drawing.Point(9, 7);
            this.volumeList.Name = "volumeList";
            this.volumeList.Size = new System.Drawing.Size(254, 27);
            this.volumeList.TabIndex = 0;
            this.volumeList.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1136, 226);
            this.tabControl1.TabIndex = 22;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.snapshotDetailsGrid);
            this.tabPage1.Controls.Add(this.statusStrip1);
            this.tabPage1.Location = new System.Drawing.Point(4, 28);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1128, 194);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Snapshot Details";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // snapshotDetailsGrid
            // 
            this.snapshotDetailsGrid.AllowUserToAddRows = false;
            this.snapshotDetailsGrid.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.snapshotDetailsGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.snapshotDetailsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.snapshotDetailsGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.snapshotDetailsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.snapshotDetailsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.snapshotDetailsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.snapshotDetailsGrid.Location = new System.Drawing.Point(3, 3);
            this.snapshotDetailsGrid.Name = "snapshotDetailsGrid";
            this.snapshotDetailsGrid.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Aquamarine;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.snapshotDetailsGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.snapshotDetailsGrid.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.snapshotDetailsGrid.Size = new System.Drawing.Size(1122, 166);
            this.snapshotDetailsGrid.TabIndex = 54;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.status_lbl});
            this.statusStrip1.Location = new System.Drawing.Point(3, 169);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1122, 22);
            this.statusStrip1.TabIndex = 21;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // status_lbl
            // 
            this.status_lbl.ForeColor = System.Drawing.Color.Red;
            this.status_lbl.Name = "status_lbl";
            this.status_lbl.Size = new System.Drawing.Size(118, 17);
            this.status_lbl.Text = "toolStripStatusLabel1";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.hostmapping_data);
            this.tabPage2.Location = new System.Drawing.Point(4, 28);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1128, 194);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Host Mapping";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // hostmapping_data
            // 
            this.hostmapping_data.AllowUserToAddRows = false;
            this.hostmapping_data.AllowUserToDeleteRows = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hostmapping_data.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.hostmapping_data.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.hostmapping_data.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.hostmapping_data.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.hostmapping_data.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.hostmapping_data.DefaultCellStyle = dataGridViewCellStyle7;
            this.hostmapping_data.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hostmapping_data.Location = new System.Drawing.Point(3, 3);
            this.hostmapping_data.Name = "hostmapping_data";
            this.hostmapping_data.Size = new System.Drawing.Size(1122, 188);
            this.hostmapping_data.TabIndex = 0;
            // 
            // total_snap_lbl
            // 
            this.total_snap_lbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.total_snap_lbl.AutoSize = true;
            this.total_snap_lbl.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.total_snap_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total_snap_lbl.ForeColor = System.Drawing.Color.Red;
            this.total_snap_lbl.Location = new System.Drawing.Point(882, 0);
            this.total_snap_lbl.Name = "total_snap_lbl";
            this.total_snap_lbl.Size = new System.Drawing.Size(212, 16);
            this.total_snap_lbl.TabIndex = 21;
            this.total_snap_lbl.Text = "Total Number Of SnapShots : ";
            // 
            // Volume_Details
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1142, 731);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox2);
            this.Name = "Volume_Details";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Volume Details";
            this.Load += new System.EventHandler(this.Volume_Details_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vol_pic)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.snapshotDetailsGrid)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.hostmapping_data)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label xms_os_lbl;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label xms_ip_lbl;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label total_initiator_lbl;
        private System.Windows.Forms.Button go_btn;
        private System.Windows.Forms.ComboBox data_filter_combobox;
        private System.Windows.Forms.ComboBox clustername_combobox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox voltype_combobox;
        private System.Windows.Forms.PictureBox vol_pic;
        private System.Windows.Forms.ComboBox volumeList;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label created_on_lbl;
        private System.Windows.Forms.Label naa_id_lbl;
        private System.Windows.Forms.Label vaai_tp_lbl;
        private System.Windows.Forms.Label unaligned_io_lbl;
        private System.Windows.Forms.Label small_io_lbl;
        private System.Windows.Forms.Label used_size_lbl;
        private System.Windows.Forms.Label vol_size_lbl;
        private System.Windows.Forms.Label cluster_name_lbl;
        private System.Windows.Forms.Label vol_index_lbl;
        private System.Windows.Forms.Label vol_percent_used_lbl;
        private System.Windows.Forms.Label vol_name_lbl;
        private System.Windows.Forms.Label total_vol_lbl;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label created_from_lbl;
        private System.Windows.Forms.Label total_snap_lbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView hostmapping_data;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel status_lbl;
        private System.Windows.Forms.DataGridView snapshotDetailsGrid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}