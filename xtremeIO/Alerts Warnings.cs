﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SQLite;
namespace xtremeIO
{
    public partial class Alerts_Warnings : Form
    {
        
        LandingPage form1_data;
        #region Global Variables
        // Global Variables listing
        List<string> allclusterNames = new List<string>();
        List<string> targetIndex = new List<string>();
        List<String> alertCode = new List<string>();
        List<string> alertState = new List<string>();
        List<string> alertType = new List<string>();
        List<string> entityData = new List<string>();
        List<string> entityName = new List<string>();
        List<string> alertDesc = new List<string>();
        List<string> objectSev = new List<string>();
        List<string> raiseTime = new List<string>();
        List<string> clusterName = new List<string>();
        // End of Variables listing
        #endregion Global Variables
        #region Create Form
        SQLiteConnection m_dbconnection;
        public Alerts_Warnings(SQLiteConnection path, string xms_ip, string xms_os, LandingPage form1_instance)
        {
            InitializeComponent();
            form1_data = form1_instance;
            if (path != null)
            {
                m_dbconnection = path;
                xms_ip_lbl.Text = xms_ip;
                xms_os_lbl.Text = xms_os;
            }
            else
            {
                MessageBox.Show("Error loading path , Please reload the application", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion Create Form
        #region Load Form
        private void Alerts_Warnings_Load(object sender, EventArgs e)
        {
            //Get data from table and fill the form
            int num = 0;
            try
            {
                DataTable dt = new DataTable();
                string sql = "select * from alertdata";
                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
                
                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
               
                da.Fill(dt);
                dt.Columns["alertcode"].ColumnName = "Alert Code";
                dt.Columns["alertstate"].ColumnName = "Alert State";
                dt.Columns["alerttype"].ColumnName = "Alert Type";
                dt.Columns["alertentitydata"].ColumnName = "Alert Entity Data";
                dt.Columns["alertdesc"].ColumnName = "Alert Description";
                dt.Columns["objectsev"].ColumnName = "Alert Severity";
                dt.Columns["raisetime"].ColumnName = "Raise Time";
                dt.Columns["alertclusterName"].ColumnName = "Cluster Name";
                dt.Columns["alertindex"].ColumnName = "Alert Index";
                dt.Columns["alertentityname"].ColumnName = "Entity Name";
                alertInfo.DataSource = dt;

                objectSev_combobox.Items.Add("No Filter");
                entityData_combobox.Items.Add("No Filter");

                cmd = new SQLiteCommand(sql, m_dbconnection);
                SQLiteDataReader dr = cmd.ExecuteReader();


                sql = "select * from alertdata";
                while (dr.Read())
                {
                    num++;
                    if (!objectSev_combobox.Items.Contains(dr[7].ToString())) 
                    {
                        objectSev_combobox.Items.Add(dr[7].ToString());
                    }
                    if (!entityData_combobox.Items.Contains(dr[4].ToString())) 
                    {
                        entityData_combobox.Items.Add(dr[4].ToString());
                    }
                }
                total_alert_lbl.Text = total_alert_lbl.Text + num.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            objectSev_combobox.SelectedIndex = objectSev_combobox.Items.IndexOf("No Filter");
            entityData_combobox.SelectedIndex = entityData_combobox.Items.IndexOf("No Filter");
            alertInfo_CellContentClick(null, null);
        }

        #endregion Load Form
        #region Go Button Click
        private void button1_Click(object sender, EventArgs e)
        {
            total_alert_lbl.Text = "Total Number of Alerts : ";

            if (objectSev_combobox.Text != "No Filter" && entityData_combobox.Text != "No Filter")
            {
                string[] insertRow = new string[10];
                try
                {
                    DataTable dt = new DataTable();
                    string sql = "select *  from alertdata where objectsev='" + objectSev_combobox.Text + "' and alertentitydata='" + entityData_combobox.Text + "'";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                  
                    da.Fill(dt);
                    dt.Columns["alertcode"].ColumnName = "Alert Code";
                    dt.Columns["alertstate"].ColumnName = "Alert State";
                    dt.Columns["alerttype"].ColumnName = "Alert Type";
                    dt.Columns["alertentitydata"].ColumnName = "Alert Entity Data";
                    dt.Columns["alertdesc"].ColumnName = "Alert Description";
                    dt.Columns["objectsev"].ColumnName = "Alert Severity";
                    dt.Columns["raisetime"].ColumnName = "Raise Time";
                    dt.Columns["alertclusterName"].ColumnName = "Cluster Name";
                    dt.Columns["alertindex"].ColumnName = "Alert Index";
                    dt.Columns["alertentityname"].ColumnName = "Entity Name";
                    alertInfo.DataSource = dt;
                    sql = "select count(*)  from alertdata where objectsev='" + objectSev_combobox.Text + "' and alertentitydata='" + entityData_combobox.Text + "'";
                    cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataReader dr = cmd.ExecuteReader();
                    total_alert_lbl.Text = total_alert_lbl.Text + dr[0].ToString();
                }

                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message.ToString());
                }

            }
            else if (objectSev_combobox.Text != "No Filter" && entityData_combobox.Text == "No Filter")
            {
                string[] insertRow = new string[10];
                try
                {
                    DataTable dt = new DataTable();
                    string sql = "select * from alertdata where objectsev='" + objectSev_combobox.Text + "'";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                   
                    da.Fill(dt);
                    dt.Columns["alertcode"].ColumnName = "Alert Code";
                    dt.Columns["alertstate"].ColumnName = "Alert State";
                    dt.Columns["alerttype"].ColumnName = "Alert Type";
                    dt.Columns["alertentitydata"].ColumnName = "Alert Entity Data";
                    dt.Columns["alertdesc"].ColumnName = "Alert Description";
                    dt.Columns["objectsev"].ColumnName = "Alert Severity";
                    dt.Columns["raisetime"].ColumnName = "Raise Time";
                    dt.Columns["alertclusterName"].ColumnName = "Cluster Name";
                    dt.Columns["alertindex"].ColumnName = "Alert Index";
                    dt.Columns["alertentityname"].ColumnName = "Entity Name";
                    alertInfo.DataSource = dt;
                    sql = "select count(*) from alertdata where objectsev='" + objectSev_combobox.Text + "'";
                    SQLiteCommand cmd1 = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataReader dr = cmd1.ExecuteReader();
                    total_alert_lbl.Text = total_alert_lbl.Text + dr[0].ToString();
                }

                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message.ToString());
                }

            }
            else if (objectSev_combobox.Text == "No Filter" && entityData_combobox.Text != "No Filter")
            {
                string[] insertRow = new string[10];
                try
                {
                    DataTable dt = new DataTable();
                    string sql = "select * from alertdata where alertentitydata='" + entityData_combobox.Text + "'";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                   
                    da.Fill(dt);
                    dt.Columns["alertcode"].ColumnName = "Alert Code";
                    dt.Columns["alertstate"].ColumnName = "Alert State";
                    dt.Columns["alerttype"].ColumnName = "Alert Type";
                    dt.Columns["alertentitydata"].ColumnName = "Alert Entity Data";
                    dt.Columns["alertdesc"].ColumnName = "Alert Description";
                    dt.Columns["objectsev"].ColumnName = "Alert Severity";
                    dt.Columns["raisetime"].ColumnName = "Raise Time";
                    dt.Columns["alertclusterName"].ColumnName = "Cluster Name";
                    dt.Columns["alertindex"].ColumnName = "Alert Index";
                    dt.Columns["alertentityname"].ColumnName = "Entity Name";
                    alertInfo.DataSource = dt;
                    sql = "select count(*) from alertdata where alertentitydata='" + entityData_combobox.Text + "'";
                    SQLiteCommand cmd1 = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataReader dr = cmd1.ExecuteReader();
                    total_alert_lbl.Text = total_alert_lbl.Text + dr[0].ToString();
                }

                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message.ToString());
                }

            }
            else if (objectSev_combobox.Text == "No Filter" && entityData_combobox.Text == "No Filter")
            {
                string[] insertRow = new string[10];
                try
                {
                    DataTable dt = new DataTable();
                    string sql = "select * from alertdata";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                  
                    da.Fill(dt);
                    dt.Columns["alertcode"].ColumnName = "Alert Code";
                    dt.Columns["alertstate"].ColumnName = "Alert State";
                    dt.Columns["alerttype"].ColumnName = "Alert Type";
                    dt.Columns["alertentitydata"].ColumnName = "Alert Entity Data";
                    dt.Columns["alertdesc"].ColumnName = "Alert Description";
                    dt.Columns["objectsev"].ColumnName = "Alert Severity";
                    dt.Columns["raisetime"].ColumnName = "Raise Time";
                    dt.Columns["alertclusterName"].ColumnName = "Cluster Name";
                    dt.Columns["alertindex"].ColumnName = "Alert Index";
                    dt.Columns["alertentityname"].ColumnName = "Entity Name";
                    alertInfo.DataSource = dt;
                    sql ="select count(*) from alertdata";
                    SQLiteCommand cmd1 = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataReader dr = cmd1.ExecuteReader();
                    total_alert_lbl.Text = total_alert_lbl.Text + dr[0].ToString();
                }

                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message.ToString());
                }

            }
        }
        #endregion Go Button Click
        #region Change Alert Image
        private void alertInfo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            // To add image , Just to beautify the form 
            if (alertInfo.CurrentCell.Value != null && alertInfo.CurrentCell.Value.ToString() !="")
            {
                int row = alertInfo.CurrentCell.RowIndex;
                
                string val = alertInfo.Rows[row].Cells[4].Value.ToString();
                
                if (val == "Target")
                {
                    entity_pic.Image = Properties.Resources.StorageController_rear_png;
                }
                else if (val == "StorageController")
                {
                    entity_pic.Image = Properties.Resources.StorageController_rear_png;
                }
                else if (val == "Cluster")
                {
                    entity_pic.Image = Properties.Resources.XtremIO_storage_2;
                }
                else if (val == "Volume")
                {
                    entity_pic.Image = Properties.Resources.DAE_front_png;
                }
                else if (val == "Xms")
                {
                    entity_pic.Image = Properties.Resources.XtremIO_storage_2;
                }
                else if (val == "DAELCC")
                {
                    entity_pic.Image = Properties.Resources.DAE_rear_png;
                }
                

            }
        }
        #endregion Change Alert Image

        private void label3_Click(object sender, EventArgs e)
        {

        }

    }

}

