﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SQLite;
namespace xtremeIO
{
    public partial class Xms_Information : Form
    {
        #region Global Variables Declaraion
        SQLiteConnection m_dbconnection;
        public Xms_Information(SQLiteConnection path, string xms_ip, string xms_os, LandingPage formInstance)
        {
            InitializeComponent();
            if (path != null)
            {
                m_dbconnection = path;
                xms_ip_lbl.Text = xms_ip;
                xms_os_lbl.Text = xms_os;
            }
        }
        #endregion
        #region Form Load

        private void Xms_Information_Load(object sender, EventArgs e)
        {
            findXMSDetails();
        }
        #endregion
        #region Get Info XMS
        private void findXMSDetails()
        {
            int val = 0, val1 = 0;
            string sql = "select * from xmsinfo";
            SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
            SQLiteDataReader dr = cmd.ExecuteReader();
            xms_ip_lbl.Text = "XMS IP Address : ";
            xms_os_lbl.Text = "XMS OS Version : ";
            while (dr.Read())
            {
                xms_ip_lbl.Text = xms_ip_lbl.Text + dr[1].ToString();
                xms_subnet_ip.Text = xms_subnet_ip.Text + dr[2].ToString();
                xms_os_lbl.Text = xms_os_lbl.Text + dr[3].ToString();
                xms_platform_lbl.Text = xms_platform_lbl.Text + dr[4].ToString();
                xms_gateway_lbl.Text = xms_gateway_lbl.Text + dr[5].ToString();
                if (dr[11].ToString() != "N/A")
                {
                    val = 100 - int.Parse(dr[11].ToString());
                    logical_vol_lbl.Text = val.ToString() + "%";
                }
                else
                {
                    logical_vol_lbl.Text = "N/A";
                }
                if (dr[12].ToString() != "N/A")
                {
                    val1 = 100 - int.Parse(dr[12].ToString());
                    phy_vol_consumed_lbl.Text = val1.ToString() + "%";
                }
                else
                {
                    phy_vol_consumed_lbl.Text = "N/A";
                }
                if (dr[13].ToString().Trim().Length > 3)
                {
                    string[] temp = dr[13].ToString().Split(',');
                    foreach (string s in temp)
                    {
                        xms_ntp_lbl.Text =xms_ntp_lbl.Text+s+Environment.NewLine;
                    }
                }
                else
                {
                    xms_ntp_lbl.Text = "N/A";
                }
                if (dr[6].ToString() != "N/A")
                {
                     double x = double.Parse(dr[6].ToString());
                    double dedupRatio = Math.Round(x, 2);
                    dedup_ratio.Text = dedupRatio.ToString() + ":1";
                }
                else
                {
                    dedup_ratio.Text = "N/A";
                }
                if (dr[8].ToString() != "N/A")
                {
                    double x = double.Parse(dr[8].ToString());
                    double compressionRatio = Math.Round(x, 2);
                    compression_ratio.Text = compressionRatio.ToString() + ":1";
                }
                else
                {
                    compression_ratio.Text = "N/A";
                }
                break;
            }

            if (val == 0)
            {
                logical_vol_pic.Image = Properties.Resources.progressbar_0;

            }
            else if (val < 10)
            {
                logical_vol_pic.Image = Properties.Resources.progressbar_0_10;
            }
            else if (val < 20)
            {
                logical_vol_pic.Image = Properties.Resources.progressbar_10_15;
            }
            else if (val < 30)
            {
                logical_vol_pic.Image = Properties.Resources.progressbar_20_30;
            }
            else if (val < 40)
            {
                logical_vol_pic.Image = Properties.Resources.progressbar_30_40;
            }
            else if (val < 50)
            {
                logical_vol_pic.Image = Properties.Resources.progressbar_40_50;
            }
            else if (val < 60)
            {
                logical_vol_pic.Image = Properties.Resources.progressbar_50_60;
            }

            else if (val < 70)
            {
                logical_vol_pic.Image = Properties.Resources.progressbar_60_70;
            }
            else if (val < 80)
            {
                logical_vol_pic.Image = Properties.Resources.progressbar_70_80;
            }
            else if (val < 90)
            {
                logical_vol_pic.Image = Properties.Resources.progressbar_80_90;
            }
            else if (val < 100)
            {
                logical_vol_pic.Image = Properties.Resources.progressbar_90_95;
            }
            else
            {
                logical_vol_pic.Image = Properties.Resources.progressbar_100;
            }


            if (val1 == 0)
            {
                phy_vol_pic.Image = Properties.Resources.progressbar_0;

            }
            else if (val1 < 10)
            {
                phy_vol_pic.Image = Properties.Resources.progressbar_0_10;
            }
            else if (val1 < 20)
            {
                phy_vol_pic.Image = Properties.Resources.progressbar_10_15;
            }
            else if (val1 < 30)
            {
                phy_vol_pic.Image = Properties.Resources.progressbar_20_30;
            }
            else if (val1 < 40)
            {
                phy_vol_pic.Image = Properties.Resources.progressbar_30_40;
            }
            else if (val1 < 50)
            {
                phy_vol_pic.Image = Properties.Resources.progressbar_40_50;
            }
            else if (val1 < 60)
            {
                phy_vol_pic.Image = Properties.Resources.progressbar_50_60;
            }

            else if (val1 < 70)
            {
                phy_vol_pic.Image = Properties.Resources.progressbar_60_70;
            }
            else if (val1 < 80)
            {
                phy_vol_pic.Image = Properties.Resources.progressbar_70_80;
            }
            else if (val1 < 90)
            {
                phy_vol_pic.Image = Properties.Resources.progressbar_80_90;
            }
            else if (val1 < 100)
            {
                phy_vol_pic.Image = Properties.Resources.progressbar_90_95;
            }
            else
            {
                phy_vol_pic.Image = Properties.Resources.progressbar_100;
            }

        }
        #endregion
    }
       

}

