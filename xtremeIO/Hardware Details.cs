﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SQLite;
namespace xtremeIO
{
    public partial class Hardware_Details : Form
    {
        #region Global Variable Declaration
        LandingPage form1_data;
        public string logPath;
        List<string> targetName = new List<string>();
        List<string> targetIndex = new List<string>();
        List<string> jumboFrames = new List<string>();
        List<string> mtuData = new List<string>();
        List<string> portAddress = new List<string>();
        List<string> macAddress = new List<string>();
        List<string> portSpeed = new List<string>();
        List<string> portState = new List<string>();
        List<string> portType = new List<string>();
        List<string> clusterName = new List<string>();
        List<string> allclusterNames = new List<string>();
        SQLiteConnection m_dbconnection;
        
        public Hardware_Details(SQLiteConnection path, string xms_ip, string xms_os, LandingPage form1_instance)
        {
            InitializeComponent();
           
            form1_data = form1_instance;
            if (path != null)
            {
                m_dbconnection = path;
                xms_ip_lbl.Text = xms_ip;
                xms_os_lbl.Text = xms_os;
            }
            else
            {
                MessageBox.Show("Error loading path , Please reload the application", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion
        #region Form Load
        private void Hardware_Details_Load(object sender, EventArgs e)
        {
            int num = 0;
            try
            {
                DataTable dt = new DataTable();
                clustername_combobox.Items.Add("No Filter");
                portType_combobox.Items.Add("No Filter");
                portstate_combobox.Items.Add("No Filter");

                string sql = "select * from targetdata";
                SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
               
                da.Fill(dt);
                dt.Columns["targetname"].ColumnName = "Target Name";
                dt.Columns["targetindex"].ColumnName = "Target Index";
                dt.Columns["targetjumboframes"].ColumnName = "Jumbo Frames (?)";
                dt.Columns["targetmtudata"].ColumnName = "Size of MTU";
                dt.Columns["targetportaddress"].ColumnName = "Target port address";
                dt.Columns["targetmacaddress"].ColumnName = "Target MAC address";
                dt.Columns["targetportstate"].ColumnName = "Port State";
                dt.Columns["targetportspeed"].ColumnName = "Port Speed";
                dt.Columns["targetporttype"].ColumnName = "Port Type";
                dt.Columns["targetsclusterName"].ColumnName = "Target Cluster Name";
                hardwareInfo.DataSource = dt;

                sql = "select * from targetdata";
                cmd = new SQLiteCommand(sql, m_dbconnection);

                SQLiteDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    num++;
                    if (!clustername_combobox.Items.Contains(dr[9].ToString()))
                    {
                        clustername_combobox.Items.Add(dr[9].ToString());
                    }



                    if (!portType_combobox.Items.Contains(dr[8].ToString()))
                    {
                        portType_combobox.Items.Add(dr[8].ToString());
                    }


                    if (!portstate_combobox.Items.Contains(dr[6].ToString()))
                    {
                        portstate_combobox.Items.Add(dr[6].ToString());
                    }


                }
                total_port_lbl.Text = total_port_lbl.Text + num.ToString();
                clustername_combobox.SelectedIndex = clustername_combobox.Items.IndexOf("No Filter");
                portstate_combobox.SelectedIndex = portstate_combobox.Items.IndexOf("No Filter");
                portType_combobox.SelectedIndex = portType_combobox.Items.IndexOf("No Filter");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        #endregion
        #region Go Button CLick
        private void button1_Click(object sender, EventArgs e)
        {
            total_port_lbl.Text = "Total Number of ports : ";
            #region !No Filter !No Filter !No Filter
            if (clustername_combobox.Text != "No Filter" && portstate_combobox.Text != "No Filter" && portType_combobox.Text != "No Filter")
            {
                try
                {
                    DataTable dt = new DataTable();
                    string sql = "select * from targetdata where targetsclusterName='" + clustername_combobox.Text + "' and targetportstate='" + portstate_combobox.Text + "' and targetporttype='" + portType_combobox.Text + "'";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                    
                    da.Fill(dt);
                    dt.Columns["targetname"].ColumnName = "Target Name";
                    dt.Columns["targetindex"].ColumnName = "Target Index";
                    dt.Columns["targetjumboframes"].ColumnName = "Jumbo Frames (?)";
                    dt.Columns["targetmtudata"].ColumnName = "Size of MTU";
                    dt.Columns["targetportaddress"].ColumnName = "Target port address";
                    dt.Columns["targetmacaddress"].ColumnName = "Target MAC address";
                    dt.Columns["targetportstate"].ColumnName = "Port State";
                    dt.Columns["targetportspeed"].ColumnName = "Port Speed";
                    dt.Columns["targetporttype"].ColumnName = "Port Type";
                    dt.Columns["targetsclusterName"].ColumnName = "Target Cluster Name";
                    hardwareInfo.DataSource = dt;
                    sql = "select count(*) from targetdata where targetsclusterName='" + clustername_combobox.Text + "' and targetportstate='" + portstate_combobox.Text + "' and targetporttype='" + portType_combobox.Text + "'";
                    cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataReader dr = cmd.ExecuteReader();
                    total_port_lbl.Text = total_port_lbl.Text + dr[0].ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            #endregion
            #region !No Filter !no Filter No Filter

            else if (clustername_combobox.Text != "No Filter" && portstate_combobox.Text != "No Filter" && portType_combobox.Text == "No Filter")
            {
                try
                {
                    DataTable dt = new DataTable();
                    string sql = "select * from targetdata where targetsclusterName='" + clustername_combobox.Text + "' and targetportstate='" + portstate_combobox.Text + "'";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                
                    da.Fill(dt);
                    dt.Columns["targetname"].ColumnName = "Target Name";
                    dt.Columns["targetindex"].ColumnName = "Target Index";
                    dt.Columns["targetjumboframes"].ColumnName = "Jumbo Frames (?)";
                    dt.Columns["targetmtudata"].ColumnName = "Size of MTU";
                    dt.Columns["targetportaddress"].ColumnName = "Target port address";
                    dt.Columns["targetmacaddress"].ColumnName = "Target MAC address";
                    dt.Columns["targetportstate"].ColumnName = "Port State";
                    dt.Columns["targetportspeed"].ColumnName = "Port Speed";
                    dt.Columns["targetporttype"].ColumnName = "Port Type";
                    dt.Columns["targetsclusterName"].ColumnName = "Target Cluster Name";
                    hardwareInfo.DataSource = dt;
                    sql = "select count(*) from targetdata where targetsclusterName='" + clustername_combobox.Text + "' and targetportstate='" + portstate_combobox.Text + "'";
                    cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataReader dr = cmd.ExecuteReader();
                    total_port_lbl.Text = total_port_lbl.Text + dr[0].ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }


            }
            #endregion
            #region !No Filter No Filter No Filter
            else if (clustername_combobox.Text != "No Filter" && portstate_combobox.Text == "No Filter" && portType_combobox.Text == "No Filter")
            {
                try
                {
                    DataTable dt = new DataTable();
                    string sql = "select * from targetdata where targetsclusterName='" + clustername_combobox.Text + "'";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                  
                    da.Fill(dt);
                    dt.Columns["targetname"].ColumnName = "Target Name";
                    dt.Columns["targetindex"].ColumnName = "Target Index";
                    dt.Columns["targetjumboframes"].ColumnName = "Jumbo Frames (?)";
                    dt.Columns["targetmtudata"].ColumnName = "Size of MTU";
                    dt.Columns["targetportaddress"].ColumnName = "Target port address";
                    dt.Columns["targetmacaddress"].ColumnName = "Target MAC address";
                    dt.Columns["targetportstate"].ColumnName = "Port State";
                    dt.Columns["targetportspeed"].ColumnName = "Port Speed";
                    dt.Columns["targetporttype"].ColumnName = "Port Type";
                    dt.Columns["targetsclusterName"].ColumnName = "Target Cluster Name";
                    hardwareInfo.DataSource = dt;
                    sql = "select count(*) from targetdata where targetsclusterName='" + clustername_combobox.Text + "'";
                    cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataReader dr = cmd.ExecuteReader();
                    total_port_lbl.Text = total_port_lbl.Text + dr[0].ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            #endregion
            #region No Filter !No Filter No Filter
            else if (clustername_combobox.Text == "No Filter" && portType_combobox.Text != "No Filter" && portstate_combobox.Text == "No Filter")
            {
                try
                {
                    DataTable dt = new DataTable();
                    string sql = "select * from targetdata where targetporttype='" + portType_combobox.Text + "'";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                 
                    da.Fill(dt);
                    dt.Columns["targetname"].ColumnName = "Target Name";
                    dt.Columns["targetindex"].ColumnName = "Target Index";
                    dt.Columns["targetjumboframes"].ColumnName = "Jumbo Frames (?)";
                    dt.Columns["targetmtudata"].ColumnName = "Size of MTU";
                    dt.Columns["targetportaddress"].ColumnName = "Target port address";
                    dt.Columns["targetmacaddress"].ColumnName = "Target MAC address";
                    dt.Columns["targetportstate"].ColumnName = "Port State";
                    dt.Columns["targetportspeed"].ColumnName = "Port Speed";
                    dt.Columns["targetporttype"].ColumnName = "Port Type";
                    dt.Columns["targetsclusterName"].ColumnName = "Target Cluster Name";
                    hardwareInfo.DataSource = dt;
                    sql = "select count(*) from targetdata where targetporttype='" + portType_combobox.Text + "'";
                    cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataReader dr = cmd.ExecuteReader();
                    total_port_lbl.Text = total_port_lbl.Text + dr[0].ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            #endregion
            #region No Filter !No FIlter !No Filter
            else if (clustername_combobox.Text == "No Filter" && portType_combobox.Text != "No Filter" && portstate_combobox.Text != "No Filter")
            {
                try
                {
                    DataTable dt = new DataTable();
                    string sql = "select * from targetdata where targetportstate='" + portstate_combobox.Text + "' and targetporttype='" + portType_combobox.Text + "'";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                 
                    da.Fill(dt);
                    dt.Columns["targetname"].ColumnName = "Target Name";
                    dt.Columns["targetindex"].ColumnName = "Target Index";
                    dt.Columns["targetjumboframes"].ColumnName = "Jumbo Frames (?)";
                    dt.Columns["targetmtudata"].ColumnName = "Size of MTU";
                    dt.Columns["targetportaddress"].ColumnName = "Target port address";
                    dt.Columns["targetmacaddress"].ColumnName = "Target MAC address";
                    dt.Columns["targetportstate"].ColumnName = "Port State";
                    dt.Columns["targetportspeed"].ColumnName = "Port Speed";
                    dt.Columns["targetporttype"].ColumnName = "Port Type";
                    dt.Columns["targetsclusterName"].ColumnName = "Target Cluster Name";
                    hardwareInfo.DataSource = dt;
                    sql = "select count(*) from targetdata where targetportstate='" + portstate_combobox.Text + "' and targetporttype='" + portType_combobox.Text + "'";
                    cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataReader dr = cmd.ExecuteReader();
                    total_port_lbl.Text = total_port_lbl.Text + dr[0].ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            #endregion
            #region No Filter No Filter !No Filter
            else if (clustername_combobox.Text == "No Filter" && portType_combobox.Text == "No Filter" && portstate_combobox.Text != "No Filter")
            {
                try
                {
                    DataTable dt = new DataTable();
                    string sql = "select * from targetdata where targetportstate='" + portstate_combobox.Text + "'";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                   
                    da.Fill(dt);
                    dt.Columns["targetname"].ColumnName = "Target Name";
                    dt.Columns["targetindex"].ColumnName = "Target Index";
                    dt.Columns["targetjumboframes"].ColumnName = "Jumbo Frames (?)";
                    dt.Columns["targetmtudata"].ColumnName = "Size of MTU";
                    dt.Columns["targetportaddress"].ColumnName = "Target port address";
                    dt.Columns["targetmacaddress"].ColumnName = "Target MAC address";
                    dt.Columns["targetportstate"].ColumnName = "Port State";
                    dt.Columns["targetportspeed"].ColumnName = "Port Speed";
                    dt.Columns["targetporttype"].ColumnName = "Port Type";
                    dt.Columns["targetsclusterName"].ColumnName = "Target Cluster Name";
                    hardwareInfo.DataSource = dt;
                    sql = "select count(*) from targetdata where targetportstate='" + portstate_combobox.Text + "'";
                    cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataReader dr = cmd.ExecuteReader();
                    total_port_lbl.Text = total_port_lbl.Text + dr[0].ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
            #endregion
            #region !No Filter No Filter !No Filter
            else if (clustername_combobox.Text != "No Filter" && portstate_combobox.Text == "No Filter" && portType_combobox.Text != "No Filter")
            {

                try
                {
                    DataTable dt = new DataTable();
                    string sql = "select * from targetdata where targetsclusterName='" + clustername_combobox.Text + "' and targetporttype='" + portType_combobox.Text + "'";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                  
                    da.Fill(dt);
                    dt.Columns["targetname"].ColumnName = "Target Name";
                    dt.Columns["targetindex"].ColumnName = "Target Index";
                    dt.Columns["targetjumboframes"].ColumnName = "Jumbo Frames (?)";
                    dt.Columns["targetmtudata"].ColumnName = "Size of MTU";
                    dt.Columns["targetportaddress"].ColumnName = "Target port address";
                    dt.Columns["targetmacaddress"].ColumnName = "Target MAC address";
                    dt.Columns["targetportstate"].ColumnName = "Port State";
                    dt.Columns["targetportspeed"].ColumnName = "Port Speed";
                    dt.Columns["targetporttype"].ColumnName = "Port Type";
                    dt.Columns["targetsclusterName"].ColumnName = "Target Cluster Name";
                    hardwareInfo.DataSource = dt;
                    sql = "select count(*) from targetdata where targetsclusterName='" + clustername_combobox.Text + "' and targetporttype='" + portType_combobox.Text + "'";
                    cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataReader dr = cmd.ExecuteReader();
                    total_port_lbl.Text = total_port_lbl.Text + dr[0].ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            #endregion
            #region No Filter No Filter No Filter
            else if (clustername_combobox.Text == "No Filter" && portType_combobox.Text == "No Filter" && portstate_combobox.Text == "No Filter")
            {
                try
                {
                    DataTable dt = new DataTable();
                    string sql = "select * from targetdata";
                    SQLiteCommand cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                    
                    da.Fill(dt);
                    dt.Columns["targetname"].ColumnName = "Target Name";
                    dt.Columns["targetindex"].ColumnName = "Target Index";
                    dt.Columns["targetjumboframes"].ColumnName = "Jumbo Frames (?)";
                    dt.Columns["targetmtudata"].ColumnName = "Size of MTU";
                    dt.Columns["targetportaddress"].ColumnName = "Target port address";
                    dt.Columns["targetmacaddress"].ColumnName = "Target MAC address";
                    dt.Columns["targetportstate"].ColumnName = "Port State";
                    dt.Columns["targetportspeed"].ColumnName = "Port Speed";
                    dt.Columns["targetporttype"].ColumnName = "Port Type";
                    dt.Columns["targetsclusterName"].ColumnName = "Target Cluster Name";
                    hardwareInfo.DataSource = dt;
                    sql = "select count(*) from targetdata";
                    cmd = new SQLiteCommand(sql, m_dbconnection);
                    SQLiteDataReader dr = cmd.ExecuteReader();
                    total_port_lbl.Text = total_port_lbl.Text + dr[0].ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
            #endregion

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
        #endregion
    }
}
