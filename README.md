# XtremIO Migration Assistant #

## This application is built on C# and .Net, with XtremIO Migration assistant the cost of migration from any 3rd party device or EMC array to XtremIO is reduced. The overall efficiency of migration is improved with automated scripts generation. With back end SQLite this stand alone application has a user-base across the globe.


# Features #

* Standalone application with no backend dependency
* Import XtremIO logs with single click and save them for future use
* Convert a 2GB Log file to a 100KB .SQLITE file.
* Export to Excel option
* Generate XMCLI Scripts
* Read Host grabs and provide insights into the migration efforts


### User Interface ###

![new_3.PNG](https://bitbucket.org/repo/ALBkAX/images/822827543-new_3.PNG)

![database_schema.png](https://bitbucket.org/repo/ALBkAX/images/2452515230-database_schema.png)

![new_4.PNG](https://bitbucket.org/repo/ALBkAX/images/3224840015-new_4.PNG)

![new_10.PNG](https://bitbucket.org/repo/ALBkAX/images/4196369379-new_10.PNG)

![pp14.png](https://bitbucket.org/repo/ALBkAX/images/2252235050-pp14.png)